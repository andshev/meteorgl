using System;
using System.Windows.Forms;

namespace ManagedOpenGl
{
    public class OpenGlContext : IDisposable
    {
        private readonly int _wnd;
        public int DC { get; private set; }
        public int RC { get; private set; }

        public void MakeCurrent()
        {
            wgl.MakeCurrent(DC, RC);
        }

        public void SwapBuffers()
        {
            Windows.SwapBuffers(DC);
        }

        public void Dispose()
        {
            if (RC != 0) wgl.DeleteContext(RC);
            if (DC != 0) Windows.ReleaseDC(_wnd, DC);
            RC = 0;
            DC = 0;
        }

        public OpenGlContext(Control c, byte color, byte depth, byte stencil)
        {
            bool b = false;

            _wnd = (int)c.Handle;

            if (gl.Handle == 0)
            {
                gl.Handle = Windows.LoadLibrary("OpenGL32.DLL");
                b = true;
            }

            DC = Windows.GetDC(_wnd);

            PixelFormatDescriptor pfd = new PixelFormatDescriptor();

            byte alpha = 0;

            if (color == 32)
            {
                color = 24;
                alpha = 8;
            }

            pfd.ColorBits = color;
            pfd.DepthBits = depth;
            pfd.AlphaBits = alpha;
            pfd.StencilBits = stencil;

            int pixel = Windows.ChoosePixelFormat(DC, pfd);

            Windows.SetPixelFormat(DC, pixel, pfd);

            RC = wgl.CreateContext(DC);

            MakeCurrent();

            if (b)
                gl.LoadExtensions();
        }
    }
}