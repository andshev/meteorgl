using System;
using System.Runtime.InteropServices;

namespace ManagedOpenGl
{
    public static class Windows
    {
        [DllImport("user32", EntryPoint = "GetDC")]
        public static extern int GetDC(int hwnd);
        [DllImport("User32")]
        public static extern int ReleaseDC(int hwnd, int hDC);
        [DllImport("GDI32")]
        public static extern int ChoosePixelFormat(int dc, [In, MarshalAs(UnmanagedType.LPStruct)]PixelFormatDescriptor pfd);
        [DllImport("GDI32")]
        public static extern int SetPixelFormat(int dc, int format, [In, MarshalAs(UnmanagedType.LPStruct)]PixelFormatDescriptor pfd);
        [DllImport("GDI32")]
        public static extern void SwapBuffers(int dc);
        [DllImport("Kernel32")]
        public static extern int GetProcAddress(int handle, String funcname);
        [DllImport("Kernel32")]
        public static extern int LoadLibrary(String funcname);
        [DllImport("Kernel32.dll")]
        public static extern int FreeLibrary(int handle);

    }
}