﻿/* gl.h
** 
** The contents of this file are subject to the GLX Public License Version 1.0
** (the "License"). You may not use this file except in compliance with the
** License. You may obtain a copy of the License at Silicon Graphics, Inc.,
** attn: Legal Services, 2011 N. Shoreline Blvd., Mountain View, CA 94043
** or at http://www.sgi.com/software/opensource/glx/license.html.
** 
** Software distributed under the License is distributed on an "AS IS"
** basis. ALL WARRANTIES ARE DISCLAIMED, INCLUDING, WITHOUT LIMITATION, ANY
** IMPLIED WARRANTIES OF MERCHANTABILITY, OF FITNESS FOR A PARTICULAR
** PURPOSE OR OF NON- INFRINGEMENT. See the License for the specific
** language governing rights and limitations under the License.
** 
** The Original Software is GLX version 1.2 source code, released February,
** 1999. The developer of the Original Software is Silicon Graphics, Inc.
** Those portions of the Subject Software created by Silicon Graphics, Inc.
** are Copyright (c) 1991-9 Silicon Graphics, Inc. All Rights Reserved.
*/

/* glext.h
** 
** License Applicability. Except to the extent portions of this file are
** made subject to an alternative license as permitted in the SGI Free
** Software License B, Version 1.1 (the "License"), the contents of this
** file are subject only to the provisions of the License. You may not use
** this file except in compliance with the License. You may obtain a copy
** of the License at Silicon Graphics, Inc., attn: Legal Services, 1600
** Amphitheatre Parkway, Mountain View, CA 94043-1351, or at:
** 
** http://oss.sgi.com/projects/FreeB
** 
** Note that, as provided in the License, the Software is distributed on an
** "AS IS" basis, with ALL EXPRESS AND IMPLIED WARRANTIES AND CONDITIONS
** DISCLAIMED, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES AND
** CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A
** PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
** 
** Original Code. The Original Code is: OpenGL Sample Implementation,
** Version 1.2.1, released January 26, 2000, developed by Silicon Graphics,
** Inc. The Original Code is Copyright (c) 1991-2002 Silicon Graphics, Inc.
** Copyright in any portions created by third parties is as indicated
** elsewhere herein. All Rights Reserved.
** 
** Additional Notice Provisions: This software was created using the
** OpenGL(R) version 1.2.1 Sample Implementation published by SGI, but has
** not been independently verified as being compliant with the OpenGL(R)
** version 1.2.1 Specification.
*/

/* wglext.h
**
** License Applicability. Except to the extent portions of this file are
** made subject to an alternative license as permitted in the SGI Free
** Software License B, Version 1.1 (the "License"), the contents of this
** file are subject only to the provisions of the License. You may not use
** this file except in compliance with the License. You may obtain a copy
** of the License at Silicon Graphics, Inc., attn: Legal Services, 1600
** Amphitheatre Parkway, Mountain View, CA 94043-1351, or at:
** 
** http://oss.sgi.com/projects/FreeB
** 
** Note that, as provided in the License, the Software is distributed on an
** "AS IS" basis, with ALL EXPRESS AND IMPLIED WARRANTIES AND CONDITIONS
** DISCLAIMED, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTIES AND
** CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A
** PARTICULAR PURPOSE, AND NON-INFRINGEMENT.
** 
** Original Code. The Original Code is: OpenGL Sample Implementation,
** Version 1.2.1, released January 26, 2000, developed by Silicon Graphics,
** Inc. The Original Code is Copyright (c) 1991-2002 Silicon Graphics, Inc.
** Copyright in any portions created by third parties is as indicated
** elsewhere herein. All Rights Reserved.
** 
** Additional Notice Provisions: This software was created using the
** OpenGL(R) version 1.2.1 Sample Implementation published by SGI, but has
** not been independently verified as being compliant with the OpenGL(R)
** version 1.2.1 Specification.
*/

/* glu.h
/*++ BUILD Version: 0004    // Increment this if a change has global effects

Copyright (c) 1985-95, Microsoft Corporation

Module Name:

    glu.h

Abstract:

    Procedure declarations, constant definitions and macros for the OpenGL
    Utility Library.

--*/
/*
** Copyright 1991-1993, Silicon Graphics, Inc.
** All Rights Reserved.
** 
** This is UNPUBLISHED PROPRIETARY SOURCE CODE of Silicon Graphics, Inc.;
** the contents of this file may not be disclosed to third parties, copied or
** duplicated in any form, in whole or in part, without the prior written
** permission of Silicon Graphics, Inc.
** 
** RESTRICTED RIGHTS LEGEND:
** Use, duplication or disclosure by the Government is subject to restrictions
** as set forth in subdivision (c)(1)(ii) of the Rights in Technical Data
** and Computer Software clause at DFARS 252.227-7013, and/or in similar or
** successor clauses in the FAR, DOD or NASA FAR Supplement. Unpublished -
** rights reserved under the Copyright Laws of the United States.
*/

/** C# Header and Wrapper by Lars Middendorf, lmid@gmx.de */

using System;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using ManagedOpenGl.Delegates;

namespace ManagedOpenGl
{
    public class gl
    {
        public const string DLLName = "OPENGL32.DLL";
        public const uint VERSION_1_1 = 1;
        public const uint VERSION_1_2 = 1;
        public const uint ARB_imaging = 1;
        public const uint ARB_multitexture = 1;
        public const uint ACCUM = 0x0100;
        public const uint LOAD = 0x0101;
        public const uint RETURN = 0x0102;
        public const uint MULT = 0x0103;
        public const uint ADD = 0x0104;
        public const uint NEVER = 0x0200;
        public const uint LESS = 0x0201;
        public const uint EQUAL = 0x0202;
        public const uint LEQUAL = 0x0203;
        public const uint GREATER = 0x0204;
        public const uint NOTEQUAL = 0x0205;
        public const uint GEQUAL = 0x0206;
        public const uint ALWAYS = 0x0207;
        public const uint CURRENT_BIT = 0x00000001;
        public const uint POINT_BIT = 0x00000002;
        public const uint LINE_BIT = 0x00000004;
        public const uint POLYGON_BIT = 0x00000008;
        public const uint POLYGON_STIPPLE_BIT = 0x00000010;
        public const uint PIXEL_MODE_BIT = 0x00000020;
        public const uint LIGHTING_BIT = 0x00000040;
        public const uint FOG_BIT = 0x00000080;
        public const uint DEPTH_BUFFER_BIT = 0x00000100;
        public const uint ACCUM_BUFFER_BIT = 0x00000200;
        public const uint STENCIL_BUFFER_BIT = 0x00000400;
        public const uint VIEWPORT_BIT = 0x00000800;
        public const uint TRANSFORM_BIT = 0x00001000;
        public const uint ENABLE_BIT = 0x00002000;
        public const uint COLOR_BUFFER_BIT = 0x00004000;
        public const uint HINT_BIT = 0x00008000;
        public const uint EVAL_BIT = 0x00010000;
        public const uint LIST_BIT = 0x00020000;
        public const uint TEXTURE_BIT = 0x00040000;
        public const uint SCISSOR_BIT = 0x00080000;
        public const uint ALL_ATTRIB_BITS = 0x000fffff;
        public const uint POINTS = 0x0000;
        public const uint LINES = 0x0001;
        public const uint LINE_LOOP = 0x0002;
        public const uint LINE_STRIP = 0x0003;
        public const uint TRIANGLES = 0x0004;
        public const uint TRIANGLE_STRIP = 0x0005;
        public const uint TRIANGLE_FAN = 0x0006;
        public const uint QUADS = 0x0007;
        public const uint QUAD_STRIP = 0x0008;
        public const uint POLYGON = 0x0009;
        public const uint ZERO = 0;
        public const uint ONE = 1;
        public const uint SRC_COLOR = 0x0300;
        public const uint ONE_MINUS_SRC_COLOR = 0x0301;
        public const uint SRC_ALPHA = 0x0302;
        public const uint ONE_MINUS_SRC_ALPHA = 0x0303;
        public const uint DST_ALPHA = 0x0304;
        public const uint ONE_MINUS_DST_ALPHA = 0x0305;
        public const uint DST_COLOR = 0x0306;
        public const uint ONE_MINUS_DST_COLOR = 0x0307;
        public const uint SRC_ALPHA_SATURATE = 0x0308;
        public const uint TRUE = 1;
        public const uint FALSE = 0;
        public const uint CLIP_PLANE0 = 0x3000;
        public const uint CLIP_PLANE1 = 0x3001;
        public const uint CLIP_PLANE2 = 0x3002;
        public const uint CLIP_PLANE3 = 0x3003;
        public const uint CLIP_PLANE4 = 0x3004;
        public const uint CLIP_PLANE5 = 0x3005;
        public const uint BYTE = 0x1400;
        public const uint UNSIGNED_BYTE = 0x1401;
        public const uint SHORT = 0x1402;
        public const uint UNSIGNED_SHORT = 0x1403;
        public const uint INT = 0x1404;
        public const uint UNSIGNED_INT = 0x1405;
        public const uint FLOAT = 0x1406;
        public const uint _2_BYTES = 0x1407;
        public const uint _3_BYTES = 0x1408;
        public const uint _4_BYTES = 0x1409;
        public const uint DOUBLE = 0x140A;
        public const uint NONE = 0;
        public const uint FRONT_LEFT = 0x0400;
        public const uint FRONT_RIGHT = 0x0401;
        public const uint BACK_LEFT = 0x0402;
        public const uint BACK_RIGHT = 0x0403;
        public const uint FRONT = 0x0404;
        public const uint BACK = 0x0405;
        public const uint LEFT = 0x0406;
        public const uint RIGHT = 0x0407;
        public const uint FRONT_AND_BACK = 0x0408;
        public const uint AUX0 = 0x0409;
        public const uint AUX1 = 0x040A;
        public const uint AUX2 = 0x040B;
        public const uint AUX3 = 0x040C;
        public const uint NO_ERROR = 0;
        public const uint INVALID_ENUM = 0x0500;
        public const uint INVALID_VALUE = 0x0501;
        public const uint INVALID_OPERATION = 0x0502;
        public const uint STACK_OVERFLOW = 0x0503;
        public const uint STACK_UNDERFLOW = 0x0504;
        public const uint OUT_OF_MEMORY = 0x0505;
        public const uint _2D = 0x0600;
        public const uint _3D = 0x0601;
        public const uint _3D_COLOR = 0x0602;
        public const uint _3D_COLOR_TEXTURE = 0x0603;
        public const uint _4D_COLOR_TEXTURE = 0x0604;
        public const uint PASS_THROUGH_TOKEN = 0x0700;
        public const uint POINT_TOKEN = 0x0701;
        public const uint LINE_TOKEN = 0x0702;
        public const uint POLYGON_TOKEN = 0x0703;
        public const uint BITMAP_TOKEN = 0x0704;
        public const uint DRAW_PIXEL_TOKEN = 0x0705;
        public const uint COPY_PIXEL_TOKEN = 0x0706;
        public const uint LINE_RESET_TOKEN = 0x0707;
        public const uint EXP = 0x0800;
        public const uint EXP2 = 0x0801;
        public const uint CW = 0x0900;
        public const uint CCW = 0x0901;
        public const uint COEFF = 0x0A00;
        public const uint ORDER = 0x0A01;
        public const uint DOMAIN = 0x0A02;
        public const uint CURRENT_COLOR = 0x0B00;
        public const uint CURRENT_INDEX = 0x0B01;
        public const uint CURRENT_NORMAL = 0x0B02;
        public const uint CURRENT_TEXTURE_COORDS = 0x0B03;
        public const uint CURRENT_RASTER_COLOR = 0x0B04;
        public const uint CURRENT_RASTER_INDEX = 0x0B05;
        public const uint CURRENT_RASTER_TEXTURE_COORDS = 0x0B06;
        public const uint CURRENT_RASTER_POSITION = 0x0B07;
        public const uint CURRENT_RASTER_POSITION_VALID = 0x0B08;
        public const uint CURRENT_RASTER_DISTANCE = 0x0B09;
        public const uint POINT_SMOOTH = 0x0B10;
        public const uint POINT_SIZE = 0x0B11;
        public const uint POINT_SIZE_RANGE = 0x0B12;
        public const uint POINT_SIZE_GRANULARITY = 0x0B13;
        public const uint LINE_SMOOTH = 0x0B20;
        public const uint LINE_WIDTH = 0x0B21;
        public const uint LINE_WIDTH_RANGE = 0x0B22;
        public const uint LINE_WIDTH_GRANULARITY = 0x0B23;
        public const uint LINE_STIPPLE = 0x0B24;
        public const uint LINE_STIPPLE_PATTERN = 0x0B25;
        public const uint LINE_STIPPLE_REPEAT = 0x0B26;
        public const uint LIST_MODE = 0x0B30;
        public const uint MAX_LIST_NESTING = 0x0B31;
        public const uint LIST_BASE = 0x0B32;
        public const uint LIST_INDEX = 0x0B33;
        public const uint POLYGON_MODE = 0x0B40;
        public const uint POLYGON_SMOOTH = 0x0B41;
        public const uint POLYGON_STIPPLE = 0x0B42;
        public const uint EDGE_FLAG = 0x0B43;
        public const uint CULL_FACE = 0x0B44;
        public const uint CULL_FACE_MODE = 0x0B45;
        public const uint FRONT_FACE = 0x0B46;
        public const uint LIGHTING = 0x0B50;
        public const uint LIGHT_MODEL_LOCAL_VIEWER = 0x0B51;
        public const uint LIGHT_MODEL_TWO_SIDE = 0x0B52;
        public const uint LIGHT_MODEL_AMBIENT = 0x0B53;
        public const uint SHADE_MODEL = 0x0B54;
        public const uint COLOR_MATERIAL_FACE = 0x0B55;
        public const uint COLOR_MATERIAL_PARAMETER = 0x0B56;
        public const uint COLOR_MATERIAL = 0x0B57;
        public const uint FOG = 0x0B60;
        public const uint FOG_INDEX = 0x0B61;
        public const uint FOG_DENSITY = 0x0B62;
        public const uint FOG_START = 0x0B63;
        public const uint FOG_END = 0x0B64;
        public const uint FOG_MODE = 0x0B65;
        public const uint FOG_COLOR = 0x0B66;
        public const uint DEPTH_RANGE = 0x0B70;
        public const uint DEPTH_TEST = 0x0B71;
        public const uint DEPTH_WRITEMASK = 0x0B72;
        public const uint DEPTH_CLEAR_VALUE = 0x0B73;
        public const uint DEPTH_FUNC = 0x0B74;
        public const uint ACCUM_CLEAR_VALUE = 0x0B80;
        public const uint STENCIL_TEST = 0x0B90;
        public const uint STENCIL_CLEAR_VALUE = 0x0B91;
        public const uint STENCIL_FUNC = 0x0B92;
        public const uint STENCIL_VALUE_MASK = 0x0B93;
        public const uint STENCIL_FAIL = 0x0B94;
        public const uint STENCIL_PASS_DEPTH_FAIL = 0x0B95;
        public const uint STENCIL_PASS_DEPTH_PASS = 0x0B96;
        public const uint STENCIL_REF = 0x0B97;
        public const uint STENCIL_WRITEMASK = 0x0B98;
        public const uint MATRIX_MODE = 0x0BA0;
        public const uint NORMALIZE = 0x0BA1;
        public const uint VIEWPORT = 0x0BA2;
        public const uint MODELVIEW_STACK_DEPTH = 0x0BA3;
        public const uint PROJECTION_STACK_DEPTH = 0x0BA4;
        public const uint TEXTURE_STACK_DEPTH = 0x0BA5;
        public const uint MODELVIEW_MATRIX = 0x0BA6;
        public const uint PROJECTION_MATRIX = 0x0BA7;
        public const uint TEXTURE_MATRIX = 0x0BA8;
        public const uint ATTRIB_STACK_DEPTH = 0x0BB0;
        public const uint CLIENT_ATTRIB_STACK_DEPTH = 0x0BB1;
        public const uint ALPHA_TEST = 0x0BC0;
        public const uint ALPHA_TEST_FUNC = 0x0BC1;
        public const uint ALPHA_TEST_REF = 0x0BC2;
        public const uint DITHER = 0x0BD0;
        public const uint BLEND_DST = 0x0BE0;
        public const uint BLEND_SRC = 0x0BE1;
        public const uint BLEND = 0x0BE2;
        public const uint LOGIC_OP_MODE = 0x0BF0;
        public const uint INDEX_LOGIC_OP = 0x0BF1;
        public const uint COLOR_LOGIC_OP = 0x0BF2;
        public const uint AUX_BUFFERS = 0x0C00;
        public const uint DRAW_BUFFER = 0x0C01;
        public const uint READ_BUFFER = 0x0C02;
        public const uint SCISSOR_BOX = 0x0C10;
        public const uint SCISSOR_TEST = 0x0C11;
        public const uint INDEX_CLEAR_VALUE = 0x0C20;
        public const uint INDEX_WRITEMASK = 0x0C21;
        public const uint COLOR_CLEAR_VALUE = 0x0C22;
        public const uint COLOR_WRITEMASK = 0x0C23;
        public const uint INDEX_MODE = 0x0C30;
        public const uint RGBA_MODE = 0x0C31;
        public const uint DOUBLEBUFFER = 0x0C32;
        public const uint STEREO = 0x0C33;
        public const uint RENDER_MODE = 0x0C40;
        public const uint PERSPECTIVE_CORRECTION_HINT = 0x0C50;
        public const uint POINT_SMOOTH_HINT = 0x0C51;
        public const uint LINE_SMOOTH_HINT = 0x0C52;
        public const uint POLYGON_SMOOTH_HINT = 0x0C53;
        public const uint FOG_HINT = 0x0C54;
        public const uint TEXTURE_GEN_S = 0x0C60;
        public const uint TEXTURE_GEN_T = 0x0C61;
        public const uint TEXTURE_GEN_R = 0x0C62;
        public const uint TEXTURE_GEN_Q = 0x0C63;
        public const uint PIXEL_MAP_I_TO_I = 0x0C70;
        public const uint PIXEL_MAP_S_TO_S = 0x0C71;
        public const uint PIXEL_MAP_I_TO_R = 0x0C72;
        public const uint PIXEL_MAP_I_TO_G = 0x0C73;
        public const uint PIXEL_MAP_I_TO_B = 0x0C74;
        public const uint PIXEL_MAP_I_TO_A = 0x0C75;
        public const uint PIXEL_MAP_R_TO_R = 0x0C76;
        public const uint PIXEL_MAP_G_TO_G = 0x0C77;
        public const uint PIXEL_MAP_B_TO_B = 0x0C78;
        public const uint PIXEL_MAP_A_TO_A = 0x0C79;
        public const uint PIXEL_MAP_I_TO_I_SIZE = 0x0CB0;
        public const uint PIXEL_MAP_S_TO_S_SIZE = 0x0CB1;
        public const uint PIXEL_MAP_I_TO_R_SIZE = 0x0CB2;
        public const uint PIXEL_MAP_I_TO_G_SIZE = 0x0CB3;
        public const uint PIXEL_MAP_I_TO_B_SIZE = 0x0CB4;
        public const uint PIXEL_MAP_I_TO_A_SIZE = 0x0CB5;
        public const uint PIXEL_MAP_R_TO_R_SIZE = 0x0CB6;
        public const uint PIXEL_MAP_G_TO_G_SIZE = 0x0CB7;
        public const uint PIXEL_MAP_B_TO_B_SIZE = 0x0CB8;
        public const uint PIXEL_MAP_A_TO_A_SIZE = 0x0CB9;
        public const uint UNPACK_SWAP_BYTES = 0x0CF0;
        public const uint UNPACK_LSB_FIRST = 0x0CF1;
        public const uint UNPACK_ROW_LENGTH = 0x0CF2;
        public const uint UNPACK_SKIP_ROWS = 0x0CF3;
        public const uint UNPACK_SKIP_PIXELS = 0x0CF4;
        public const uint UNPACK_ALIGNMENT = 0x0CF5;
        public const uint PACK_SWAP_BYTES = 0x0D00;
        public const uint PACK_LSB_FIRST = 0x0D01;
        public const uint PACK_ROW_LENGTH = 0x0D02;
        public const uint PACK_SKIP_ROWS = 0x0D03;
        public const uint PACK_SKIP_PIXELS = 0x0D04;
        public const uint PACK_ALIGNMENT = 0x0D05;
        public const uint MAP_COLOR = 0x0D10;
        public const uint MAP_STENCIL = 0x0D11;
        public const uint INDEX_SHIFT = 0x0D12;
        public const uint INDEX_OFFSET = 0x0D13;
        public const uint RED_SCALE = 0x0D14;
        public const uint RED_BIAS = 0x0D15;
        public const uint ZOOM_X = 0x0D16;
        public const uint ZOOM_Y = 0x0D17;
        public const uint GREEN_SCALE = 0x0D18;
        public const uint GREEN_BIAS = 0x0D19;
        public const uint BLUE_SCALE = 0x0D1A;
        public const uint BLUE_BIAS = 0x0D1B;
        public const uint ALPHA_SCALE = 0x0D1C;
        public const uint ALPHA_BIAS = 0x0D1D;
        public const uint DEPTH_SCALE = 0x0D1E;
        public const uint DEPTH_BIAS = 0x0D1F;
        public const uint MAX_EVAL_ORDER = 0x0D30;
        public const uint MAX_LIGHTS = 0x0D31;
        public const uint MAX_CLIP_PLANES = 0x0D32;
        public const uint MAX_TEXTURE_SIZE = 0x0D33;
        public const uint MAX_PIXEL_MAP_TABLE = 0x0D34;
        public const uint MAX_ATTRIB_STACK_DEPTH = 0x0D35;
        public const uint MAX_MODELVIEW_STACK_DEPTH = 0x0D36;
        public const uint MAX_NAME_STACK_DEPTH = 0x0D37;
        public const uint MAX_PROJECTION_STACK_DEPTH = 0x0D38;
        public const uint MAX_TEXTURE_STACK_DEPTH = 0x0D39;
        public const uint MAX_VIEWPORT_DIMS = 0x0D3A;
        public const uint MAX_CLIENT_ATTRIB_STACK_DEPTH = 0x0D3B;
        public const uint SUBPIXEL_BITS = 0x0D50;
        public const uint INDEX_BITS = 0x0D51;
        public const uint RED_BITS = 0x0D52;
        public const uint GREEN_BITS = 0x0D53;
        public const uint BLUE_BITS = 0x0D54;
        public const uint ALPHA_BITS = 0x0D55;
        public const uint DEPTH_BITS = 0x0D56;
        public const uint STENCIL_BITS = 0x0D57;
        public const uint ACCUM_RED_BITS = 0x0D58;
        public const uint ACCUM_GREEN_BITS = 0x0D59;
        public const uint ACCUM_BLUE_BITS = 0x0D5A;
        public const uint ACCUM_ALPHA_BITS = 0x0D5B;
        public const uint NAME_STACK_DEPTH = 0x0D70;
        public const uint AUTO_NORMAL = 0x0D80;
        public const uint MAP1_COLOR_4 = 0x0D90;
        public const uint MAP1_INDEX = 0x0D91;
        public const uint MAP1_NORMAL = 0x0D92;
        public const uint MAP1_TEXTURE_COORD_1 = 0x0D93;
        public const uint MAP1_TEXTURE_COORD_2 = 0x0D94;
        public const uint MAP1_TEXTURE_COORD_3 = 0x0D95;
        public const uint MAP1_TEXTURE_COORD_4 = 0x0D96;
        public const uint MAP1_VERTEX_3 = 0x0D97;
        public const uint MAP1_VERTEX_4 = 0x0D98;
        public const uint MAP2_COLOR_4 = 0x0DB0;
        public const uint MAP2_INDEX = 0x0DB1;
        public const uint MAP2_NORMAL = 0x0DB2;
        public const uint MAP2_TEXTURE_COORD_1 = 0x0DB3;
        public const uint MAP2_TEXTURE_COORD_2 = 0x0DB4;
        public const uint MAP2_TEXTURE_COORD_3 = 0x0DB5;
        public const uint MAP2_TEXTURE_COORD_4 = 0x0DB6;
        public const uint MAP2_VERTEX_3 = 0x0DB7;
        public const uint MAP2_VERTEX_4 = 0x0DB8;
        public const uint MAP1_GRID_DOMAIN = 0x0DD0;
        public const uint MAP1_GRID_SEGMENTS = 0x0DD1;
        public const uint MAP2_GRID_DOMAIN = 0x0DD2;
        public const uint MAP2_GRID_SEGMENTS = 0x0DD3;
        public const uint TEXTURE_1D = 0x0DE0;
        public const uint TEXTURE_2D = 0x0DE1;
        public const uint FEEDBACK_BUFFER_POINTER = 0x0DF0;
        public const uint FEEDBACK_BUFFER_SIZE = 0x0DF1;
        public const uint FEEDBACK_BUFFER_TYPE = 0x0DF2;
        public const uint SELECTION_BUFFER_POINTER = 0x0DF3;
        public const uint SELECTION_BUFFER_SIZE = 0x0DF4;
        public const uint TEXTURE_WIDTH = 0x1000;
        public const uint TEXTURE_HEIGHT = 0x1001;
        public const uint TEXTURE_INTERNAL_FORMAT = 0x1003;
        public const uint TEXTURE_BORDER_COLOR = 0x1004;
        public const uint TEXTURE_BORDER = 0x1005;
        public const uint DONT_CARE = 0x1100;
        public const uint FASTEST = 0x1101;
        public const uint NICEST = 0x1102;
        public const uint LIGHT0 = 0x4000;
        public const uint LIGHT1 = 0x4001;
        public const uint LIGHT2 = 0x4002;
        public const uint LIGHT3 = 0x4003;
        public const uint LIGHT4 = 0x4004;
        public const uint LIGHT5 = 0x4005;
        public const uint LIGHT6 = 0x4006;
        public const uint LIGHT7 = 0x4007;
        public const uint AMBIENT = 0x1200;
        public const uint DIFFUSE = 0x1201;
        public const uint SPECULAR = 0x1202;
        public const uint POSITION = 0x1203;
        public const uint SPOT_DIRECTION = 0x1204;
        public const uint SPOT_EXPONENT = 0x1205;
        public const uint SPOT_CUTOFF = 0x1206;
        public const uint CONSTANT_ATTENUATION = 0x1207;
        public const uint LINEAR_ATTENUATION = 0x1208;
        public const uint QUADRATIC_ATTENUATION = 0x1209;
        public const uint COMPILE = 0x1300;
        public const uint COMPILE_AND_EXECUTE = 0x1301;
        public const uint CLEAR = 0x1500;
        public const uint AND = 0x1501;
        public const uint AND_REVERSE = 0x1502;
        public const uint COPY = 0x1503;
        public const uint AND_INVERTED = 0x1504;
        public const uint NOOP = 0x1505;
        public const uint XOR = 0x1506;
        public const uint OR = 0x1507;
        public const uint NOR = 0x1508;
        public const uint EQUIV = 0x1509;
        public const uint INVERT = 0x150A;
        public const uint OR_REVERSE = 0x150B;
        public const uint COPY_INVERTED = 0x150C;
        public const uint OR_INVERTED = 0x150D;
        public const uint NAND = 0x150E;
        public const uint SET = 0x150F;
        public const uint EMISSION = 0x1600;
        public const uint SHININESS = 0x1601;
        public const uint AMBIENT_AND_DIFFUSE = 0x1602;
        public const uint COLOR_INDEXES = 0x1603;
        public const uint MODELVIEW = 0x1700;
        public const uint PROJECTION = 0x1701;
        public const uint TEXTURE = 0x1702;
        public const uint COLOR = 0x1800;
        public const uint DEPTH = 0x1801;
        public const uint STENCIL = 0x1802;
        public const uint COLOR_INDEX = 0x1900;
        public const uint STENCIL_INDEX = 0x1901;
        public const uint DEPTH_COMPONENT = 0x1902;
        public const uint RED = 0x1903;
        public const uint GREEN = 0x1904;
        public const uint BLUE = 0x1905;
        public const uint ALPHA = 0x1906;
        public const uint RGB = 0x1907;
        public const uint RGBA = 0x1908;
        public const uint LUMINANCE = 0x1909;
        public const uint LUMINANCE_ALPHA = 0x190A;
        public const uint BITMAP = 0x1A00;
        public const uint POINT = 0x1B00;
        public const uint LINE = 0x1B01;
        public const uint FILL = 0x1B02;
        public const uint RENDER = 0x1C00;
        public const uint FEEDBACK = 0x1C01;
        public const uint SELECT = 0x1C02;
        public const uint FLAT = 0x1D00;
        public const uint SMOOTH = 0x1D01;
        public const uint KEEP = 0x1E00;
        public const uint REPLACE = 0x1E01;
        public const uint INCR = 0x1E02;
        public const uint DECR = 0x1E03;
        public const uint VENDOR = 0x1F00;
        public const uint RENDERER = 0x1F01;
        public const uint VERSION = 0x1F02;
        public const uint EXTENSIONS = 0x1F03;
        public const uint S = 0x2000;
        public const uint T = 0x2001;
        public const uint R = 0x2002;
        public const uint Q = 0x2003;
        public const uint MODULATE = 0x2100;
        public const uint DECAL = 0x2101;
        public const uint TEXTURE_ENV_MODE = 0x2200;
        public const uint TEXTURE_ENV_COLOR = 0x2201;
        public const uint TEXTURE_ENV = 0x2300;
        public const uint EYE_LINEAR = 0x2400;
        public const uint OBJECT_LINEAR = 0x2401;
        public const uint SPHERE_MAP = 0x2402;
        public const uint TEXTURE_GEN_MODE = 0x2500;
        public const uint OBJECT_PLANE = 0x2501;
        public const uint EYE_PLANE = 0x2502;
        public const uint NEAREST = 0x2600;
        public const uint LINEAR = 0x2601;
        public const uint NEAREST_MIPMAP_NEAREST = 0x2700;
        public const uint LINEAR_MIPMAP_NEAREST = 0x2701;
        public const uint NEAREST_MIPMAP_LINEAR = 0x2702;
        public const uint LINEAR_MIPMAP_LINEAR = 0x2703;
        public const uint TEXTURE_MAG_FILTER = 0x2800;
        public const uint TEXTURE_MIN_FILTER = 0x2801;
        public const uint TEXTURE_WRAP_S = 0x2802;
        public const uint TEXTURE_WRAP_T = 0x2803;
        public const uint CLAMP = 0x2900;
        public const uint REPEAT = 0x2901;
        public const uint CLIENT_PIXEL_STORE_BIT = 0x00000001;
        public const uint CLIENT_VERTEX_ARRAY_BIT = 0x00000002;
        public const uint CLIENT_ALL_ATTRIB_BITS = 0xffffffff;
        public const uint POLYGON_OFFSET_FACTOR = 0x8038;
        public const uint POLYGON_OFFSET_UNITS = 0x2A00;
        public const uint POLYGON_OFFSET_POINT = 0x2A01;
        public const uint POLYGON_OFFSET_LINE = 0x2A02;
        public const uint POLYGON_OFFSET_FILL = 0x8037;
        public const uint ALPHA4 = 0x803B;
        public const uint ALPHA8 = 0x803C;
        public const uint ALPHA12 = 0x803D;
        public const uint ALPHA16 = 0x803E;
        public const uint LUMINANCE4 = 0x803F;
        public const uint LUMINANCE8 = 0x8040;
        public const uint LUMINANCE12 = 0x8041;
        public const uint LUMINANCE16 = 0x8042;
        public const uint LUMINANCE4_ALPHA4 = 0x8043;
        public const uint LUMINANCE6_ALPHA2 = 0x8044;
        public const uint LUMINANCE8_ALPHA8 = 0x8045;
        public const uint LUMINANCE12_ALPHA4 = 0x8046;
        public const uint LUMINANCE12_ALPHA12 = 0x8047;
        public const uint LUMINANCE16_ALPHA16 = 0x8048;
        public const uint INTENSITY = 0x8049;
        public const uint INTENSITY4 = 0x804A;
        public const uint INTENSITY8 = 0x804B;
        public const uint INTENSITY12 = 0x804C;
        public const uint INTENSITY16 = 0x804D;
        public const uint R3_G3_B2 = 0x2A10;
        public const uint RGB4 = 0x804F;
        public const uint RGB5 = 0x8050;
        public const uint RGB8 = 0x8051;
        public const uint RGB10 = 0x8052;
        public const uint RGB12 = 0x8053;
        public const uint RGB16 = 0x8054;
        public const uint RGBA2 = 0x8055;
        public const uint RGBA4 = 0x8056;
        public const uint RGB5_A1 = 0x8057;
        public const uint RGBA8 = 0x8058;
        public const uint RGB10_A2 = 0x8059;
        public const uint RGBA12 = 0x805A;
        public const uint RGBA16 = 0x805B;
        public const uint TEXTURE_RED_SIZE = 0x805C;
        public const uint TEXTURE_GREEN_SIZE = 0x805D;
        public const uint TEXTURE_BLUE_SIZE = 0x805E;
        public const uint TEXTURE_ALPHA_SIZE = 0x805F;
        public const uint TEXTURE_LUMINANCE_SIZE = 0x8060;
        public const uint TEXTURE_INTENSITY_SIZE = 0x8061;
        public const uint PROXY_TEXTURE_1D = 0x8063;
        public const uint PROXY_TEXTURE_2D = 0x8064;
        public const uint TEXTURE_PRIORITY = 0x8066;
        public const uint TEXTURE_RESIDENT = 0x8067;
        public const uint TEXTURE_BINDING_1D = 0x8068;
        public const uint TEXTURE_BINDING_2D = 0x8069;
        public const uint TEXTURE_BINDING_3D = 0x806A;
        public const uint VERTEX_ARRAY = 0x8074;
        public const uint NORMAL_ARRAY = 0x8075;
        public const uint COLOR_ARRAY = 0x8076;
        public const uint INDEX_ARRAY = 0x8077;
        public const uint TEXTURE_COORD_ARRAY = 0x8078;
        public const uint EDGE_FLAG_ARRAY = 0x8079;
        public const uint VERTEX_ARRAY_SIZE = 0x807A;
        public const uint VERTEX_ARRAY_TYPE = 0x807B;
        public const uint VERTEX_ARRAY_STRIDE = 0x807C;
        public const uint NORMAL_ARRAY_TYPE = 0x807E;
        public const uint NORMAL_ARRAY_STRIDE = 0x807F;
        public const uint COLOR_ARRAY_SIZE = 0x8081;
        public const uint COLOR_ARRAY_TYPE = 0x8082;
        public const uint COLOR_ARRAY_STRIDE = 0x8083;
        public const uint INDEX_ARRAY_TYPE = 0x8085;
        public const uint INDEX_ARRAY_STRIDE = 0x8086;
        public const uint TEXTURE_COORD_ARRAY_SIZE = 0x8088;
        public const uint TEXTURE_COORD_ARRAY_TYPE = 0x8089;
        public const uint TEXTURE_COORD_ARRAY_STRIDE = 0x808A;
        public const uint EDGE_FLAG_ARRAY_STRIDE = 0x808C;
        public const uint VERTEX_ARRAY_POINTER = 0x808E;
        public const uint NORMAL_ARRAY_POINTER = 0x808F;
        public const uint COLOR_ARRAY_POINTER = 0x8090;
        public const uint INDEX_ARRAY_POINTER = 0x8091;
        public const uint TEXTURE_COORD_ARRAY_POINTER = 0x8092;
        public const uint EDGE_FLAG_ARRAY_POINTER = 0x8093;
        public const uint V2F = 0x2A20;
        public const uint V3F = 0x2A21;
        public const uint C4UB_V2F = 0x2A22;
        public const uint C4UB_V3F = 0x2A23;
        public const uint C3F_V3F = 0x2A24;
        public const uint N3F_V3F = 0x2A25;
        public const uint C4F_N3F_V3F = 0x2A26;
        public const uint T2F_V3F = 0x2A27;
        public const uint T4F_V4F = 0x2A28;
        public const uint T2F_C4UB_V3F = 0x2A29;
        public const uint T2F_C3F_V3F = 0x2A2A;
        public const uint T2F_N3F_V3F = 0x2A2B;
        public const uint T2F_C4F_N3F_V3F = 0x2A2C;
        public const uint T4F_C4F_N3F_V4F = 0x2A2D;
        public const uint BGR = 0x80E0;
        public const uint BGRA = 0x80E1;
        public const uint CONSTANT_COLOR = 0x8001;
        public const uint ONE_MINUS_CONSTANT_COLOR = 0x8002;
        public const uint CONSTANT_ALPHA = 0x8003;
        public const uint ONE_MINUS_CONSTANT_ALPHA = 0x8004;
        public const uint BLEND_COLOR = 0x8005;
        public const uint FUNC_ADD = 0x8006;
        public const uint MIN = 0x8007;
        public const uint MAX = 0x8008;
        public const uint BLEND_EQUATION = 0x8009;
        public const uint FUNC_SUBTRACT = 0x800A;
        public const uint FUNC_REVERSE_SUBTRACT = 0x800B;
        public const uint COLOR_MATRIX = 0x80B1;
        public const uint COLOR_MATRIX_STACK_DEPTH = 0x80B2;
        public const uint MAX_COLOR_MATRIX_STACK_DEPTH = 0x80B3;
        public const uint POST_COLOR_MATRIX_RED_SCALE = 0x80B4;
        public const uint POST_COLOR_MATRIX_GREEN_SCALE = 0x80B5;
        public const uint POST_COLOR_MATRIX_BLUE_SCALE = 0x80B6;
        public const uint POST_COLOR_MATRIX_ALPHA_SCALE = 0x80B7;
        public const uint POST_COLOR_MATRIX_RED_BIAS = 0x80B8;
        public const uint POST_COLOR_MATRIX_GREEN_BIAS = 0x80B9;
        public const uint POST_COLOR_MATRIX_BLUE_BIAS = 0x80BA;
        public const uint POST_COLOR_MATRIX_ALPHA_BIAS = 0x80BB;
        public const uint COLOR_TABLE = 0x80D0;
        public const uint POST_CONVOLUTION_COLOR_TABLE = 0x80D1;
        public const uint POST_COLOR_MATRIX_COLOR_TABLE = 0x80D2;
        public const uint PROXY_COLOR_TABLE = 0x80D3;
        public const uint PROXY_POST_CONVOLUTION_COLOR_TABLE = 0x80D4;
        public const uint PROXY_POST_COLOR_MATRIX_COLOR_TABLE = 0x80D5;
        public const uint COLOR_TABLE_SCALE = 0x80D6;
        public const uint COLOR_TABLE_BIAS = 0x80D7;
        public const uint COLOR_TABLE_FORMAT = 0x80D8;
        public const uint COLOR_TABLE_WIDTH = 0x80D9;
        public const uint COLOR_TABLE_RED_SIZE = 0x80DA;
        public const uint COLOR_TABLE_GREEN_SIZE = 0x80DB;
        public const uint COLOR_TABLE_BLUE_SIZE = 0x80DC;
        public const uint COLOR_TABLE_ALPHA_SIZE = 0x80DD;
        public const uint COLOR_TABLE_LUMINANCE_SIZE = 0x80DE;
        public const uint COLOR_TABLE_INTENSITY_SIZE = 0x80DF;
        public const uint CONVOLUTION_1D = 0x8010;
        public const uint CONVOLUTION_2D = 0x8011;
        public const uint SEPARABLE_2D = 0x8012;
        public const uint CONVOLUTION_BORDER_MODE = 0x8013;
        public const uint CONVOLUTION_FILTER_SCALE = 0x8014;
        public const uint CONVOLUTION_FILTER_BIAS = 0x8015;
        public const uint REDUCE = 0x8016;
        public const uint CONVOLUTION_FORMAT = 0x8017;
        public const uint CONVOLUTION_WIDTH = 0x8018;
        public const uint CONVOLUTION_HEIGHT = 0x8019;
        public const uint MAX_CONVOLUTION_WIDTH = 0x801A;
        public const uint MAX_CONVOLUTION_HEIGHT = 0x801B;
        public const uint POST_CONVOLUTION_RED_SCALE = 0x801C;
        public const uint POST_CONVOLUTION_GREEN_SCALE = 0x801D;
        public const uint POST_CONVOLUTION_BLUE_SCALE = 0x801E;
        public const uint POST_CONVOLUTION_ALPHA_SCALE = 0x801F;
        public const uint POST_CONVOLUTION_RED_BIAS = 0x8020;
        public const uint POST_CONVOLUTION_GREEN_BIAS = 0x8021;
        public const uint POST_CONVOLUTION_BLUE_BIAS = 0x8022;
        public const uint POST_CONVOLUTION_ALPHA_BIAS = 0x8023;
        public const uint CONSTANT_BORDER = 0x8151;
        public const uint REPLICATE_BORDER = 0x8153;
        public const uint CONVOLUTION_BORDER_COLOR = 0x8154;
        public const uint MAX_ELEMENTS_VERTICES = 0x80E8;
        public const uint MAX_ELEMENTS_INDICES = 0x80E9;
        public const uint HISTOGRAM = 0x8024;
        public const uint PROXY_HISTOGRAM = 0x8025;
        public const uint HISTOGRAM_WIDTH = 0x8026;
        public const uint HISTOGRAM_FORMAT = 0x8027;
        public const uint HISTOGRAM_RED_SIZE = 0x8028;
        public const uint HISTOGRAM_GREEN_SIZE = 0x8029;
        public const uint HISTOGRAM_BLUE_SIZE = 0x802A;
        public const uint HISTOGRAM_ALPHA_SIZE = 0x802B;
        public const uint HISTOGRAM_LUMINANCE_SIZE = 0x802C;
        public const uint HISTOGRAM_SINK = 0x802D;
        public const uint MINMAX = 0x802E;
        public const uint MINMAX_FORMAT = 0x802F;
        public const uint MINMAX_SINK = 0x8030;
        public const uint TABLE_TOO_LARGE = 0x8031;
        public const uint UNSIGNED_BYTE_3_3_2 = 0x8032;
        public const uint UNSIGNED_SHORT_4_4_4_4 = 0x8033;
        public const uint UNSIGNED_SHORT_5_5_5_1 = 0x8034;
        public const uint UNSIGNED_INT_8_8_8_8 = 0x8035;
        public const uint UNSIGNED_INT_10_10_10_2 = 0x8036;
        public const uint UNSIGNED_BYTE_2_3_3_REV = 0x8362;
        public const uint UNSIGNED_SHORT_5_6_5 = 0x8363;
        public const uint UNSIGNED_SHORT_5_6_5_REV = 0x8364;
        public const uint UNSIGNED_SHORT_4_4_4_4_REV = 0x8365;
        public const uint UNSIGNED_SHORT_1_5_5_5_REV = 0x8366;
        public const uint UNSIGNED_INT_8_8_8_8_REV = 0x8367;
        public const uint UNSIGNED_INT_2_10_10_10_REV = 0x8368;
        public const uint RESCALE_NORMAL = 0x803A;
        public const uint LIGHT_MODEL_COLOR_CONTROL = 0x81F8;
        public const uint SINGLE_COLOR = 0x81F9;
        public const uint SEPARATE_SPECULAR_COLOR = 0x81FA;
        public const uint PACK_SKIP_IMAGES = 0x806B;
        public const uint PACK_IMAGE_HEIGHT = 0x806C;
        public const uint UNPACK_SKIP_IMAGES = 0x806D;
        public const uint UNPACK_IMAGE_HEIGHT = 0x806E;
        public const uint TEXTURE_3D = 0x806F;
        public const uint PROXY_TEXTURE_3D = 0x8070;
        public const uint TEXTURE_DEPTH = 0x8071;
        public const uint TEXTURE_WRAP_R = 0x8072;
        public const uint MAX_3D_TEXTURE_SIZE = 0x8073;
        public const uint CLAMP_TO_EDGE = 0x812F;
        public const uint TEXTURE_MIN_LOD = 0x813A;
        public const uint TEXTURE_MAX_LOD = 0x813B;
        public const uint TEXTURE_BASE_LEVEL = 0x813C;
        public const uint TEXTURE_MAX_LEVEL = 0x813D;
        public const uint SMOOTH_POINT_SIZE_RANGE = 0x0B12;
        public const uint SMOOTH_POINT_SIZE_GRANULARITY = 0x0B13;
        public const uint SMOOTH_LINE_WIDTH_RANGE = 0x0B22;
        public const uint SMOOTH_LINE_WIDTH_GRANULARITY = 0x0B23;
        public const uint ALIASED_POINT_SIZE_RANGE = 0x846D;
        public const uint ALIASED_LINE_WIDTH_RANGE = 0x846E;
        public const uint TEXTURE0_ARB = 0x84C0;
        public const uint TEXTURE1_ARB = 0x84C1;
        public const uint TEXTURE2_ARB = 0x84C2;
        public const uint TEXTURE3_ARB = 0x84C3;
        public const uint TEXTURE4_ARB = 0x84C4;
        public const uint TEXTURE5_ARB = 0x84C5;
        public const uint TEXTURE6_ARB = 0x84C6;
        public const uint TEXTURE7_ARB = 0x84C7;
        public const uint TEXTURE8_ARB = 0x84C8;
        public const uint TEXTURE9_ARB = 0x84C9;
        public const uint TEXTURE10_ARB = 0x84CA;
        public const uint TEXTURE11_ARB = 0x84CB;
        public const uint TEXTURE12_ARB = 0x84CC;
        public const uint TEXTURE13_ARB = 0x84CD;
        public const uint TEXTURE14_ARB = 0x84CE;
        public const uint TEXTURE15_ARB = 0x84CF;
        public const uint TEXTURE16_ARB = 0x84D0;
        public const uint TEXTURE17_ARB = 0x84D1;
        public const uint TEXTURE18_ARB = 0x84D2;
        public const uint TEXTURE19_ARB = 0x84D3;
        public const uint TEXTURE20_ARB = 0x84D4;
        public const uint TEXTURE21_ARB = 0x84D5;
        public const uint TEXTURE22_ARB = 0x84D6;
        public const uint TEXTURE23_ARB = 0x84D7;
        public const uint TEXTURE24_ARB = 0x84D8;
        public const uint TEXTURE25_ARB = 0x84D9;
        public const uint TEXTURE26_ARB = 0x84DA;
        public const uint TEXTURE27_ARB = 0x84DB;
        public const uint TEXTURE28_ARB = 0x84DC;
        public const uint TEXTURE29_ARB = 0x84DD;
        public const uint TEXTURE30_ARB = 0x84DE;
        public const uint TEXTURE31_ARB = 0x84DF;
        public const uint ACTIVE_TEXTURE_ARB = 0x84E0;
        public const uint CLIENT_ACTIVE_TEXTURE_ARB = 0x84E1;
        public const uint MAX_TEXTURE_UNITS_ARB = 0x84E2;
        public const uint ABGR_EXT = 0x8000;
        public const uint CONSTANT_COLOR_EXT = 0x8001;
        public const uint ONE_MINUS_CONSTANT_COLOR_EXT = 0x8002;
        public const uint CONSTANT_ALPHA_EXT = 0x8003;
        public const uint ONE_MINUS_CONSTANT_ALPHA_EXT = 0x8004;
        public const uint BLEND_COLOR_EXT = 0x8005;
        public const uint FUNC_ADD_EXT = 0x8006;
        public const uint MIN_EXT = 0x8007;
        public const uint MAX_EXT = 0x8008;
        public const uint BLEND_EQUATION_EXT = 0x8009;
        public const uint FUNC_SUBTRACT_EXT = 0x800A;
        public const uint FUNC_REVERSE_SUBTRACT_EXT = 0x800B;
        public const uint COMBINE_EXT = 0x8570;
        public const uint COMBINE_RGB_EXT = 0x8571;
        public const uint COMBINE_ALPHA_EXT = 0x8572;
        public const uint RGB_SCALE_EXT = 0x8573;
        public const uint ADD_SIGNED_EXT = 0x8574;
        public const uint INTERPOLATE_EXT = 0x8575;
        public const uint CONSTANT_EXT = 0x8576;
        public const uint PRIMARY_COLOR_EXT = 0x8577;
        public const uint PREVIOUS_EXT = 0x8578;
        public const uint SOURCE0_RGB_EXT = 0x8580;
        public const uint SOURCE1_RGB_EXT = 0x8581;
        public const uint SOURCE2_RGB_EXT = 0x8582;
        public const uint SOURCE0_ALPHA_EXT = 0x8588;
        public const uint SOURCE1_ALPHA_EXT = 0x8589;
        public const uint SOURCE2_ALPHA_EXT = 0x858A;
        public const uint OPERAND0_RGB_EXT = 0x8590;
        public const uint OPERAND1_RGB_EXT = 0x8591;
        public const uint OPERAND2_RGB_EXT = 0x8592;
        public const uint OPERAND0_ALPHA_EXT = 0x8598;
        public const uint OPERAND1_ALPHA_EXT = 0x8599;
        public const uint OPERAND2_ALPHA_EXT = 0x859A;
        public const uint LOGIC_OP = INDEX_LOGIC_OP;
        public const uint TEXTURE_COMPONENTS = TEXTURE_INTERNAL_FORMAT;
        public const uint ARRAY_BUFFER_ARB = 0x8892;
        public const uint ELEMENT_ARRAY_BUFFER_ARB = 0x8893;
        public const uint ARRAY_BUFFER_BINDING_ARB = 0x8894;
        public const uint ELEMENT_ARRAY_BUFFER_BINDING_ARB = 0x8895;
        public const uint VERTEX_ARRAY_BUFFER_BINDING_ARB = 0x8896;
        public const uint NORMAL_ARRAY_BUFFER_BINDING_ARB = 0x8897;
        public const uint COLOR_ARRAY_BUFFER_BINDING_ARB = 0x8898;
        public const uint INDEX_ARRAY_BUFFER_BINDING_ARB = 0x8899;
        public const uint TEXTURE_COORD_ARRAY_BUFFER_BINDING_ARB = 0x889A;
        public const uint EDGE_FLAG_ARRAY_BUFFER_BINDING_ARB = 0x889B;
        public const uint SECONDARY_COLOR_ARRAY_BUFFER_BINDING_ARB = 0x889C;
        public const uint FOG_COORDINATE_ARRAY_BUFFER_BINDING_ARB = 0x889D;
        public const uint WEIGHT_ARRAY_BUFFER_BINDING_ARB = 0x889E;
        public const uint VERTEX_ATTRIB_ARRAY_BUFFER_BINDING_ARB = 0x889F;
        public const uint STREAM_DRAW_ARB = 0x88E0;
        public const uint STREAM_READ_ARB = 0x88E1;
        public const uint STREAM_COPY_ARB = 0x88E2;
        public const uint STATIC_DRAW_ARB = 0x88E4;
        public const uint STATIC_READ_ARB = 0x88E5;
        public const uint STATIC_COPY_ARB = 0x88E6;
        public const uint DYNAMIC_DRAW_ARB = 0x88E8;
        public const uint DYNAMIC_READ_ARB = 0x88E9;
        public const uint DYNAMIC_COPY_ARB = 0x88EA;
        public const uint READ_ONLY_ARB = 0x88B8;
        public const uint WRITE_ONLY_ARB = 0x88B9;
        public const uint READ_WRITE_ARB = 0x88BA;
        public const uint BUFFER_SIZE_ARB = 0x8764;
        public const uint BUFFER_USAGE_ARB = 0x8765;
        public const uint BUFFER_ACCESS_ARB = 0x88BB;
        public const uint BUFFER_MAPPED_ARB = 0x88BC;
        public const uint BUFFER_MAP_POINTER_ARB = 0x88BD;
        public const uint SAMPLES_PASSED_ARB = 0x8914;
        public const uint QUERY_COUNTER_BITS_ARB = 0x8864;
        public const uint CURRENT_QUERY_ARB = 0x8865;
        public const uint QUERY_RESULT_ARB = 0x8866;
        public const uint QUERY_RESULT_AVAILABLE_ARB = 0x8867;
        public const uint FRAGMENT_PROGRAM_ARB = 0x8804;
        public const uint VERTEX_PROGRAM_ARB = 0x8620;
        public const uint PROGRAM_ERROR_POSITION_ARB = 0x864B;
        public const uint PROGRAM_ERROR_STRING_ARB = 0x8874;
        public const uint VERTEX_PROGRAM_POINT_SIZE_ARB = 0x8642;
        public const uint VERTEX_PROGRAM_TWO_SIDE_ARB = 0x8643;
        public const uint COLOR_SUM_ARB = 0x8458;
        public const uint PROGRAM_FORMAT_ASCII_ARB = 0x8875;
        public const uint PROGRAM_LENGTH_ARB = 0x8627;
        public const uint PROGRAM_FORMAT_ARB = 0x8876;
        public const uint PROGRAM_BINDING_ARB = 0x8677;
        public const uint PROGRAM_INSTRUCTIONS_ARB = 0x88A0;
        public const uint MAX_PROGRAM_INSTRUCTIONS_ARB = 0x88A1;
        public const uint PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A2;
        public const uint MAX_PROGRAM_NATIVE_INSTRUCTIONS_ARB = 0x88A3;
        public const uint PROGRAM_TEMPORARIES_ARB = 0x88A4;
        public const uint MAX_PROGRAM_TEMPORARIES_ARB = 0x88A5;
        public const uint PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A6;
        public const uint MAX_PROGRAM_NATIVE_TEMPORARIES_ARB = 0x88A7;
        public const uint PROGRAM_PARAMETERS_ARB = 0x88A8;
        public const uint MAX_PROGRAM_PARAMETERS_ARB = 0x88A9;
        public const uint PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AA;
        public const uint MAX_PROGRAM_NATIVE_PARAMETERS_ARB = 0x88AB;
        public const uint PROGRAM_ATTRIBS_ARB = 0x88AC;
        public const uint MAX_PROGRAM_ATTRIBS_ARB = 0x88AD;
        public const uint PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AE;
        public const uint MAX_PROGRAM_NATIVE_ATTRIBS_ARB = 0x88AF;
        public const uint PROGRAM_ADDRESS_REGISTERS_ARB = 0x88B0;
        public const uint MAX_PROGRAM_ADDRESS_REGISTERS_ARB = 0x88B1;
        public const uint PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = 0x88B2;
        public const uint MAX_PROGRAM_NATIVE_ADDRESS_REGISTERS_ARB = 0x88B3;
        public const uint MAX_PROGRAM_LOCAL_PARAMETERS_ARB = 0x88B4;
        public const uint MAX_PROGRAM_ENV_PARAMETERS_ARB = 0x88B5;
        public const uint PROGRAM_UNDER_NATIVE_LIMITS_ARB = 0x88B6;
        public const uint VERTEX_ATTRIB_ARRAY_ENABLED_ARB = 0x8622;
        public const uint VERTEX_ATTRIB_ARRAY_SIZE_ARB = 0x8623;
        public const uint VERTEX_ATTRIB_ARRAY_STRIDE_ARB = 0x8624;
        public const uint VERTEX_ATTRIB_ARRAY_TYPE_ARB = 0x8625;
        public const uint VERTEX_ATTRIB_ARRAY_NORMALIZED_ARB = 0x886A;
        public const uint CURRENT_VERTEX_ATTRIB_ARB = 0x8626;
        public const uint VERTEX_ATTRIB_ARRAY_POINTER_ARB = 0x8645;
        public const uint PROGRAM_STRING_ARB = 0x8628;
        public const uint CURRENT_MATRIX_ARB = 0x8641;
        public const uint TRANSPOSE_CURRENT_MATRIX_ARB = 0x88B7;
        public const uint CURRENT_MATRIX_STACK_DEPTH_ARB = 0x8640;
        public const uint MAX_VERTEX_ATTRIBS_ARB = 0x8869;
        public const uint MAX_PROGRAM_MATRICES_ARB = 0x862F;
        public const uint MAX_PROGRAM_MATRIX_STACK_DEPTH_ARB = 0x862E;
        public const uint MATRIX0_ARB = 0x88C0;
        public const uint MATRIX1_ARB = 0x88C1;
        public const uint MATRIX2_ARB = 0x88C2;
        public const uint MATRIX3_ARB = 0x88C3;
        public const uint MATRIX4_ARB = 0x88C4;
        public const uint MATRIX5_ARB = 0x88C5;
        public const uint MATRIX6_ARB = 0x88C6;
        public const uint MATRIX7_ARB = 0x88C7;
        public const uint MATRIX8_ARB = 0x88C8;
        public const uint MATRIX9_ARB = 0x88C9;
        public const uint MATRIX10_ARB = 0x88CA;
        public const uint MATRIX11_ARB = 0x88CB;
        public const uint MATRIX12_ARB = 0x88CC;
        public const uint MATRIX13_ARB = 0x88CD;
        public const uint MATRIX14_ARB = 0x88CE;
        public const uint MATRIX15_ARB = 0x88CF;
        public const uint MATRIX16_ARB = 0x88D0;
        public const uint MATRIX17_ARB = 0x88D1;
        public const uint MATRIX18_ARB = 0x88D2;
        public const uint MATRIX19_ARB = 0x88D3;
        public const uint MATRIX20_ARB = 0x88D4;
        public const uint MATRIX21_ARB = 0x88D5;
        public const uint MATRIX22_ARB = 0x88D6;
        public const uint MATRIX23_ARB = 0x88D7;
        public const uint MATRIX24_ARB = 0x88D8;
        public const uint MATRIX25_ARB = 0x88D9;
        public const uint MATRIX26_ARB = 0x88DA;
        public const uint MATRIX27_ARB = 0x88DB;
        public const uint MATRIX28_ARB = 0x88DC;
        public const uint MATRIX29_ARB = 0x88DD;
        public const uint MATRIX30_ARB = 0x88DE;
        public const uint MATRIX31_ARB = 0x88DF;
        public const uint INCR_WRAP_EXT = 0x8507;
        public const uint GL_DECR_WRAP_EXT = 8508;
        public const uint COMPRESSED_ALPHA_ARB = 0x84E9;
        public const uint COMPRESSED_LUMINANCE_ARB = 0x84EA;
        public const uint COMPRESSED_LUMINANCE_ALPHA_ARB = 0x84EB;
        public const uint COMPRESSED_INTENSITY_ARB = 0x84EC;
        public const uint COMPRESSED_RGB_ARB = 0x84ED;
        public const uint COMPRESSED_RGBA_ARB = 0x84EE;
        public const uint TEXTURE_COMPRESSION_HINT_ARB = 0x84EF;
        public const uint TEXTURE_COMPRESSED_IMAGE_SIZE_ARB = 0x86A0;
        public const uint TEXTURE_COMPRESSED_ARB = 0x86A1;
        public const uint NUM_COMPRESSED_TEXTURE_FORMATS_ARB = 0x86A2;
        public const uint COMPRESSED_TEXTURE_FORMATS_ARB = 0x86A3;
        public const uint TEXTURE_FILTER_CONTROL_EXT = 0x8500;
        public const uint TEXTURE_LOD_BIAS_EXT = 0x8501;
        public const uint MAX_TEXTURE_LOD_BIAS_EXT = 0x84FD;
        public const uint TEXTURE_MIN_LOD_SGIS = 0x813A;
        public const uint EXTURE_MAX_LOD_SGIS = 0x813B;
        public const uint TEXTURE_BASE_LEVEL_SGIS = 0x813C;
        public const uint TEXTURE_MAX_LEVEL_SGIS = 0x813D;
        public const uint GENERATE_MIPMAP_SGIS = 0x8191;
        public const uint GL_MIRRORED_REPEAT_ARB = 0x8370;
        public const uint DOT3_RGB_ARB = 0x86AE;
        public const uint DOT3_RGBA_ARB = 0x86AF;
        public const uint TEXTURE_3D_ARB = 0x806F;
        public const uint TEXTURE_WRAP_R_ARB = 0x8072;

        public static int PROGRAM_OBJECT_ARB = 0x8B40;
        public static int OBJECT_TYPE_ARB = 0x8B4E;
        public static int OBJECT_SUBTYPE_ARB = 0x8B4F;
        public static int SHADER_OBJECT_ARB = 0x8B48;
        public static int FLOAT_VEC2_ARB = 0x8B50;
        public static int FLOAT_VEC3_ARB = 0x8B51;
        public static int FLOAT_VEC4_ARB = 0x8B52;
        public static int INT_VEC2_ARB = 0x8B53;
        public static int INT_VEC3_ARB = 0x8B54;
        public static int INT_VEC4_ARB = 0x8B55;
        public static int BOOL_ARB = 0x8B56;
        public static int BOOL_VEC2_ARB = 0x8B57;
        public static int BOOL_VEC3_ARB = 0x8B58;
        public static int BOOL_VEC4_ARB = 0x8B59;
        public static int FLOAT_MAT2_ARB = 0x8B5A;
        public static int FLOAT_MAT3_ARB = 0x8B5B;
        public static int FLOAT_MAT4_ARB = 0x8B5C;
        public static int VERTEX_SHADER_ARB = 0x8B31;
        public static int MAX_VERTEX_UNIFORM_COMPONENTS_ARB = 0x8B4A;
        public static int MAX_TEXTURE_IMAGE_UNITS_ARB = 0x8872;
        public static int MAX_VERTEX_TEXTURE_IMAGE_UNITS_ARB = 0x8B4C;
        public static int MAX_COMBINED_TEXTURE_IMAGE_UNITS_ARB = 0x8B4D;
        public static int MAX_TEXTURE_COORDS_ARB = 0x8871;
        public static int FRAGMENT_SHADER_ARB = 0x8B30;
        public static int MAX_FRAGMENT_UNIFORM_COMPONENTS_ARB = 0x8B49;
        public static int MAX_VARYING_FLOATS_ARB = 0x8B4B;
        public static int OBJECT_DELETE_STATUS_ARB = 0x8B80;
        public static int OBJECT_COMPILE_STATUS_ARB = 0x8B81;
        public static int OBJECT_LINK_STATUS_ARB = 0x8B82;
        public static int OBJECT_VALIDATE_STATUS_ARB = 0x8B83;
        public static int OBJECT_INFO_LOG_LENGTH_ARB = 0x8B84;
        public static int OBJECT_ATTACHED_OBJECTS_ARB = 0x8B85;
        public static int OBJECT_ACTIVE_UNIFORMS_ARB = 0x8B86;
        public static int OBJECT_ACTIVE_UNIFORM_MAX_LENGTH_ARB = 0x8B87;
        public static int OBJECT_SHADER_SOURCE_LENGTH_ARB = 0x8B88;
        public static int OBJECT_ACTIVE_ATTRIBUTE_MAX_LENGTH_ARB = 0x8B8A;
        public static int OBJECT_ACTIVE_ATTRIBUTES_ARB = 0x8B89;


        [DllImport(DLLName, EntryPoint = "glAccum")]
        public static extern void Accum(uint op, float value);

        [DllImport(DLLName, EntryPoint = "glAlphaFunc")]
        public static extern void AlphaFunc(uint func, float refval);

        [DllImport(DLLName, EntryPoint = "glAreTexturesResident")]
        public static extern bool AreTexturesResident(int n, uint[] textures, bool[] residences);

        [DllImport(DLLName, EntryPoint = "glArrayElement")]
        public static extern void ArrayElement(int i);

        [DllImport(DLLName, EntryPoint = "glBegin")]
        public static extern void Begin(uint mode);

        [DllImport(DLLName, EntryPoint = "glBindTexture")]
        public static extern void BindTexture(uint target, uint texture);

        [DllImport(DLLName, EntryPoint = "glBitmap")]
        public static extern void Bitmap(int width, int height, float xorig, float yorig, float xmove, float ymove, byte[] bitmap);

        [DllImport(DLLName, EntryPoint = "glBlendFunc")]
        public static extern void BlendFunc(uint sfactor, uint dfactor);

        [DllImport(DLLName, EntryPoint = "glCallList")]
        public static extern void CallList(uint list);

        // void glCallLists (GLsizei n, GLenum type, const GLvoid *lists);
        // Подозреваю, что GLvoid* не обязательно int[]; добавил object[]
        // на всякий случай добавил перегрузку с IntPtr
        [DllImport(DLLName, EntryPoint = "glCallLists")]
        public static extern void CallLists(int n, uint type, int[] lists);

        [DllImport(DLLName, EntryPoint = "glCallLists")]
        public static extern void CallLists(int n, uint type, int offset);

        [DllImport(DLLName, EntryPoint = "glCallLists")]
        public static extern void CallLists(int n, uint type, object[] lists);

        [DllImport(DLLName, EntryPoint = "glCallLists")]
        public static extern void CallLists(int n, uint type, IntPtr lists);

        [DllImport(DLLName, EntryPoint = "glClear")]
        public static extern void Clear(uint mask);

        [DllImport(DLLName, EntryPoint = "glClearAccum")]
        public static extern void ClearAccum(float red, float green, float blue, float alpha);

        [DllImport(DLLName, EntryPoint = "glClearColor")]
        public static extern void ClearColor(float red, float green, float blue, float alpha);

        [DllImport(DLLName, EntryPoint = "glClearDepth")]
        public static extern void ClearDepth(double depth);

        [DllImport(DLLName, EntryPoint = "glClearIndex")]
        public static extern void ClearIndex(float c);

        [DllImport(DLLName, EntryPoint = "glClearStencil")]
        public static extern void ClearStencil(int s);

        [DllImport(DLLName, EntryPoint = "glClipPlane")]
        public static extern void ClipPlane(uint plane, double[] equation);

        [DllImport(DLLName, EntryPoint = "glColor3b")]
        public static extern void Color3b(sbyte red, sbyte green, sbyte blue);

        [DllImport(DLLName, EntryPoint = "glColor3bv")]
        public static extern void Color3bv(ref sbyte v);

        [DllImport(DLLName, EntryPoint = "glColor3d")]
        public static extern void Color3d(double red, double green, double blue);

        [DllImport(DLLName, EntryPoint = "glColor3dv")]
        public static extern void Color3dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glColor3f")]
        public static extern void Color3f(float red, float green, float blue);

        [DllImport(DLLName, EntryPoint = "glColor3fv")]
        public static extern void Color3fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glColor3i")]
        public static extern void Color3i(int red, int green, int blue);

        [DllImport(DLLName, EntryPoint = "glColor3iv")]
        public static extern void Color3iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glColor3s")]
        public static extern void Color3s(short red, short green, short blue);

        [DllImport(DLLName, EntryPoint = "glColor3sv")]
        public static extern void Color3sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glColor3ub")]
        public static extern void Color3ub(byte red, byte green, byte blue);

        [DllImport(DLLName, EntryPoint = "glColor3ubv")]
        public static extern void Color3ubv(byte[] v);

        [DllImport(DLLName, EntryPoint = "glColor3ui")]
        public static extern void Color3ui(uint red, uint green, uint blue);

        [DllImport(DLLName, EntryPoint = "glColor3uiv")]
        public static extern void Color3uiv(uint[] v);

        [DllImport(DLLName, EntryPoint = "glColor3us")]
        public static extern void Color3us(short red, ushort green, ushort blue);

        [DllImport(DLLName, EntryPoint = "glColor3usv")]
        public static extern void Color3usv(short[] v);

        [DllImport(DLLName, EntryPoint = "glColor4b")]
        public static extern void Color4b(sbyte red, sbyte green, sbyte blue, sbyte alpha);

        [DllImport(DLLName, EntryPoint = "glColor4bv")]
        public static extern void Color4bv(sbyte[] v);

        [DllImport(DLLName, EntryPoint = "glColor4d")]
        public static extern void Color4d(double red, double green, double blue, double alpha);

        [DllImport(DLLName, EntryPoint = "glColor4dv")]
        public static extern void Color4dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glColor4f")]
        public static extern void Color4f(float red, float green, float blue, float alpha);

        [DllImport(DLLName, EntryPoint = "glColor4fv")]
        public static extern void Color4fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glColor4i")]
        public static extern void Color4i(int red, int green, int blue, int alpha);

        [DllImport(DLLName, EntryPoint = "glColor4iv")]
        public static extern void Color4iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glColor4s")]
        public static extern void Color4s(short red, short green, short blue, short alpha);

        [DllImport(DLLName, EntryPoint = "glColor4sv")]
        public static extern void Color4sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glColor4ub")]
        public static extern void Color4ub(byte red, byte green, byte blue, byte alpha);

        [DllImport(DLLName, EntryPoint = "glColor4ubv")]
        public static extern void Color4ubv(byte[] v);

        [DllImport(DLLName, EntryPoint = "glColor4ui")]
        public static extern void Color4ui(uint red, uint green, uint blue, uint alpha);

        [DllImport(DLLName, EntryPoint = "glColor4uiv")]
        public static extern void Color4uiv(uint[] v);

        [DllImport(DLLName, EntryPoint = "glColor4us")]
        public static extern void Color4us(ushort red, ushort green, ushort blue, ushort alpha);

        [DllImport(DLLName, EntryPoint = "glColor4usv")]
        public static extern void Color4usv(ushort[] v);

        [DllImport(DLLName, EntryPoint = "glColorMask")]
        public static extern void ColorMask(bool red, bool green, bool blue, bool alpha);

        [DllImport(DLLName, EntryPoint = "glColorMaterial")]
        public static extern void ColorMaterial(uint face, uint mode);

        [DllImport(DLLName, EntryPoint = "glColorPointer")]
        public static extern void ColorPointer(int size, uint type, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glColorPointer")]
        public static extern void ColorPointer(int size, uint type, int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glCopyPixels")]
        public static extern void CopyPixels(int x, int y, int width, int height, uint type);

        [DllImport(DLLName, EntryPoint = "glCopyTexImage1D")]
        public static extern void CopyTexImage1D(uint target, int level, uint internalformat, int x, int y, int width, int border);

        [DllImport(DLLName, EntryPoint = "glCopyTexImage2D")]
        public static extern void CopyTexImage2D(uint target, int level, uint internalformat, int x, int y, int width, int height, int border);

        [DllImport(DLLName, EntryPoint = "glCopyTexSubImage1D")]
        public static extern void CopyTexSubImage1D(uint target, int level, int xoffset, int x, int y, int width);

        [DllImport(DLLName, EntryPoint = "glCopyTexSubImage2D")]
        public static extern void CopyTexSubImage2D(uint target, int level, int xoffset, int yoffset, int x, int y, int width, int height);

        [DllImport(DLLName, EntryPoint = "glCullFace")]
        public static extern void CullFace(uint mode);

        [DllImport(DLLName, EntryPoint = "glDeleteLists")]
        public static extern void DeleteLists(uint list, int range);

        [DllImport(DLLName, EntryPoint = "glDeleteTextures")]
        public static extern void DeleteTextures(int n, uint[] textures);

        [DllImport(DLLName, EntryPoint = "glDeleteTextures")]
        public static extern void DeleteTextures(int n, ref int texture);

        [DllImport(DLLName, EntryPoint = "glDepthFunc")]
        public static extern void DepthFunc(uint func);

        [DllImport(DLLName, EntryPoint = "glDepthMask")]
        public static extern void DepthMask(bool flag);

        [DllImport(DLLName, EntryPoint = "glDepthRange")]
        public static extern void DepthRange(double zNear, double zFar);

        [DllImport(DLLName, EntryPoint = "glDisable")]
        public static extern void Disable(uint cap);

        [DllImport(DLLName, EntryPoint = "glDisableClientState")]
        public static extern void DisableClientState(uint array);

        [DllImport(DLLName, EntryPoint = "glDrawArrays")]
        public static extern void DrawArrays(uint mode, int first, int count);

        [DllImport(DLLName, EntryPoint = "glDrawBuffer")]
        public static extern void DrawBuffer(uint mode);

        [DllImport(DLLName, EntryPoint = "glDrawElements")]
        public static extern void DrawElements(uint mode, int count, uint type, int[] indices);

        [DllImport(DLLName, EntryPoint = "glDrawElements")]
        public static extern void DrawElements(uint mode, int count, uint type, IntPtr indices);

        [DllImport(DLLName, EntryPoint = "glDrawElements")]
        public static extern void DrawElements(uint mode, int count, uint type, int offset);

        [DllImport(DLLName, EntryPoint = "glDrawPixels")]
        public static extern void DrawPixels(int width, int height, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glEdgeFlag")]
        public static extern void EdgeFlag(bool flag);

        [DllImport(DLLName, EntryPoint = "glEdgeFlagPointer")]
        public static extern void EdgeFlagPointer(int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glEdgeFlagPointer")]
        public static extern void EdgeFlagPointer(int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glEdgeFlagv")]
        public static extern void EdgeFlagv(bool[] flag);

        [DllImport(DLLName, EntryPoint = "glEnable")]
        public static extern void Enable(uint cap);

        [DllImport(DLLName, EntryPoint = "glEnableClientState")]
        public static extern void EnableClientState(uint array);

        [DllImport(DLLName, EntryPoint = "glEnd")]
        public static extern void End();

        [DllImport(DLLName, EntryPoint = "glEndList")]
        public static extern void EndList();

        [DllImport(DLLName, EntryPoint = "glEvalCoord1d")]
        public static extern void EvalCoord1d(double u);

        [DllImport(DLLName, EntryPoint = "glEvalCoord1dv")]
        public static extern void EvalCoord1dv(double[] u);

        [DllImport(DLLName, EntryPoint = "glEvalCoord1f")]
        public static extern void EvalCoord1f(float u);

        [DllImport(DLLName, EntryPoint = "glEvalCoord1fv")]
        public static extern void EvalCoord1fv(float[] u);

        [DllImport(DLLName, EntryPoint = "glEvalCoord2d")]
        public static extern void EvalCoord2d(double u, double v);

        [DllImport(DLLName, EntryPoint = "glEvalCoord2dv")]
        public static extern void EvalCoord2dv(double[] u);

        [DllImport(DLLName, EntryPoint = "glEvalCoord2f")]
        public static extern void EvalCoord2f(float u, float v);

        [DllImport(DLLName, EntryPoint = "glEvalCoord2fv")]
        public static extern void EvalCoord2fv(ref float u);

        [DllImport(DLLName, EntryPoint = "glEvalMesh1")]
        public static extern void EvalMesh1(uint mode, int i1, int i2);

        [DllImport(DLLName, EntryPoint = "glEvalMesh2")]
        public static extern void EvalMesh2(uint mode, int i1, int i2, int j1, int j2);

        [DllImport(DLLName, EntryPoint = "glEvalPoint1")]
        public static extern void EvalPoint1(int i);

        [DllImport(DLLName, EntryPoint = "glEvalPoint2")]
        public static extern void EvalPoint2(int i, int j);

        [DllImport(DLLName, EntryPoint = "glFeedbackBuffer")]
        public static extern void FeedbackBuffer(int size, uint type, float[] buffer);

        [DllImport(DLLName, EntryPoint = "glFinish")]
        public static extern void Finish();

        [DllImport(DLLName, EntryPoint = "glFlush")]
        public static extern void Flush();

        [DllImport(DLLName, EntryPoint = "glFogf")]
        public static extern void Fogf(uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glFogfv")]
        public static extern void Fogfv(uint pname, float[] fogparams);

        [DllImport(DLLName, EntryPoint = "glFogi")]
        public static extern void Fogi(uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glFogiv")]
        public static extern void Fogiv(uint pname, int[] fogparams);

        [DllImport(DLLName, EntryPoint = "glFrontFace")]
        public static extern void FrontFace(uint mode);

        [DllImport(DLLName, EntryPoint = "glFrustum")]
        public static extern void Frustum(double left, double right, double bottom, double top, double zNear, double zFar);

        [DllImport(DLLName, EntryPoint = "glGenLists")]
        public static extern uint GenLists(int range);

        [DllImport(DLLName, EntryPoint = "glGenTextures")]
        public static extern void GenTextures(int n, uint[] textures);

        [DllImport(DLLName, EntryPoint = "glGenTextures")]
        public static extern void GenTextures(int n, ref uint texture);

        [DllImport(DLLName, EntryPoint = "glGetBooleanv")]
        public static extern void GetBooleanv(uint pname, bool[] bparams);

        [DllImport(DLLName, EntryPoint = "glGetClipPlane")]
        public static extern void GetClipPlane(uint plane, double[] equation);

        [DllImport(DLLName, EntryPoint = "glGetDoublev")]
        public static extern void GetDoublev(uint pname, double[] dparams);

        [DllImport(DLLName, EntryPoint = "glGetError")]
        public static extern uint GetError();

        [DllImport(DLLName, EntryPoint = "glGetFloatv")]
        public static extern void GetFloatv(uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetIntegerv")]
        public static extern void GetIntegerv(uint pname, int[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetLightfv")]
        public static extern void GetLightfv(uint light, uint pname, float[] lparams);

        [DllImport(DLLName, EntryPoint = "glGetLightiv")]
        public static extern void GetLightiv(uint light, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glGetMapdv")]
        public static extern void GetMapdv(uint target, uint query, double[] v);

        [DllImport(DLLName, EntryPoint = "glGetMapfv")]
        public static extern void GetMapfv(uint target, uint query, float[] v);

        [DllImport(DLLName, EntryPoint = "glGetMapiv")]
        public static extern void GetMapiv(uint target, uint query, int[] v);

        [DllImport(DLLName, EntryPoint = "glGetMaterialfv")]
        public static extern void GetMaterialfv(uint face, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetMaterialiv")]
        public static extern void GetMaterialiv(uint face, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glGetPixelMapfv")]
        public static extern void GetPixelMapfv(uint map, float[] values);

        [DllImport(DLLName, EntryPoint = "glGetPixelMapuiv")]
        public static extern void GetPixelMapuiv(uint map, uint[] values);

        [DllImport(DLLName, EntryPoint = "glGetPixelMapusv")]
        public static extern void GetPixelMapusv(uint map, ushort[] values);

        [DllImport(DLLName, EntryPoint = "glGetPointerv")]
        public static extern void GetPointerv(uint pname, ref int param);

        [DllImport(DLLName, EntryPoint = "glGetPolygonStipple")]
        public static extern void GetPolygonStipple(byte[] mask);

        [DllImport(DLLName, EntryPoint = "glGetString")]
        private static extern IntPtr GetString_(int name);

        [DllImport(DLLName, EntryPoint = "glGetTexEnvfv")]
        public static extern void GetTexEnvfv(uint target, uint pname, float[] envparams);

        [DllImport(DLLName, EntryPoint = "glGetTexEnviv")]
        public static extern void GetTexEnviv(uint target, uint pname, int[] envparams);

        [DllImport(DLLName, EntryPoint = "glGetTexGendv")]
        public static extern void GetTexGendv(uint coord, uint pname, double[] dparams);

        [DllImport(DLLName, EntryPoint = "glGetTexGenfv")]
        public static extern void GetTexGenfv(uint coord, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetTexGeniv")]
        public static extern void GetTexGeniv(uint coord, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glGetTexImage")]
        public static extern void GetTexImage(uint target, int level, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glGetTexLevelParameterfv")]
        public static extern void GetTexLevelParameterfv(uint target, int level, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetTexLevelParameteriv")]
        public static extern void GetTexLevelParameteriv(uint target, int level, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glGetTexParameterfv")]
        public static extern void GetTexParameterfv(uint target, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glGetTexParameteriv")]
        public static extern void GetTexParameteriv(uint target, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glHint")]
        public static extern void Hint(uint target, uint mode);

        [DllImport(DLLName, EntryPoint = "glIndexMask")]
        public static extern void IndexMask(uint mask);

        [DllImport(DLLName, EntryPoint = "glIndexPointer")]
        public static extern void IndexPointer(uint type, int stride, int[] pointer);

        [DllImport(DLLName, EntryPoint = "glIndexPointer")]
        public static extern void IndexPointer(uint type, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glIndexd")]
        public static extern void Indexd(double c);

        [DllImport(DLLName, EntryPoint = "glIndexdv")]
        public static extern void Indexdv(double[] c);

        [DllImport(DLLName, EntryPoint = "glIndexf")]
        public static extern void Indexf(float c);

        [DllImport(DLLName, EntryPoint = "glIndexfv")]
        public static extern void Indexfv(float[] c);

        [DllImport(DLLName, EntryPoint = "glIndexi")]
        public static extern void Indexi(int c);

        [DllImport(DLLName, EntryPoint = "glIndexiv")]
        public static extern void Indexiv(int[] c);

        [DllImport(DLLName, EntryPoint = "glIndexs")]
        public static extern void Indexs(short c);

        [DllImport(DLLName, EntryPoint = "glIndexsv")]
        public static extern void Indexsv(short[] c);

        [DllImport(DLLName, EntryPoint = "glIndexub")]
        public static extern void Indexub(byte c);

        [DllImport(DLLName, EntryPoint = "glIndexubv")]
        public static extern void Indexubv(byte[] c);

        [DllImport(DLLName, EntryPoint = "glInitNames")]
        public static extern void InitNames();
        
        [DllImport(DLLName, EntryPoint = "glInterleavedArrays")]
        public static extern void interleavedArrays(uint format, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glInterleavedArrays")]
        public static extern void interleavedArrays(uint format, int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glIsEnabled")]
        public static extern bool IsEnabled(uint cap);

        [DllImport(DLLName, EntryPoint = "glIsList")]
        public static extern bool IsList(uint list);

        [DllImport(DLLName, EntryPoint = "glIsTexture")]
        public static extern bool IsTexture(uint texture);

        [DllImport(DLLName, EntryPoint = "glLightModelf")]
        public static extern void LightModelf(uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glLightModelfv")]
        public static extern void LightModelfv(uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glLightModeli")]
        public static extern void LightModeli(uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glLightModeliv")]
        public static extern void LightModeliv(uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glLightf")]
        public static extern void Lightf(uint light, uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glLightfv")]
        public static extern void Lightfv(uint light, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glLighti")]
        public static extern void Lighti(uint light, uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glLightiv")]
        public static extern void Lightiv(uint light, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glLineStipple")]
        public static extern void LineStipple(int factor, ushort pattern);

        [DllImport(DLLName, EntryPoint = "glLineWidth")]
        public static extern void LineWidth(float width);

        [DllImport(DLLName, EntryPoint = "glListBase")]
        public static extern void ListBase(uint basevalue);

        [DllImport(DLLName, EntryPoint = "glLoadIdentity")]
        public static extern void LoadIdentity();

        [DllImport(DLLName, EntryPoint = "glLoadMatrixd")]
        public static extern void LoadMatrixd(double[] m);

        [DllImport(DLLName, EntryPoint = "glLoadMatrixf")]
        public static extern void LoadMatrixf(float[] m);

        [DllImport(DLLName, EntryPoint = "glLoadName")]
        public static extern void LoadName(uint name);

        [DllImport(DLLName, EntryPoint = "glLoadName")]
        public static extern void LoadName(object name);

        [DllImport(DLLName, EntryPoint = "glLogicOp")]
        public static extern void LogicOp(uint opcode);

        [DllImport(DLLName, EntryPoint = "glMap1d")]
        public static extern void Map1d(uint target, double u1, double u2, int stride, int order, double[] points);

        [DllImport(DLLName, EntryPoint = "glMap1f")]
        public static extern void Map1f(uint target, float u1, float u2, int stride, int order, float[] points);

        [DllImport(DLLName, EntryPoint = "glMap2d")]
        public static extern void Map2d(uint target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, double[] points);

        [DllImport(DLLName, EntryPoint = "glMap2f")]
        public static extern void Map2f(uint target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, float[] points);

        [DllImport(DLLName, EntryPoint = "glMapGrid1d")]
        public static extern void MapGrid1d(int un, double u1, double u2);

        [DllImport(DLLName, EntryPoint = "glMapGrid1f")]
        public static extern void MapGrid1f(int un, float u1, float u2);

        [DllImport(DLLName, EntryPoint = "glMapGrid2d")]
        public static extern void MapGrid2d(int un, double u1, double u2, int vn, double v1, double v2);

        [DllImport(DLLName, EntryPoint = "glMapGrid2f")]
        public static extern void MapGrid2f(int un, float u1, float u2, int vn, float v1, float v2);

        [DllImport(DLLName, EntryPoint = "glMaterialf")]
        public static extern void Materialf(uint face, uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glMaterialfv")]
        public static extern void Materialfv(uint face, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glMateriali")]
        public static extern void Materiali(uint face, uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glMaterialiv")]
        public static extern void Materialiv(uint face, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glMatrixMode")]
        public static extern void MatrixMode(uint mode);

        [DllImport(DLLName, EntryPoint = "glMultMatrixd")]
        public static extern void MultMatrixd(double[] m);

        [DllImport(DLLName, EntryPoint = "glMultMatrixf")]
        public static extern void MultMatrixf(float[] m);

        [DllImport(DLLName, EntryPoint = "glNewList")]
        public static extern void NewList(uint list, uint mode);

        [DllImport(DLLName, EntryPoint = "glNormal3b")]
        public static extern void Normal3b(sbyte nx, sbyte ny, sbyte nz);

        [DllImport(DLLName, EntryPoint = "glNormal3bv")]
        public static extern void Normal3bv(sbyte[] v);

        [DllImport(DLLName, EntryPoint = "glNormal3d")]
        public static extern void Normal3d(double nx, double ny, double nz);

        [DllImport(DLLName, EntryPoint = "glNormal3dv")]
        public static extern void Normal3dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glNormal3f")]
        public static extern void Normal3f(float nx, float ny, float nz);

        [DllImport(DLLName, EntryPoint = "glNormal3fv")]
        public static extern void Normal3fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glNormal3i")]
        public static extern void Normal3i(int nx, int ny, int nz);

        [DllImport(DLLName, EntryPoint = "glNormal3iv")]
        public static extern void Normal3iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glNormal3s")]
        public static extern void Normal3s(short nx, short ny, short nz);

        [DllImport(DLLName, EntryPoint = "glNormal3sv")]
        public static extern void Normal3sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glNormalPointer")]
        public static extern void NormalPointer(uint type, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glNormalPointer")]
        public static extern void NormalPointer(uint type, int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glOrtho")]
        public static extern void Ortho(double left, double right, double bottom, double top, double zNear, double zFar);

        [DllImport(DLLName, EntryPoint = "glPassThrough")]
        public static extern void PassThrough(float token);

        [DllImport(DLLName, EntryPoint = "glPixelMapfv")]
        public static extern void PixelMapfv(uint map, int mapsize, float[] values);

        [DllImport(DLLName, EntryPoint = "glPixelMapuiv")]
        public static extern void PixelMapuiv(uint map, int mapsize, uint[] values);

        [DllImport(DLLName, EntryPoint = "glPixelMapusv")]
        public static extern void PixelMapusv(uint map, int mapsize, ushort[] values);

        [DllImport(DLLName, EntryPoint = "glPixelStoref")]
        public static extern void PixelStoref(uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glPixelStorei")]
        public static extern void PixelStorei(uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glPixelTransferf")]
        public static extern void PixelTransferf(uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glPixelTransferi")]
        public static extern void PixelTransferi(uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glPixelZoom")]
        public static extern void PixelZoom(float xfactor, float yfactor);

        [DllImport(DLLName, EntryPoint = "glPointSize")]
        public static extern void PointSize(float size);

        [DllImport(DLLName, EntryPoint = "glPolygonMode")]
        public static extern void PolygonMode(uint face, uint mode);

        [DllImport(DLLName, EntryPoint = "glPolygonOffset")]
        public static extern void PolygonOffset(float factor, float units);

        [DllImport(DLLName, EntryPoint = "glPolygonStipple")]
        public static extern void PolygonStipple(byte[] mask);

        [DllImport(DLLName, EntryPoint = "glPopAttrib")]
        public static extern void PopAttrib();

        [DllImport(DLLName, EntryPoint = "glPopClientAttrib")]
        public static extern void PopClientAttrib();

        [DllImport(DLLName, EntryPoint = "glPopMatrix")]
        public static extern void PopMatrix();

        [DllImport(DLLName, EntryPoint = "glPopName")]
        public static extern void PopName();

        [DllImport(DLLName, EntryPoint = "glPrioritizeTextures")]
        public static extern void PrioritizeTextures(int n, uint[] textures, float[] priorities);

        [DllImport(DLLName, EntryPoint = "glPushAttrib")]
        public static extern void PushAttrib(uint mask);

        [DllImport(DLLName, EntryPoint = "glPushClientAttrib")]
        public static extern void PushClientAttrib(uint mask);

        [DllImport(DLLName, EntryPoint = "glPushMatrix")]
        public static extern void PushMatrix();

        [DllImport(DLLName, EntryPoint = "glPushName")]
        public static extern void PushName(uint name);

        [DllImport(DLLName, EntryPoint = "glRasterPos2d")]
        public static extern void RasterPos2d(double x, double y);

        [DllImport(DLLName, EntryPoint = "glRasterPos2dv")]
        public static extern void RasterPos2dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos2f")]
        public static extern void RasterPos2f(float x, float y);

        [DllImport(DLLName, EntryPoint = "glRasterPos2fv")]
        public static extern void RasterPos2fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos2i")]
        public static extern void RasterPos2i(int x, int y);

        [DllImport(DLLName, EntryPoint = "glRasterPos2iv")]
        public static extern void RasterPos2iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos2s")]
        public static extern void RasterPos2s(short x, short y);

        [DllImport(DLLName, EntryPoint = "glRasterPos2sv")]
        public static extern void RasterPos2sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos3d")]
        public static extern void RasterPos3d(double x, double y, double z);

        [DllImport(DLLName, EntryPoint = "glRasterPos3dv")]
        public static extern void RasterPos3dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos3f")]
        public static extern void RasterPos3f(float x, float y, float z);

        [DllImport(DLLName, EntryPoint = "glRasterPos3fv")]
        public static extern void RasterPos3fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos3i")]
        public static extern void RasterPos3i(int x, int y, int z);

        [DllImport(DLLName, EntryPoint = "glRasterPos3iv")]
        public static extern void RasterPos3iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos3s")]
        public static extern void RasterPos3s(short x, short y, short z);

        [DllImport(DLLName, EntryPoint = "glRasterPos3sv")]
        public static extern void RasterPos3sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos4d")]
        public static extern void RasterPos4d(double x, double y, double z, double w);

        [DllImport(DLLName, EntryPoint = "glRasterPos4dv")]
        public static extern void RasterPos4dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos4f")]
        public static extern void RasterPos4f(float x, float y, float z, float w);

        [DllImport(DLLName, EntryPoint = "glRasterPos4fv")]
        public static extern void RasterPos4fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos4i")]
        public static extern void RasterPos4i(int x, int y, int z, int w);

        [DllImport(DLLName, EntryPoint = "glRasterPos4iv")]
        public static extern void RasterPos4iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glRasterPos4s")]
        public static extern void RasterPos4s(short x, short y, short z, short w);

        [DllImport(DLLName, EntryPoint = "glRasterPos4sv")]
        public static extern void RasterPos4sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glReadBuffer")]
        public static extern void ReadBuffer(uint mode);

        [DllImport(DLLName, EntryPoint = "glReadPixels")]
        public static extern void ReadPixels(int x, int y, int width, int height, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glRectd")]
        public static extern void Rectd(double x1, double y1, double x2, double y2);

        [DllImport(DLLName, EntryPoint = "glRectdv")]
        public static extern void Rectdv(double[] v1, double[] v2);

        [DllImport(DLLName, EntryPoint = "glRectf")]
        public static extern void Rectf(float x1, float y1, float x2, float y2);

        [DllImport(DLLName, EntryPoint = "glRectfv")]
        public static extern void Rectfv(float[] v1, float[] v2);

        [DllImport(DLLName, EntryPoint = "glRecti")]
        public static extern void Recti(int x1, int y1, int x2, int y2);

        [DllImport(DLLName, EntryPoint = "glRectiv")]
        public static extern void Rectiv(int[] v1, int[] v2);

        [DllImport(DLLName, EntryPoint = "glRects")]
        public static extern void Rects(short x1, short y1, short x2, short y2);

        [DllImport(DLLName, EntryPoint = "glRectsv")]
        public static extern void Rectsv(short[] v1, short[] v2);

        [DllImport(DLLName, EntryPoint = "glRenderMode")]
        public static extern int RenderMode(uint mode);

        [DllImport(DLLName, EntryPoint = "glRotated")]
        public static extern void Rotated(double angle, double x, double y, double z);

        [DllImport(DLLName, EntryPoint = "glRotatef")]
        public static extern void Rotatef(float angle, float x, float y, float z);

        [DllImport(DLLName, EntryPoint = "glScaled")]
        public static extern void Scaled(double x, double y, double z);

        [DllImport(DLLName, EntryPoint = "glScalef")]
        public static extern void Scalef(float x, float y, float z);

        [DllImport(DLLName, EntryPoint = "glScissor")]
        public static extern void Scissor(int x, int y, int width, int height);

        [DllImport(DLLName, EntryPoint = "glSelectBuffer")]
        public static extern void SelectBuffer(int size, int[] buffer);

        [DllImport(DLLName, EntryPoint = "glSelectBuffer")]
        public static extern void SelectBuffer(int size, IntPtr buffer);

        [DllImport(DLLName, EntryPoint = "glSelectBuffer")]
        public static extern void SelectBuffer(int size, uint[] buffer);

        [DllImport(DLLName, EntryPoint = "glShadeModel")]
        public static extern void ShadeModel(uint mode);

        [DllImport(DLLName, EntryPoint = "glStencilFunc")]
        public static extern void StencilFunc(uint func, int refvalue, uint mask);

        [DllImport(DLLName, EntryPoint = "glStencilMask")]
        public static extern void StencilMask(uint mask);

        [DllImport(DLLName, EntryPoint = "glStencilOp")]
        public static extern void StencilOp(uint fail, uint zfail, uint zpass);

        [DllImport(DLLName, EntryPoint = "glTexCoord1d")]
        public static extern void TexCoord1d(double s);

        [DllImport(DLLName, EntryPoint = "glTexCoord1dv")]
        public static extern void TexCoord1dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord1f")]
        public static extern void TexCoord1f(float s);

        [DllImport(DLLName, EntryPoint = "glTexCoord1fv")]
        public static extern void TexCoord1fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord1i")]
        public static extern void TexCoord1i(int s);

        [DllImport(DLLName, EntryPoint = "glTexCoord1iv")]
        public static extern void TexCoord1iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord1s")]
        public static extern void TexCoord1s(short s);

        [DllImport(DLLName, EntryPoint = "glTexCoord1sv")]
        public static extern void TexCoord1sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord2d")]
        public static extern void TexCoord2d(double s, double t);

        [DllImport(DLLName, EntryPoint = "glTexCoord2dv")]
        public static extern void TexCoord2dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord2f")]
        public static extern void TexCoord2f(float s, float t);

        [DllImport(DLLName, EntryPoint = "glTexCoord2fv")]
        public static extern void TexCoord2fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord2i")]
        public static extern void TexCoord2i(int s, int t);

        [DllImport(DLLName, EntryPoint = "glTexCoord2iv")]
        public static extern void TexCoord2iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord2s")]
        public static extern void TexCoord2s(short s, short t);

        [DllImport(DLLName, EntryPoint = "glTexCoord2sv")]
        public static extern void TexCoord2sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord3d")]
        public static extern void TexCoord3d(double s, double t, double r);

        [DllImport(DLLName, EntryPoint = "glTexCoord3dv")]
        public static extern void TexCoord3dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord3f")]
        public static extern void TexCoord3f(float s, float t, float r);

        [DllImport(DLLName, EntryPoint = "glTexCoord3fv")]
        public static extern void TexCoord3fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord3i")]
        public static extern void TexCoord3i(int s, int t, int r);

        [DllImport(DLLName, EntryPoint = "glTexCoord3iv")]
        public static extern void TexCoord3iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord3s")]
        public static extern void TexCoord3s(short s, short t, short r);

        [DllImport(DLLName, EntryPoint = "glTexCoord3sv")]
        public static extern void TexCoord3sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord4d")]
        public static extern void TexCoord4d(double s, double t, double r, double q);

        [DllImport(DLLName, EntryPoint = "glTexCoord4dv")]
        public static extern void TexCoord4dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord4f")]
        public static extern void TexCoord4f(float s, float t, float r, float q);

        [DllImport(DLLName, EntryPoint = "glTexCoord4fv")]
        public static extern void TexCoord4fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord4i")]
        public static extern void TexCoord4i(int s, int t, int r, int q);

        [DllImport(DLLName, EntryPoint = "glTexCoord4iv")]
        public static extern void TexCoord4iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoord4s")]
        public static extern void TexCoord4s(short s, short t, short r, short q);

        [DllImport(DLLName, EntryPoint = "glTexCoord4sv")]
        public static extern void TexCoord4sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glTexCoordPointer")]
        public static extern void TexCoordPointer(int size, uint type, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glTexCoordPointer")]
        public static extern void TexCoordPointer(int size, uint type, int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glTexCoordPointer")]
        public static extern void TexCoordPointer(int size, uint type, int stride, float[] pointer);

        [DllImport(DLLName, EntryPoint = "glTexEnvf")]
        public static extern void TexEnvf(uint target, uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glTexEnvfv")]
        public static extern void TexEnvfv(uint target, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glTexEnvi")]
        public static extern void TexEnvi(uint target, uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glTexEnviv")]
        public static extern void TexEnviv(uint target, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glTexGend")]
        public static extern void TexGend(uint coord, uint pname, double param);

        [DllImport(DLLName, EntryPoint = "glTexGendv")]
        public static extern void TexGendv(uint coord, uint pname, double[] dparams);

        [DllImport(DLLName, EntryPoint = "glTexGenf")]
        public static extern void TexGenf(uint coord, uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glTexGenfv")]
        public static extern void TexGenfv(uint coord, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glTexGeni")]
        public static extern void TexGeni(uint coord, uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glTexGeniv")]
        public static extern void TexGeniv(uint coord, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glTexImage1D")]
        public static extern void TexImage1D(uint target, int level, int internalformat, int width, int border, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glTexImage1D")]
        public static extern void TexImage1D(uint target, int level, int internalformat, int width, int border, uint format, uint type, byte[] pixels);

        [DllImport(DLLName, EntryPoint = "glTexImage2D")]
        public static extern void TexImage2D(uint target, int level, int internalformat, int width, int height, int border, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glTexImage2D")]
        public static extern void TexImage2D(uint target, int level, int internalformat, int width, int height, int border, uint format, uint type, byte[] pixels);

        [DllImport(DLLName, EntryPoint = "glTexParameterf")]
        public static extern void TexParameterf(uint target, uint pname, float param);

        [DllImport(DLLName, EntryPoint = "glTexParameterfv")]
        public static extern void TexParameterfv(uint target, uint pname, float[] fparams);

        [DllImport(DLLName, EntryPoint = "glTexParameteri")]
        public static extern void TexParameteri(uint target, uint pname, int param);

        [DllImport(DLLName, EntryPoint = "glTexParameteriv")]
        public static extern void TexParameteriv(uint target, uint pname, int[] iparams);

        [DllImport(DLLName, EntryPoint = "glTexSubImage1D")]
        public static extern void TexSubImage1D(uint target, int level, int xoffset, int width, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glTexSubImage1D")]
        public static extern void TexSubImage1D(uint target, int level, int xoffset, int width, uint format, uint type, byte[] pixels);

        [DllImport(DLLName, EntryPoint = "glTexSubImage2D")]
        public static extern void TexSubImage2D(uint target, int level, int xoffset, int yoffset, int width, int height, uint format, uint type, IntPtr pixels);

        [DllImport(DLLName, EntryPoint = "glTexSubImage2D")]
        public static extern void TexSubImage2D(uint target, int level, int xoffset, int yoffset, int width, int height, uint format, uint type, byte[] pixels);

        [DllImport(DLLName, EntryPoint = "glTranslated")]
        public static extern void Translated(double x, double y, double z);

        [DllImport(DLLName, EntryPoint = "glTranslatef")]
        public static extern void Translatef(float x, float y, float z);

        [DllImport(DLLName, EntryPoint = "glVertex2d")]
        public static extern void Vertex2d(double x, double y);

        [DllImport(DLLName, EntryPoint = "glVertex2dv")]
        public static extern void Vertex2dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glVertex2f")]
        public static extern void Vertex2f(float x, float y);

        [DllImport(DLLName, EntryPoint = "glVertex2fv")]
        public static extern void Vertex2fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glVertex2i")]
        public static extern void Vertex2i(int x, int y);

        [DllImport(DLLName, EntryPoint = "glVertex2iv")]
        public static extern void Vertex2iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glVertex2s")]
        public static extern void Vertex2s(short x, short y);

        [DllImport(DLLName, EntryPoint = "glVertex2sv")]
        public static extern void Vertex2sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glVertex3d")]
        public static extern void Vertex3d(double x, double y, double z);

        [DllImport(DLLName, EntryPoint = "glVertex3dv")]
        public static extern void Vertex3dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glVertex3f")]
        public static extern void Vertex3f(float x, float y, float z);

        [DllImport(DLLName, EntryPoint = "glVertex3fv")]
        public static extern void Vertex3fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glVertex3i")]
        public static extern void Vertex3i(int x, int y, int z);

        [DllImport(DLLName, EntryPoint = "glVertex3iv")]
        public static extern void Vertex3iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glVertex3s")]
        public static extern void Vertex3s(short x, short y, short z);

        [DllImport(DLLName, EntryPoint = "glVertex3sv")]
        public static extern void Vertex3sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glVertex4d")]
        public static extern void Vertex4d(double x, double y, double z, double w);

        [DllImport(DLLName, EntryPoint = "glVertex4dv")]
        public static extern void Vertex4dv(double[] v);

        [DllImport(DLLName, EntryPoint = "glVertex4f")]
        public static extern void Vertex4f(float x, float y, float z, float w);

        [DllImport(DLLName, EntryPoint = "glVertex4fv")]
        public static extern void Vertex4fv(float[] v);

        [DllImport(DLLName, EntryPoint = "glVertex4i")]
        public static extern void Vertex4i(int x, int y, int z, int w);

        [DllImport(DLLName, EntryPoint = "glVertex4iv")]
        public static extern void Vertex4iv(int[] v);

        [DllImport(DLLName, EntryPoint = "glVertex4s")]
        public static extern void Vertex4s(short x, short y, short z, short w);

        [DllImport(DLLName, EntryPoint = "glVertex4sv")]
        public static extern void Vertex4sv(short[] v);

        [DllImport(DLLName, EntryPoint = "glVertexPointer")]
        public static extern void VertexPointer(int size, uint type, int stride, IntPtr pointer);

        [DllImport(DLLName, EntryPoint = "glVertexPointer")]
        public static extern void VertexPointer(int size, uint type, int stride, float[] pointer);

        [DllImport(DLLName, EntryPoint = "glVertexPointer")]
        public static extern void VertexPointer(int size, uint type, int stride, int offset);

        [DllImport(DLLName, EntryPoint = "glViewport")]
        public static extern void Viewport(int x, int y, int width, int height);

        public static glActiveTexture ActiveTexture;
        public static glClientActiveTexture ClientActiveTexture;
        public static glMultiTexCoord1d MultiTexCoord1d;
        public static glMultiTexCoord1dv MultiTexCoord1dv;
        public static glMultiTexCoord1f MultiTexCoord1f;
        public static glMultiTexCoord1fv MultiTexCoord1fv;
        public static glMultiTexCoord1i MultiTexCoord1i;
        public static glMultiTexCoord1iv MultiTexCoord1iv;
        public static glMultiTexCoord1s MultiTexCoord1s;
        public static glMultiTexCoord1sv MultiTexCoord1sv;
        public static glMultiTexCoord2d MultiTexCoord2d;
        public static glMultiTexCoord2dv MultiTexCoord2dv;
        public static glMultiTexCoord2f MultiTexCoord2f;
        public static glMultiTexCoord2fv MultiTexCoord2fv;
        public static glMultiTexCoord2i MultiTexCoord2i;
        public static glMultiTexCoord2iv MultiTexCoord2iv;
        public static glMultiTexCoord2s MultiTexCoord2s;
        public static glMultiTexCoord2sv MultiTexCoord2sv;
        public static glMultiTexCoord3d MultiTexCoord3d;
        public static glMultiTexCoord3dv MultiTexCoord3dv;
        public static glMultiTexCoord3f MultiTexCoord3f;
        public static glMultiTexCoord3fv MultiTexCoord3fv;
        public static glMultiTexCoord3i MultiTexCoord3i;
        public static glMultiTexCoord3iv MultiTexCoord3iv;
        public static glMultiTexCoord3s MultiTexCoord3s;
        public static glMultiTexCoord3sv MultiTexCoord3sv;
        public static glMultiTexCoord4d MultiTexCoord4d;
        public static glMultiTexCoord4dv MultiTexCoord4dv;
        public static glMultiTexCoord4f MultiTexCoord4f;
        public static glMultiTexCoord4fv MultiTexCoord4fv;
        public static glMultiTexCoord4i MultiTexCoord4i;
        public static glMultiTexCoord4iv MultiTexCoord4iv;
        public static glMultiTexCoord4s MultiTexCoord4s;
        public static glMultiTexCoord4sv MultiTexCoord4sv;

        public static glBindBufferARB BindBufferARB;
        public static glDeleteBuffersARB DeleteBuffersARB_;
        public static glGenBuffersARB GenBuffersARB_;
        public static glIsBufferARB IsBufferARB;
        private static glBufferData BufferData_;
        private static glBufferSubData BufferSubData_;
        public static glGetBufferSubDataARB GetBufferSubDataARB;
        public static glMapBufferARB MapBufferARB;
        public static glUnmapBufferARB UnmapBufferARB;
        public static glGetBufferParameterivARB GetBufferParameterivARB;
        public static glGetBufferPointervARB GetBufferPointervARB;

        public static glGenQueriesARB GenQueriesARB_;
        public static glDeleteQueriesARB DeleteQueriesARB_;
        public static glIsQueryARB IsQueryARB;
        public static glBeginQueryARB BeginQueryARB;
        public static glEndQueryARB EndQueryARB;
        public static glGetQueryivARB GetQueryivARB;
        public static glGetQueryObjectivARB GetQueryObjectivARB;
        public static glGetQueryObjectuivARB GetQueryObjectuivARB;

        public static glVertexAttribPointerARB VertexAttribPointerARB;
        public static glEnableVertexAttribArrayARB EnableVertexAttribArrayARB;
        public static glDisableVertexAttribArrayARB DisableVertexAttribArrayARB;
        private static glProgramStringARB ProgramStringARB_;
        public static glBindProgramARB BindProgramARB;
        public static glDeleteProgramsARB DeleteProgramsARB_;
        public static glGenProgramsARB GenProgramsARB_;
        public static glProgramLocalParameter4fARB ProgramLocalParameter4fARB;
        public static glProgramEnvParameter4fARB ProgramEnvParameter4fARB;
        public static glGetProgramivARB GetProgramivARB;
        public static glGetProgramEnvParameterfvARB GetProgramEnvParameterfvARB;
        public static glGetProgramLocalParameterfvARB GetProgramLocalParameterfvARB;
        public static glVertexAttrib2fARB VertexAttrib2fARB;
        public static glVertexAttrib3fARB VertexAttrib3fARB;
        public static glVertexAttrib4fARB VertexAttrib4fARB;

        public static glStencilOpSeparateATI StencilOpSeparateATI;
        public static glStencilFuncSeparateATI StencilFuncSeparateATI;

        public static glGetCompressedTexImageARB GetCompressedTexImageARB;
        public static glCompressedTexImage2DARB CompressedTexImage2DARB;

        public static glTexImage3D TexImage3D;
        public static glBlendEquation BlendEquation;
        public static glWindowPos2f WindowPos2f;

        public static glCreateShaderObjectARB CreateShaderObjectARB;
        private static glShaderSourceARB ShaderSourceARB_;
        public static glCompileShaderARB CompileShaderARB;
        public static glDeleteObjectARB DeleteObjectARB;
        public static glGetHandleARB GetHandleARB;
        public static glDetachObjectARB DetachObjectARB;
        public static glCreateProgramObjectARB CreateProgramObjectARB;
        public static glAttachObjectARB AttachObjectARB;
        public static glLinkProgramARB LinkProgramARB;
        public static glUseProgramObjectARB UseProgramObjectARB;
        public static glValidateProgramARB ValidateProgramARB;
        public static glGetObjectParameterfvARB GetObjectParameterfvARB;
        public static glGetObjectParameterivARB GetObjectParameterivARB;
        public static glGetActiveAttribARB GetActiveAttribARB;
        public static glGetActiveUniformARB GetActiveUniformARB;
        public static glGetAttachedObjectsARB GetAttachedObjectsARB;
        public static glGetAttribLocationARB GetAttribLocationARB;
        public static glGetShaderSourceARB GetShaderSourceARB;
        public static glGetUniformfvARB GetUniformfvARB;
        public static glGetUniformivARB GetUniformivARB;
        public static glGetUniformLocationARB GetUniformLocationARB;
        private static glGetInfoLogARB GetInfoLogARB_;
        public static glBindAttribLocationARB BindAttribLocationARB;
        public static glUniform1fARB Uniform1fARB;
        public static glUniform2fARB Uniform2fARB;
        public static glUniform3fARB Uniform3fARB;
        public static glUniform4fARB Uniform4fARB;
        public static glUniform1iARB Uniform1iARB;
        public static glUniform2iARB Uniform2iARB;
        public static glUniform3iARB Uniform3iARB;
        public static glUniform4iARB Uniform4iARB;
        public static glUniform1fvARB Uniform1fvARB;
        public static glUniform2fvARB Uniform2fvARB;
        public static glUniform3fvARB Uniform3fvARB;
        public static glUniform4fvARB Uniform4fvARB;
        public static glUniform1ivARB Uniform1ivARB;
        public static glUniform2ivARB Uniform2ivARB;
        public static glUniform3ivARB Uniform3ivARB;
        public static glUniform4ivARB Uniform4ivARB;
        public static glUniformMatrix2fvARB UniformMatrix2fvARB;
        public static glUniformMatrix3fvARB UniformMatrix3fvARB;
        public static glUniformMatrix4fvARB UniformMatrix4fvARB;

        public static int Handle;
        
        public static void TexImage2D(uint target, int level, int internalformat, int border, Bitmap bmp)
        {
            BitmapData data = bmp.LockBits(
        		new Rectangle(0, 0, bmp.Size.Width, bmp.Size.Height),
        		ImageLockMode.ReadOnly,
        		PixelFormat.Format32bppArgb);
//            byte[] imagedata = new Byte[bmp.Size.Width * bmp.Size.Height * 4];
            
            TexImage2D(target,
                       level,
                       internalformat,
                       bmp.Size.Width,
                       bmp.Size.Height,
                       border,
                       gl.BGRA,
                       gl.UNSIGNED_BYTE,
                       data.Scan0);
            bmp.UnlockBits(data);
        }

        public static void TexImage2D(uint target, int level, int internalformat, int border, string filename)
        {
            using (Bitmap bmp = new Bitmap(filename))
            {
                gl.TexImage2D(gl.TEXTURE_2D, 0, (int)gl.RGBA8, 0, bmp);
            }
        }

        public static void TexImage2D(uint target, int level, int internalformat, int border, Stream st)
        {
            using (Bitmap bmp = new Bitmap(st))
            {
                gl.TexImage2D(gl.TEXTURE_2D, 0, (int)gl.RGBA8, 0, bmp);
            }
        }

        public static void ColorPointer(int size, uint type, int stride, Array data)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                ColorPointer(size, type, stride, h.AddrOfPinnedObject());
            }
            finally
            {
                h.Free();
            }
        }

        public static void NormalPointer(uint type, int stride, Array data)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                NormalPointer(type, stride, h.AddrOfPinnedObject());
            }
            finally
            {
                h.Free();
            }
        }

        public static void TexCoordPointer(int size, uint type, int stride, Array data)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                TexCoordPointer(size, type, stride, h.AddrOfPinnedObject());
            }
            finally
            {
                h.Free();
            }
        }

        public static void VertexPointer(int size, uint type, int stride, Array data)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                VertexPointer(size, type, stride, h.AddrOfPinnedObject());
            }
            finally
            {
                h.Free();
            }
        }

        public static void BufferData(uint target, int size, IntPtr data, int usage)
        {
            BufferData_(target, size, data, usage);
        }

        public static void BufferData(uint target, int size, Array data, int usage)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                BufferData(target, size, h.AddrOfPinnedObject(), usage);
            }
            finally
            {
                h.Free();
            }
        }

        public static void BufferSubData(uint target, int offset, int size, IntPtr data)
        {
            BufferSubData_(target, offset, size, data);
        }

        public static void BufferSubData(uint target, int offset, int size, Array data)
        {
            GCHandle h = GCHandle.Alloc(data, GCHandleType.Pinned);
            try
            {
                BufferSubData(target, offset, size, h.AddrOfPinnedObject());
            }
            finally
            {
                h.Free();
            }
        }

        public static string GetInfoLog(int obj)
        {
            StringBuilder info = new StringBuilder(1024);
            int length = 1024;
            gl.GetInfoLogARB_(obj, 1024, ref length, info);
            return info.ToString();
        }

        public static void ShaderSource(int shader, string source)
        {
            string[] s = new string[] { source };
            int[] l = new int[1] { source.Length };
            gl.ShaderSourceARB_(shader, 1, s, l);
        }

        public static string GetString(int name)
        {
            IntPtr i = GetString_(name);
            return Marshal.PtrToStringAnsi(i);
        }

        public static uint GenTexture()
        {
            uint i = 0;
            GenTextures(1, ref i);
            return i;
        }

        public static uint[] GenTextures(int n)
        {
            uint[] res = new uint[n];
            GenTextures(n, res);
            return res;
        }

        public static void ProgramStringARB(uint target, string source)
        {
            ProgramStringARB_(target, PROGRAM_FORMAT_ASCII_ARB, source.Length, source);
        }

        public static int[] GetIntegerv(uint name, int size)
        {
            int[] res = new int[size];
            GetIntegerv(name, res);
            return res;
        }

        public static float[] GetFloatv(uint name, int size)
        {
            float[] res = new float[size];
            GetFloatv(name, res);
            return res;
        }

        public static double[] GetDoublev(uint name, int size)
        {
            double[] res = new double[size];
            GetDoublev(name, res);
            return res;
        }

        public static void Viewport(Rectangle r)
        {
            Viewport(r.Left, r.Top, r.Width, r.Height);
        }

        public static void GenBuffersARB(int n, int[] buffers)
        {
            GenBuffersARB_(n, buffers);
        }

        public static int[] GenBuffersARB(int n)
        {
            int[] result = new int[n];
            GenBuffersARB_(n, result);
            return result;
        }

        public static int GenBufferARB()
        {
            int[] result = new int[1];
            GenBuffersARB_(1, result);
            return result[0];
        }

        public static void DeleteBuffersARB(int n, int[] buffers)
        {
            DeleteBuffersARB_(n, buffers);
        }

        public static void DeleteBuffersARB(int[] buffers)
        {
            DeleteBuffersARB_(buffers.Length, buffers);
        }

        public static void GenQueriesARB(int n, int[] queries)
        {
            GenQueriesARB_(n, queries);
        }

        public static int[] GenQueriesARB(int n)
        {
            int[] result = new int[n];
            GenQueriesARB_(n, result);
            return result;
        }

        public static int GenQueryARB()
        {
            int[] result = new int[1];
            GenQueriesARB_(1, result);
            return result[0];
        }

        public static void DeleteQueriesARB(int n, int[] queries)
        {
            DeleteQueriesARB_(n, queries);
        }

        public static void DeleteQueriesARB(int[] queries)
        {
            DeleteQueriesARB_(queries.Length, queries);
        }

        public static void GenProgramsARB(int n, int[] progs)
        {
            GenProgramsARB_(n, progs);
        }

        public static int[] GenProgramsARB(int n)
        {
            int[] result = new int[n];
            GenProgramsARB_(n, result);
            return result;
        }

        public static int GenProgramARB()
        {
            int[] result = new int[1];
            GenProgramsARB_(1, result);
            return result[0];
        }

        public static int GenProgramARB(uint target, string source)
        {
            int r = GenProgramARB();
            BindProgramARB(target, r);
            ProgramStringARB(target, source);
            return r;
        }

        public static void DeleteProgramsARB(int n, int[] progs)
        {
            DeleteProgramsARB_(n, progs);
        }

        public static void DeleteProgramsARB(int[] progs)
        {
            DeleteProgramsARB_(progs.Length, progs);
        }

        public static void DeleteProgramARB(int prog)
        {
            int[] progs = new int[] { prog };
            DeleteProgramsARB_(1, progs);
        }

        private static Delegate GetAddress(string name, Type t)
        {
            int addr = wgl.GetProcAddress(name);
            if (addr == 0) return null; else return Marshal.GetDelegateForFunctionPointer(new IntPtr(addr), t);
        }

        public static void LoadExtensions()
        {
            ActiveTexture = (glActiveTexture)GetAddress("glActiveTexture", typeof(glActiveTexture));
            ClientActiveTexture = (glClientActiveTexture)GetAddress("glClientActiveTexture", typeof(glClientActiveTexture));
            MultiTexCoord1d = (glMultiTexCoord1d)GetAddress("glMultiTexCoord1d", typeof(glMultiTexCoord1d));
            MultiTexCoord1dv = (glMultiTexCoord1dv)GetAddress("glMultiTexCoord1dv", typeof(glMultiTexCoord1dv));
            MultiTexCoord1f = (glMultiTexCoord1f)GetAddress("glMultiTexCoord1f", typeof(glMultiTexCoord1f));
            MultiTexCoord1fv = (glMultiTexCoord1fv)GetAddress("glMultiTexCoord1fv", typeof(glMultiTexCoord1fv));
            MultiTexCoord1i = (glMultiTexCoord1i)GetAddress("glMultiTexCoord1i", typeof(glMultiTexCoord1i));
            MultiTexCoord1iv = (glMultiTexCoord1iv)GetAddress("glMultiTexCoord1iv", typeof(glMultiTexCoord1iv));
            MultiTexCoord1s = (glMultiTexCoord1s)GetAddress("glMultiTexCoord1s", typeof(glMultiTexCoord1s));
            MultiTexCoord1sv = (glMultiTexCoord1sv)GetAddress("glMultiTexCoord1sv", typeof(glMultiTexCoord1sv));
            MultiTexCoord2d = (glMultiTexCoord2d)GetAddress("glMultiTexCoord2d", typeof(glMultiTexCoord2d));
            MultiTexCoord2dv = (glMultiTexCoord2dv)GetAddress("glMultiTexCoord2dv", typeof(glMultiTexCoord2dv));
            MultiTexCoord2f = (glMultiTexCoord2f)GetAddress("glMultiTexCoord2f", typeof(glMultiTexCoord2f));
            MultiTexCoord2fv = (glMultiTexCoord2fv)GetAddress("glMultiTexCoord2fv", typeof(glMultiTexCoord2fv));
            MultiTexCoord2i = (glMultiTexCoord2i)GetAddress("glMultiTexCoord2i", typeof(glMultiTexCoord2i));
            MultiTexCoord2iv = (glMultiTexCoord2iv)GetAddress("glMultiTexCoord2iv", typeof(glMultiTexCoord2iv));
            MultiTexCoord2s = (glMultiTexCoord2s)GetAddress("glMultiTexCoord2s", typeof(glMultiTexCoord2s));
            MultiTexCoord2sv = (glMultiTexCoord2sv)GetAddress("glMultiTexCoord2sv", typeof(glMultiTexCoord2sv));
            MultiTexCoord3d = (glMultiTexCoord3d)GetAddress("glMultiTexCoord3d", typeof(glMultiTexCoord3d));
            MultiTexCoord3dv = (glMultiTexCoord3dv)GetAddress("glMultiTexCoord3dv", typeof(glMultiTexCoord3dv));
            MultiTexCoord3f = (glMultiTexCoord3f)GetAddress("glMultiTexCoord3f", typeof(glMultiTexCoord3f));
            MultiTexCoord3fv = (glMultiTexCoord3fv)GetAddress("glMultiTexCoord3fv", typeof(glMultiTexCoord3fv));
            MultiTexCoord3i = (glMultiTexCoord3i)GetAddress("glMultiTexCoord3i", typeof(glMultiTexCoord3i));
            MultiTexCoord3iv = (glMultiTexCoord3iv)GetAddress("glMultiTexCoord3iv", typeof(glMultiTexCoord3iv));
            MultiTexCoord3s = (glMultiTexCoord3s)GetAddress("glMultiTexCoord3s", typeof(glMultiTexCoord3s));
            MultiTexCoord3sv = (glMultiTexCoord3sv)GetAddress("glMultiTexCoord3sv", typeof(glMultiTexCoord3sv));
            MultiTexCoord4d = (glMultiTexCoord4d)GetAddress("glMultiTexCoord4d", typeof(glMultiTexCoord4d));
            MultiTexCoord4dv = (glMultiTexCoord4dv)GetAddress("glMultiTexCoord4dv", typeof(glMultiTexCoord4dv));
            MultiTexCoord4f = (glMultiTexCoord4f)GetAddress("glMultiTexCoord4f", typeof(glMultiTexCoord4f));
            MultiTexCoord4fv = (glMultiTexCoord4fv)GetAddress("glMultiTexCoord4fv", typeof(glMultiTexCoord4fv));
            MultiTexCoord4i = (glMultiTexCoord4i)GetAddress("glMultiTexCoord4i", typeof(glMultiTexCoord4i));
            MultiTexCoord4iv = (glMultiTexCoord4iv)GetAddress("glMultiTexCoord4iv", typeof(glMultiTexCoord4iv));
            MultiTexCoord4s = (glMultiTexCoord4s)GetAddress("glMultiTexCoord4s", typeof(glMultiTexCoord4s));
            MultiTexCoord4sv = (glMultiTexCoord4sv)GetAddress("glMultiTexCoord4sv", typeof(glMultiTexCoord4sv));
            BindBufferARB = (glBindBufferARB)GetAddress("glBindBufferARB", typeof(glBindBufferARB));
            DeleteBuffersARB_ = (glDeleteBuffersARB)GetAddress("glDeleteBuffersARB", typeof(glDeleteBuffersARB));
            GenBuffersARB_ = (glGenBuffersARB)GetAddress("glGenBuffersARB", typeof(glGenBuffersARB));
            IsBufferARB = (glIsBufferARB)GetAddress("glIsBufferARB", typeof(glIsBufferARB));
            BufferData_ = (glBufferData)GetAddress("glBufferData", typeof(glBufferData));
            BufferSubData_ = (glBufferSubData)GetAddress("glBufferSubData", typeof(glBufferSubData));
            GetBufferSubDataARB = (glGetBufferSubDataARB)GetAddress("glGetBufferSubDataARB", typeof(glGetBufferSubDataARB));
            MapBufferARB = (glMapBufferARB)GetAddress("glMapBufferARB", typeof(glMapBufferARB));
            UnmapBufferARB = (glUnmapBufferARB)GetAddress("glUnmapBufferARB", typeof(glUnmapBufferARB));
            GetBufferParameterivARB = (glGetBufferParameterivARB)GetAddress("glGetBufferParameterivARB", typeof(glGetBufferParameterivARB));
            GetBufferPointervARB = (glGetBufferPointervARB)GetAddress("glGetBufferPointervARB", typeof(glGetBufferPointervARB));
            GenQueriesARB_ = (glGenQueriesARB)GetAddress("glGenQueriesARB", typeof(glGenQueriesARB));
            DeleteQueriesARB_ = (glDeleteQueriesARB)GetAddress("glDeleteQueriesARB", typeof(glDeleteQueriesARB));
            IsQueryARB = (glIsQueryARB)GetAddress("glIsQueryARB", typeof(glIsQueryARB));
            BeginQueryARB = (glBeginQueryARB)GetAddress("glBeginQueryARB", typeof(glBeginQueryARB));
            EndQueryARB = (glEndQueryARB)GetAddress("glEndQueryARB", typeof(glEndQueryARB));
            GetQueryivARB = (glGetQueryivARB)GetAddress("glGetQueryivARB", typeof(glGetQueryivARB));
            GetQueryObjectivARB = (glGetQueryObjectivARB)GetAddress("glGetQueryObjectivARB", typeof(glGetQueryObjectivARB));
            GetQueryObjectuivARB = (glGetQueryObjectuivARB)GetAddress("glGetQueryObjectuivARB", typeof(glGetQueryObjectuivARB));
            VertexAttribPointerARB = (glVertexAttribPointerARB)GetAddress("glVertexAttribPointerARB", typeof(glVertexAttribPointerARB));
            EnableVertexAttribArrayARB = (glEnableVertexAttribArrayARB)GetAddress("glEnableVertexAttribArrayARB", typeof(glEnableVertexAttribArrayARB));
            DisableVertexAttribArrayARB = (glDisableVertexAttribArrayARB)GetAddress("glDisableVertexAttribArrayARB", typeof(glDisableVertexAttribArrayARB));
            ProgramStringARB_ = (glProgramStringARB)GetAddress("glProgramStringARB", typeof(glProgramStringARB));
            BindProgramARB = (glBindProgramARB)GetAddress("glBindProgramARB", typeof(glBindProgramARB));
            DeleteProgramsARB_ = (glDeleteProgramsARB)GetAddress("glDeleteProgramsARB", typeof(glDeleteProgramsARB));
            GenProgramsARB_ = (glGenProgramsARB)GetAddress("glGenProgramsARB", typeof(glGenProgramsARB));
            ProgramLocalParameter4fARB = (glProgramLocalParameter4fARB)GetAddress("glProgramLocalParameter4fARB", typeof(glProgramLocalParameter4fARB));
            ProgramEnvParameter4fARB = (glProgramEnvParameter4fARB)GetAddress("glProgramEnvParameter4fARB", typeof(glProgramEnvParameter4fARB));
            GetProgramivARB = (glGetProgramivARB)GetAddress("glGetProgramivARB", typeof(glGetProgramivARB));
            GetProgramEnvParameterfvARB = (glGetProgramEnvParameterfvARB)GetAddress("glGetProgramEnvParameterfvARB", typeof(glGetProgramEnvParameterfvARB));
            GetProgramLocalParameterfvARB = (glGetProgramLocalParameterfvARB)GetAddress("glGetProgramLocalParameterfvARB", typeof(glGetProgramLocalParameterfvARB));
            VertexAttrib2fARB = (glVertexAttrib2fARB)GetAddress("glVertexAttrib2fARB", typeof(glVertexAttrib2fARB));
            VertexAttrib3fARB = (glVertexAttrib3fARB)GetAddress("glVertexAttrib3fARB", typeof(glVertexAttrib3fARB));
            VertexAttrib4fARB = (glVertexAttrib4fARB)GetAddress("glVertexAttrib4fARB", typeof(glVertexAttrib4fARB));
            StencilOpSeparateATI = (glStencilOpSeparateATI)GetAddress("glStencilOpSeparateATI", typeof(glStencilOpSeparateATI));
            StencilFuncSeparateATI = (glStencilFuncSeparateATI)GetAddress("glStencilFuncSeparateATI", typeof(glStencilFuncSeparateATI));
            GetCompressedTexImageARB = (glGetCompressedTexImageARB)GetAddress("glGetCompressedTexImageARB", typeof(glGetCompressedTexImageARB));
            CompressedTexImage2DARB = (glCompressedTexImage2DARB)GetAddress("glCompressedTexImage2DARB", typeof(glCompressedTexImage2DARB));
            TexImage3D = (glTexImage3D)GetAddress("glTexImage3D", typeof(glTexImage3D));
            BlendEquation = (glBlendEquation)GetAddress("glBlendEquation", typeof(glBlendEquation));
            WindowPos2f = (glWindowPos2f)GetAddress("glWindowPos2f", typeof(glWindowPos2f));
            CreateShaderObjectARB = (glCreateShaderObjectARB)GetAddress("glCreateShaderObjectARB", typeof(glCreateShaderObjectARB));
            ShaderSourceARB_ = (glShaderSourceARB)GetAddress("glShaderSourceARB", typeof(glShaderSourceARB));
            CompileShaderARB = (glCompileShaderARB)GetAddress("glCompileShaderARB", typeof(glCompileShaderARB));
            DeleteObjectARB = (glDeleteObjectARB)GetAddress("glDeleteObjectARB", typeof(glDeleteObjectARB));
            GetHandleARB = (glGetHandleARB)GetAddress("glGetHandleARB", typeof(glGetHandleARB));
            DetachObjectARB = (glDetachObjectARB)GetAddress("glDetachObjectARB", typeof(glDetachObjectARB));
            CreateProgramObjectARB = (glCreateProgramObjectARB)GetAddress("glCreateProgramObjectARB", typeof(glCreateProgramObjectARB));
            AttachObjectARB = (glAttachObjectARB)GetAddress("glAttachObjectARB", typeof(glAttachObjectARB));
            LinkProgramARB = (glLinkProgramARB)GetAddress("glLinkProgramARB", typeof(glLinkProgramARB));
            UseProgramObjectARB = (glUseProgramObjectARB)GetAddress("glUseProgramObjectARB", typeof(glUseProgramObjectARB));
            ValidateProgramARB = (glValidateProgramARB)GetAddress("glValidateProgramARB", typeof(glValidateProgramARB));
            GetObjectParameterfvARB = (glGetObjectParameterfvARB)GetAddress("glGetObjectParameterfvARB", typeof(glGetObjectParameterfvARB));
            GetObjectParameterivARB = (glGetObjectParameterivARB)GetAddress("glGetObjectParameterivARB", typeof(glGetObjectParameterivARB));
            GetActiveAttribARB = (glGetActiveAttribARB)GetAddress("glGetActiveAttribARB", typeof(glGetActiveAttribARB));
            GetActiveUniformARB = (glGetActiveUniformARB)GetAddress("glGetActiveUniformARB", typeof(glGetActiveUniformARB));
            GetAttachedObjectsARB = (glGetAttachedObjectsARB)GetAddress("glGetAttachedObjectsARB", typeof(glGetAttachedObjectsARB));
            GetAttribLocationARB = (glGetAttribLocationARB)GetAddress("glGetAttribLocationARB", typeof(glGetAttribLocationARB));
            GetShaderSourceARB = (glGetShaderSourceARB)GetAddress("glGetShaderSourceARB", typeof(glGetShaderSourceARB));
            GetUniformfvARB = (glGetUniformfvARB)GetAddress("glGetUniformfvARB", typeof(glGetUniformfvARB));
            GetUniformivARB = (glGetUniformivARB)GetAddress("glGetUniformivARB", typeof(glGetUniformivARB));
            GetUniformLocationARB = (glGetUniformLocationARB)GetAddress("glGetUniformLocationARB", typeof(glGetUniformLocationARB));
            GetInfoLogARB_ = (glGetInfoLogARB)GetAddress("glGetInfoLogARB", typeof(glGetInfoLogARB));
            BindAttribLocationARB = (glBindAttribLocationARB)GetAddress("glBindAttribLocationARB", typeof(glBindAttribLocationARB));
            Uniform1fARB = (glUniform1fARB)GetAddress("glUniform1fARB", typeof(glUniform1fARB));
            Uniform2fARB = (glUniform2fARB)GetAddress("glUniform2fARB", typeof(glUniform2fARB));
            Uniform3fARB = (glUniform3fARB)GetAddress("glUniform3fARB", typeof(glUniform3fARB));
            Uniform4fARB = (glUniform4fARB)GetAddress("glUniform4fARB", typeof(glUniform4fARB));
            Uniform1iARB = (glUniform1iARB)GetAddress("glUniform1iARB", typeof(glUniform1iARB));
            Uniform2iARB = (glUniform2iARB)GetAddress("glUniform2iARB", typeof(glUniform2iARB));
            Uniform3iARB = (glUniform3iARB)GetAddress("glUniform3iARB", typeof(glUniform3iARB));
            Uniform4iARB = (glUniform4iARB)GetAddress("glUniform4iARB", typeof(glUniform4iARB));
            Uniform1fvARB = (glUniform1fvARB)GetAddress("glUniform1fvARB", typeof(glUniform1fvARB));
            Uniform2fvARB = (glUniform2fvARB)GetAddress("glUniform2fvARB", typeof(glUniform2fvARB));
            Uniform3fvARB = (glUniform3fvARB)GetAddress("glUniform3fvARB", typeof(glUniform3fvARB));
            Uniform4fvARB = (glUniform4fvARB)GetAddress("glUniform4fvARB", typeof(glUniform4fvARB));
            Uniform1ivARB = (glUniform1ivARB)GetAddress("glUniform1ivARB", typeof(glUniform1ivARB));
            Uniform2ivARB = (glUniform2ivARB)GetAddress("glUniform2ivARB", typeof(glUniform2ivARB));
            Uniform3ivARB = (glUniform3ivARB)GetAddress("glUniform3ivARB", typeof(glUniform3ivARB));
            Uniform4ivARB = (glUniform4ivARB)GetAddress("glUniform4ivARB", typeof(glUniform4ivARB));
            UniformMatrix2fvARB = (glUniformMatrix2fvARB)GetAddress("glUniformMatrix2fvARB", typeof(glUniformMatrix2fvARB));
            UniformMatrix3fvARB = (glUniformMatrix3fvARB)GetAddress("glUniformMatrix3fvARB", typeof(glUniformMatrix3fvARB));
            UniformMatrix4fvARB = (glUniformMatrix4fvARB)GetAddress("glUniformMatrix4fvARB", typeof(glUniformMatrix4fvARB));
        }

    }

}
