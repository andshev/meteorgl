using System;
using System.Runtime.InteropServices;
using System.Security;
using ManagedOpenGl.Delegates;

namespace ManagedOpenGl
{
    public class glu
    {
        private const string GLUDLLName = "glu32.dll";

        public const bool VERSION_1_1 = true;
        public const bool VERSION_1_2 = true;
        public const bool VERSION_1_3 = true;
        public const uint INVALID_ENUM = 100900;
        public const uint INVALID_VALUE = 100901;
        public const uint OUT_OF_MEMORY = 100902;
        public const uint INCOMPATIBLE_GL_VERSION = 100903;
        public const uint INVALID_OPERATION = 100904;
        public const uint VERSION = 100800;
        public const uint EXTENSIONS = 100801;
        public const uint TRUE = 1;
        public const uint FALSE = 0;
        public const uint SMOOTH = 100000;
        public const uint FLAT = 100001;
        public const uint NONE = 100002;
        public const uint POINT = 100010;
        public const uint LINE = 100011;
        public const uint FILL = 100012;
        public const uint SILHOUETTE = 100013;
        public const uint OUTSIDE = 100020;
        public const uint INSIDE = 100021;
        public const double TESS_MAX_COORD = 1.0e150;
        public const uint TESS_WINDING_RULE = 100140;
        public const uint TESS_BOUNDARY_ONLY = 100141;
        public const uint TESS_TOLERANCE = 100142;
        public const uint TESS_WINDING_ODD = 100130;
        public const uint TESS_WINDING_NONZERO = 100131;
        public const uint TESS_WINDING_POSITIVE = 100132;
        public const uint TESS_WINDING_NEGATIVE = 100133;
        public const uint TESS_WINDING_ABS_GEQ_TWO = 100134;
        public const uint TESS_BEGIN = 100100;
        public const uint BEGIN = 100100;
        public const uint TESS_VERTEX = 100101;
        public const uint VERTEX = 100101;
        public const uint TESS_END = 100102;
        public const uint END = 100102;
        public const uint TESS_ERROR = 100103;
        public const uint TESS_EDGE_FLAG = 100104;
        public const uint EDGE_FLAG = 100104;
        public const uint TESS_COMBINE = 100105;
        public const uint TESS_BEGIN_DATA = 100106;
        public const uint TESS_VERTEX_DATA = 100107;
        public const uint TESS_END_DATA = 100108;
        public const uint TESS_ERROR_DATA = 100109;
        public const uint TESS_EDGE_FLAG_DATA = 100110;
        public const uint TESS_COMBINE_DATA = 100111;
        public const uint TESS_ERROR1 = 100151;
        public const uint TESS_ERROR2 = 100152;
        public const uint TESS_ERROR3 = 100153;
        public const uint TESS_ERROR4 = 100154;
        public const uint TESS_ERROR5 = 100155;
        public const uint TESS_ERROR6 = 100156;
        public const uint TESS_ERROR7 = 100157;
        public const uint TESS_ERROR8 = 100158;
        public const uint TESS_MISSING_BEGIN_POLYGON = TESS_ERROR1;
        public const uint TESS_MISSING_BEGIN_CONTOUR = TESS_ERROR2;
        public const uint TESS_MISSING_END_POLYGON = TESS_ERROR3;
        public const uint TESS_MISSING_END_CONTOUR = TESS_ERROR4;
        public const uint TESS_COORD_TOO_LARGE = TESS_ERROR5;
        public const uint TESS_NEED_COMBINE_CALLBACK = TESS_ERROR6;
        public const uint AUTO_LOAD_MATRIX = 100200;
        public const uint CULLING = 100201;
        public const uint PARAMETRIC_TOLERANCE = 100202;
        public const uint SAMPLING_TOLERANCE = 100203;
        public const uint DISPLAY_MODE = 100204;
        public const uint SAMPLING_METHOD = 100205;
        public const uint U_STEP = 100206;
        public const uint V_STEP = 100207;
        public const uint NURBS_MODE = 100160;
        public const uint NURBS_MODE_EXT = 100160;
        public const uint NURBS_TESSELLATOR = 100161;
        public const uint NURBS_TESSELLATOR_EXT = 100161;
        public const uint NURBS_RENDERER = 100162;
        public const uint NURBS_RENDERER_EXT = 100162;
        public const uint OBJECT_PARAMETRIC_ERROR = 100208;
        public const uint OBJECT_PARAMETRIC_ERROR_EXT = 100208;
        public const uint OBJECT_PATH_LENGTH = 100209;
        public const uint OBJECT_PATH_LENGTH_EXT = 100209;
        public const uint PATH_LENGTH = 100215;
        public const uint PARAMETRIC_ERROR = 100216;
        public const uint DOMAIN_DISTANCE = 100217;
        public const uint MAP1_TRIM_2 = 100210;
        public const uint MAP1_TRIM_3 = 100211;
        public const uint OUTLINE_POLYGON = 100240;
        public const uint OUTLINE_PATCH = 100241;
        public const uint NURBS_ERROR = 100103;
        public const uint ERROR = 100103;
        public const uint NURBS_BEGIN = 100164;
        public const uint NURBS_BEGIN_EXT = 100164;
        public const uint NURBS_VERTEX = 100165;
        public const uint NURBS_VERTEX_EXT = 100165;
        public const uint NURBS_NORMAL = 100166;
        public const uint NURBS_NORMAL_EXT = 100166;
        public const uint NURBS_COLOR = 100167;
        public const uint NURBS_COLOR_EXT = 100167;
        public const uint NURBS_TEXTURE_COORD = 100168;
        public const uint NURBS_TEX_COORD_EXT = 100168;
        public const uint NURBS_END = 100169;
        public const uint NURBS_END_EXT = 100169;
        public const uint NURBS_BEGIN_DATA = 100170;
        public const uint NURBS_BEGIN_DATA_EXT = 100170;
        public const uint NURBS_VERTEX_DATA = 100171;
        public const uint NURBS_VERTEX_DATA_EXT = 100171;
        public const uint NURBS_NORMAL_DATA = 100172;
        public const uint NURBS_NORMAL_DATA_EXT = 100172;
        public const uint NURBS_COLOR_DATA = 100173;
        public const uint NURBS_COLOR_DATA_EXT = 100173;
        public const uint NURBS_TEXTURE_COORD_DATA = 100174;
        public const uint NURBS_TEX_COORD_DATA_EXT = 100174;
        public const uint NURBS_END_DATA = 100175;
        public const uint NURBS_END_DATA_EXT = 100175;
        public const uint NURBS_ERROR1 = 100251;
        public const uint NURBS_ERROR2 = 100252;
        public const uint NURBS_ERROR3 = 100253;
        public const uint NURBS_ERROR4 = 100254;
        public const uint NURBS_ERROR5 = 100255;
        public const uint NURBS_ERROR6 = 100256;
        public const uint NURBS_ERROR7 = 100257;
        public const uint NURBS_ERROR8 = 100258;
        public const uint NURBS_ERROR9 = 100259;
        public const uint NURBS_ERROR10 = 100260;
        public const uint NURBS_ERROR11 = 100261;
        public const uint NURBS_ERROR12 = 100262;
        public const uint NURBS_ERROR13 = 100263;
        public const uint NURBS_ERROR14 = 100264;
        public const uint NURBS_ERROR15 = 100265;
        public const uint NURBS_ERROR16 = 100266;
        public const uint NURBS_ERROR17 = 100267;
        public const uint NURBS_ERROR18 = 100268;
        public const uint NURBS_ERROR19 = 100269;
        public const uint NURBS_ERROR20 = 100270;
        public const uint NURBS_ERROR21 = 100271;
        public const uint NURBS_ERROR22 = 100272;
        public const uint NURBS_ERROR23 = 100273;
        public const uint NURBS_ERROR24 = 100274;
        public const uint NURBS_ERROR25 = 100275;
        public const uint NURBS_ERROR26 = 100276;
        public const uint NURBS_ERROR27 = 100277;
        public const uint NURBS_ERROR28 = 100278;
        public const uint NURBS_ERROR29 = 100279;
        public const uint NURBS_ERROR30 = 100280;
        public const uint NURBS_ERROR31 = 100281;
        public const uint NURBS_ERROR32 = 100282;
        public const uint NURBS_ERROR33 = 100283;
        public const uint NURBS_ERROR34 = 100284;
        public const uint NURBS_ERROR35 = 100285;
        public const uint NURBS_ERROR36 = 100286;
        public const uint NURBS_ERROR37 = 100287;
        public const uint CW = 100120;
        public const uint CCW = 100121;
        public const uint INTERIOR = 100122;
        public const uint EXTERIOR = 100123;
        public const uint UNKNOWN = 100124;
        public const uint EXT_object_space_tess = 1;
        public const uint EXT_nurbs_tessellator = 1;

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUnurbs
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUquadric
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUtesselator
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUnurbsObj
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUquadricObj
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUtesselatorObj
        {
            private IntPtr Data;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct GLUtriangulatorObj
        {
            private IntPtr Data;
        }

        private static gluNurbsBeginCallback nurbsBeginCallback;
        private static gluNurbsBeginDataCallback nurbsBeginDataCallback;
        private static gluNurbsColorCallback nurbsColorCallback;
        private static gluNurbsColorDataCallback nurbsColorDataCallback;
        private static gluNurbsEndCallback nurbsEndCallback;
        private static gluNurbsEndDataCallback nurbsEndDataCallback;
        private static gluNurbsErrorCallback nurbsErrorCallback;
        private static gluNurbsNormalCallback nurbsNormalCallback;
        private static gluNurbsNormalDataCallback nurbsNormalDataCallback;
        private static gluNurbsTexCoordCallback nurbsTexCoordCallback;
        private static gluNurbsTexCoordDataCallback nurbsTexCoordDataCallback;
        private static gluNurbsVertexCallback nurbsVertexCallback;
        private static gluNurbsVertexDataCallback nurbsVertexDataCallback;
        private static gluQuadricErrorCallback quadricErrorCallback;
        private static gluTessBeginCallback tessBeginCallback;
        private static gluTessBeginDataCallback tessBeginDataCallback;
        private static gluTessCombineCallback tessCombineCallback;
        private static gluTessCombineCallback1 tessCombineCallback1;
        private static gluTessCombineDataCallback tessCombineDataCallback;
        private static gluTessEdgeFlagCallback tessEdgeFlagCallback;
        private static gluTessEdgeFlagDataCallback tessEdgeFlagDataCallback;
        private static gluTessEndCallback tessEndCallback;
        private static gluTessEndDataCallback tessEndDataCallback;
        private static gluTessErrorCallback tessErrorCallback;
        private static gluTessErrorDataCallback tessErrorDataCallback;
        private static gluTessVertexCallback tessVertexCallback;
        private static gluTessVertexCallback1 tessVertexCallback1;
        private static gluTessVertexDataCallback tessVertexDataCallback;

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsBeginCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsBeginDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsColorCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsColorDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsEndCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsEndDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsErrorCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsNormalCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsNormalDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsTexCoordCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsTexCoordDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsVertexCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluNurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsVertexDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluQuadricCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluQuadricCallback([In] GLUquadric quad, int which, [In] gluQuadricErrorCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessBeginCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessBeginDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineCallback1 func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessEdgeFlagCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessEdgeFlagDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessEndCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessEndDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessErrorCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessErrorDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexCallback1 func);

        [DllImport(GLUDLLName, EntryPoint = "gluTessCallback"), SuppressUnmanagedCodeSecurity]
        private static extern void __gluTessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexDataCallback func);

        [DllImport(GLUDLLName, EntryPoint = "gluBeginCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void BeginCurve([In] GLUnurbs nurb);
       
        [DllImport(GLUDLLName, EntryPoint = "gluBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void BeginPolygon([In] GLUtesselator tess);

        [DllImport(GLUDLLName, EntryPoint = "gluBeginSurface"), SuppressUnmanagedCodeSecurity]
        public static extern void BeginSurface([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluBeginTrim"), SuppressUnmanagedCodeSecurity]
        public static extern void BeginTrim([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] byte[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] byte[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] double[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] double[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] short[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] short[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] int[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] int[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] float[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] float[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] ushort[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] ushort[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] uint[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] uint[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmapLevels(uint target, int internalFormat, int width, int format, int type, int level, int min, int max, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] byte[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] byte[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] double[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] double[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] short[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] short[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] int[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] int[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] float[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] float[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] ushort[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] ushort[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] uint[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] uint[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild1DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build1DMipmaps(uint target, int internalFormat, int width, int format, int type, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] byte[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] byte[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] double[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] double[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] short[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] short[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] int[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] int[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] float[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] float[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] ushort[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] ushort[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] uint[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] uint[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "Build2DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmapLevels(uint target, int internalFormat, int width, int height, int format, int type, int level, int min, int max, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] byte[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] byte[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] double[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] double[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] short[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] short[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] int[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] int[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] float[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] float[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] ushort[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] ushort[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] uint[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] uint[ , , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild2DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build2DMipmaps(uint target, int internalFormat, int width, int height, int format, int type, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] byte[ , ] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] byte[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] double[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] double[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] short[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] short[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] int[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] int[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] float[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] float[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] ushort[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] ushort[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] uint[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] uint[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmapLevels"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmapLevels(uint target, int internalFormat, int width, int height, int depth, int format, int type, int level, int min, int max, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] byte[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] byte[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] double[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] double[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "glu"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "glu"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] short[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] short[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] int[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] int[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] float[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] float[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] ushort[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] ushort[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] uint[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] uint[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluBuild3DMipmaps"), SuppressUnmanagedCodeSecurity]
        public static extern int Build3DMipmaps(uint target, int internalFormat, int width, int height, int depth, int format, int type, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluCheckExtension"), SuppressUnmanagedCodeSecurity]
        public static extern int CheckExtension(string extensionName, string extensionString);

        [DllImport(GLUDLLName, EntryPoint = "gluCylinder"), SuppressUnmanagedCodeSecurity]
        public static extern void Cylinder([In] GLUquadric quad, double baseRadius, double topRadius, double height, int slices, int stacks);

        [DllImport(GLUDLLName, EntryPoint = "gluDeleteNurbsRenderer"), SuppressUnmanagedCodeSecurity]
        public static extern void DeleteNurbsRenderer([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluDeleteQuadric"), SuppressUnmanagedCodeSecurity]
        public static extern void DeleteQuadric([In] GLUquadric quad);

        [DllImport(GLUDLLName, EntryPoint = "gluDeleteTess"), SuppressUnmanagedCodeSecurity]
        public static extern void DeleteTess([In] GLUtesselator tess);

        [DllImport(GLUDLLName, EntryPoint = "gluDisk"), SuppressUnmanagedCodeSecurity]
        public static extern void Disk([In] GLUquadric quad, double innerRadius, double outerRadius, int slices, int loops);

        [DllImport(GLUDLLName, EntryPoint = "gluEndCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void EndCurve([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluEndPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void EndPolygon([In] GLUtesselator tess);

        [DllImport(GLUDLLName, EntryPoint = "gluEndSurface"), SuppressUnmanagedCodeSecurity]
        public static extern void EndSurface([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluEndTrim"), SuppressUnmanagedCodeSecurity]
        public static extern void EndTrim([In] GLUnurbs nurb);

        [DllImport(GLUDLLName, EntryPoint = "gluErrorString"), SuppressUnmanagedCodeSecurity]
        public static extern string ErrorString(int errorCode);

        public static string ErrorStringWIN(int errorCode)
        {
            return ErrorUnicodeStringEXT(errorCode);
        }

        [DllImport(GLUDLLName, EntryPoint = "gluErrorUnicodeStringEXT"), SuppressUnmanagedCodeSecurity]
        public static extern string ErrorUnicodeStringEXT(int errorCode);

        [DllImport(GLUDLLName, EntryPoint = "gluGetNurbsProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetNurbsProperty([In] GLUnurbs nurb, int property, [Out] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluGetNurbsProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetNurbsProperty([In] GLUnurbs nurb, int property, out float data);

        [DllImport(GLUDLLName, EntryPoint = "gluGetNurbsProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetNurbsProperty([In] GLUnurbs nurb, int property, [Out] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluGetString"), SuppressUnmanagedCodeSecurity]
        public static extern string GetString(int name);

        [DllImport(GLUDLLName, EntryPoint = "gluGetTessProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetTessProperty([In] GLUtesselator tess, int which, [Out] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluGetTessProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetTessProperty([In] GLUtesselator tess, int which, out double data);

        [DllImport(GLUDLLName, EntryPoint = "gluGetTessProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void GetTessProperty([In] GLUtesselator tess, int which, [Out] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluLoadSamplingMatrices"), SuppressUnmanagedCodeSecurity]
        public static extern void LoadSamplingMatrices([In] GLUnurbs nurb, [In] float[] modelMatrix, [In] float[] projectionMatrix, [In] int[] viewport);

        [DllImport(GLUDLLName, EntryPoint = "gluLookAt"), SuppressUnmanagedCodeSecurity]
        public static extern void LookAt(double eyeX, double eyeY, double eyeZ, double centerX, double centerY, double centerZ, double upX, double upY, double upZ);

        [DllImport(GLUDLLName, EntryPoint = "gluNewNurbsRenderer"), SuppressUnmanagedCodeSecurity]
        public static extern GLUnurbs NewNurbsRenderer();

        [DllImport(GLUDLLName, EntryPoint = "gluNewQuadric"), SuppressUnmanagedCodeSecurity]
        public static extern GLUquadric NewQuadric();

        [DllImport(GLUDLLName, EntryPoint = "gluNewTess"), SuppressUnmanagedCodeSecurity]
        public static extern GLUtesselator NewTess();

        [DllImport(GLUDLLName, EntryPoint = "gluNextContour"), SuppressUnmanagedCodeSecurity]
        public static extern void NextContour([In] GLUtesselator tess, int type);

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsBeginCallback func)
        {
            nurbsBeginCallback = func;
            __gluNurbsCallback(nurb, which, nurbsBeginCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsBeginDataCallback func)
        {
            nurbsBeginDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsBeginDataCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsColorCallback func)
        {
            nurbsColorCallback = func;
            __gluNurbsCallback(nurb, which, nurbsColorCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsColorDataCallback func)
        {
            nurbsColorDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsColorDataCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsEndCallback func)
        {
            nurbsEndCallback = func;
            __gluNurbsCallback(nurb, which, nurbsEndCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsEndDataCallback func)
        {
            nurbsEndDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsEndDataCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsErrorCallback func)
        {
            nurbsErrorCallback = func;
            __gluNurbsCallback(nurb, which, nurbsErrorCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsNormalCallback func)
        {
            nurbsNormalCallback = func;
            __gluNurbsCallback(nurb, which, nurbsNormalCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsNormalDataCallback func)
        {
            nurbsNormalDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsNormalDataCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsTexCoordCallback func)
        {
            nurbsTexCoordCallback = func;
            __gluNurbsCallback(nurb, which, nurbsTexCoordCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsTexCoordDataCallback func)
        {
            nurbsTexCoordDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsTexCoordDataCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsVertexCallback func)
        {
            nurbsVertexCallback = func;
            __gluNurbsCallback(nurb, which, nurbsVertexCallback);
        }

        public static void NurbsCallback([In] GLUnurbs nurb, int which, [In] gluNurbsVertexDataCallback func)
        {
            nurbsVertexDataCallback = func;
            __gluNurbsCallback(nurb, which, nurbsVertexDataCallback);
        }

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] byte[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] byte[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] byte[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] double[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] double[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] double[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] short[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] short[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "glu"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] short[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] int[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] int[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] int[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] float[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] float[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] float[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] ushort[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] ushort[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] ushort[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] uint[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] uint[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] uint[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackData"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackData([In] GLUnurbs nurb, [In] IntPtr userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] byte[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] byte[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] byte[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] double[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] double[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] double[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] short[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] short[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] short[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] int[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] int[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] int[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] float[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] float[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] float[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] ushort[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] ushort[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] ushort[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] uint[] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] uint[,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] uint[, ,] userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCallbackDataEXT"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCallbackDataEXT([In] GLUnurbs nurb, [In] IntPtr userData);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCurve([In] GLUnurbs nurb, int knotCount, [In] float[] knots, int stride, [In] float[] control, int order, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsCurve([In] GLUnurbs nurb, int knotCount, [In] float[] knots, int stride, [In] float[,] control, int order, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsProperty([In] GLUnurbs nurb, int property, float val);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsSurface"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsSurface([In] GLUnurbs nurb, int sKnotCount, [In] float[] sKnots, int tKnotCount, [In] float[] tKnots, int sStride, int tStride, float[] control, int sOrder, int tOrder, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsSurface"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsSurface([In] GLUnurbs nurb, int sKnotCount, [In] float[] sKnots, int tKnotCount, [In] float[] tKnots, int sStride, int tStride, float[,] control, int sOrder, int tOrder, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluNurbsSurface"), SuppressUnmanagedCodeSecurity]
        public static extern void NurbsSurface([In] GLUnurbs nurb, int sKnotCount, [In] float[] sKnots, int tKnotCount, [In] float[] tKnots, int sStride, int tStride, float[, ,] control, int sOrder, int tOrder, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluOrtho2D"), SuppressUnmanagedCodeSecurity]
        public static extern void Ortho2D(double left, double right, double bottom, double top);

        [DllImport(GLUDLLName, EntryPoint = "gluPartialDisk"), SuppressUnmanagedCodeSecurity]
        public static extern void PartialDisk([In] GLUquadric quad, double innerRadius, double outerRadius, int slices, int loops, double startAngle, double sweepAngle);

        [DllImport(GLUDLLName, EntryPoint = "gluPerspective"), SuppressUnmanagedCodeSecurity]
        public static extern void Perspective(double fovY, double aspectRatio, double zNear, double zFar);

        [DllImport(GLUDLLName, EntryPoint = "gluPickMatrix"), SuppressUnmanagedCodeSecurity]
        public static extern void PickMatrix(double x, double y, double width, double height, [In] int[] viewport);

        [DllImport(GLUDLLName, EntryPoint = "gluProject"), SuppressUnmanagedCodeSecurity]
        public static extern int Project(double objX, double objY, double objZ, [In] double[] modelMatrix, [In] double[] projectionMatrix, [In] int[] viewport, out double winX, out double winY, out double winZ);

        [DllImport(GLUDLLName, EntryPoint = "gluPwlCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void PwlCurve([In] GLUnurbs nurb, int count, [In] float[] data, int stride, int type);

        [DllImport(GLUDLLName, EntryPoint = "gluPwlCurve"), SuppressUnmanagedCodeSecurity]
        public static extern void PwlCurve([In] GLUnurbs nurb, int count, [In] float[,] data, int stride, int type);

        public static void QuadricCallback([In] GLUquadric quad, int which, [In] gluQuadricErrorCallback func)
        {
            quadricErrorCallback = func;
            __gluQuadricCallback(quad, which, quadricErrorCallback);

        }

        [DllImport(GLUDLLName, EntryPoint = "gluQuadricDrawStyle"), SuppressUnmanagedCodeSecurity]
        public static extern void QuadricDrawStyle([In] GLUquadric quad, int drawStyle);

        [DllImport(GLUDLLName, EntryPoint = "gluQuadricNormals"), SuppressUnmanagedCodeSecurity]
        public static extern void QuadricNormals([In] GLUquadric quad, int normal);

        [DllImport(GLUDLLName, EntryPoint = "gluQuadricOrientation"), SuppressUnmanagedCodeSecurity]
        public static extern void QuadricOrientation([In] GLUquadric quad, int orientation);

        [DllImport(GLUDLLName, EntryPoint = "gluQuadricTexture"), SuppressUnmanagedCodeSecurity]
        public static extern void QuadricTexture([In] GLUquadric quad, int texture);
        
        [DllImport(GLUDLLName, EntryPoint = "gluScaleImage"), SuppressUnmanagedCodeSecurity]
        public static extern int ScaleImage(int format, int widthIn, int heightIn, int typeIn, [In] IntPtr dataIn, int widthOut, int heightOut, int typeOut, [Out] IntPtr dataOut);
        
        [DllImport(GLUDLLName, EntryPoint = "gluScaleImage"), SuppressUnmanagedCodeSecurity]
        public static extern int ScaleImage(int format, int widthIn, int heightIn, int typeIn, [In] byte[] dataIn, int widthOut, int heightOut, int typeOut, [Out] byte[] dataOut);
        
        [DllImport(GLUDLLName, EntryPoint = "gluSphere"), SuppressUnmanagedCodeSecurity]
        public static extern void Sphere([In] GLUquadric quad, double radius, int slices, int stacks);
        
        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginContour"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginContour([In] GLUtesselator tess);
        
        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] byte[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] byte[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] double[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] double[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] short[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] short[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] int[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] int[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] float[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] float[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] ushort[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] ushort[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] uint[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] uint[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessBeginPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessBeginPolygon([In] GLUtesselator tess, [In] IntPtr data);

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessBeginCallback func)
        {
            tessBeginCallback = func;
            __gluTessCallback(tess, which, tessBeginCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessBeginDataCallback func)
        {
            tessBeginDataCallback = func;
            __gluTessCallback(tess, which, tessBeginDataCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineCallback func)
        {
            tessCombineCallback = func;
            __gluTessCallback(tess, which, tessCombineCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineCallback1 func)
        {
            tessCombineCallback1 = func;
            __gluTessCallback(tess, which, tessCombineCallback1);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessCombineDataCallback func)
        {
            tessCombineDataCallback = func;
            __gluTessCallback(tess, which, tessCombineDataCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessEdgeFlagCallback func)
        {
            tessEdgeFlagCallback = func;
            __gluTessCallback(tess, which, tessEdgeFlagCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessEdgeFlagDataCallback func)
        {
            tessEdgeFlagDataCallback = func;
            __gluTessCallback(tess, which, tessEdgeFlagDataCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessEndCallback func)
        {
            tessEndCallback = func;
            __gluTessCallback(tess, which, tessEndCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessEndDataCallback func)
        {
            tessEndDataCallback = func;
            __gluTessCallback(tess, which, tessEndDataCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessErrorCallback func)
        {
            tessErrorCallback = func;
            __gluTessCallback(tess, which, tessErrorCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessErrorDataCallback func)
        {
            tessErrorDataCallback = func;
            __gluTessCallback(tess, which, tessErrorDataCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexCallback func)
        {
            tessVertexCallback = func;
            __gluTessCallback(tess, which, tessVertexCallback);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexCallback1 func)
        {
            tessVertexCallback1 = func;
            __gluTessCallback(tess, which, tessVertexCallback1);
        }

        public static void TessCallback([In] GLUtesselator tess, int which, [In] gluTessVertexDataCallback func)
        {
            tessVertexDataCallback = func;
            __gluTessCallback(tess, which, tessVertexDataCallback);
        }

        [DllImport(GLUDLLName, EntryPoint = "gluTessEndContour"), SuppressUnmanagedCodeSecurity]
        public static extern void TessEndContour([In] GLUtesselator tess);

        [DllImport(GLUDLLName, EntryPoint = "gluTessEndPolygon"), SuppressUnmanagedCodeSecurity]
        public static extern void TessEndPolygon([In] GLUtesselator tess);

        [DllImport(GLUDLLName, EntryPoint = "gluTessNormal"), SuppressUnmanagedCodeSecurity]
        public static extern void TessNormal([In] GLUtesselator tess, double x, double y, double z);

        [DllImport(GLUDLLName, EntryPoint = "gluTessProperty"), SuppressUnmanagedCodeSecurity]
        public static extern void TessProperty([In] GLUtesselator tess, int which, double data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] byte[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] byte[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] byte[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] double[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] double[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] double[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] short[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] short[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] short[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] int[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] int[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] int[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] float[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] float[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] float[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] ushort[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] ushort[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] ushort[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] uint[] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] uint[,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] uint[, ,] data);

        [DllImport(GLUDLLName, EntryPoint = "gluTessVertex"), SuppressUnmanagedCodeSecurity]
        public static extern void TessVertex([In] GLUtesselator tess, [In] double[] location, [In] IntPtr data);

        [DllImport(GLUDLLName, EntryPoint = "gluUnProject"), SuppressUnmanagedCodeSecurity]
        public static extern int UnProject(double winX, double winY, double winZ, [In] double[] modelMatrix, [In] double[] projectionMatrix, [In] int[] viewport, out double objX, out double objY, out double objZ);

        [DllImport(GLUDLLName, EntryPoint = "gluUnProject4"), SuppressUnmanagedCodeSecurity]
        public static extern int UnProject4(double winX, double winY, double winZ, double clipW, [In] double[] modelMatrix, [In] double[] projectionMatrix, [In] int[] viewport, double nearVal, double farVal, out double objX, out double objY, out double objZ, out double objW);
    }
}
