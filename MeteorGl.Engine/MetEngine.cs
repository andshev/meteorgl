﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ManagedOpenGl;
using MeteorGl.Engine.Timers;
using MeteorGl.Engine.Utils;

namespace MeteorGl.Engine
{
    /// <summary>
    /// <para>Main class, engine control center</para>
    /// </summary>
	public static class MetEngine
	{
	    public static bool IsRunning { get; private set; }
	    public static MetSuperTimer Timer { get; private set; }
	    public static MetOpenGlControl ViewControl { get; private set; }
	    public static MetProgram Program { get; private set; }
	    public static OpenGlContext OpenGlContext { get; private set; }
        public static bool ControlMode { get; set; }
	    public static bool FullScreen { get; set; }
	    public static int Width { get; set; } = 800;
	    public static int Height { get; set; } = 600;
	    public static ushort MaxFps { get; set; } = 50;
        public static byte Depth { get; set; } = 32;
	    public static bool LimitFps { get; set; } = true;

        private static MetWin _window;
        public static MetWin Window
        {
            get
            {
                if (!ControlMode)
                    return _window;
                else
                {
                    throw new Exception("Cannot access window object in control mode");
                }
            }
        }

        private static MetScreen.DeviceSettings _devPrev;
        private static ulong _animNext;
        
		static void StartWindowGl()
		{
            ControlMode = false;

            MetScreen.DeviceSettings devMode = MetScreen.GetCurrentSettings();
            _devPrev = MetScreen.GetCurrentSettings();

			ViewControl = new MetOpenGlControl();
			ViewControl.Dock = DockStyle.Fill;

            _window = new MetWin();

            if (FullScreen)
            {
                Window.StartPosition = FormStartPosition.Manual;
                Window.Left = 0;
                Window.Top = 0;
                Window.FormBorderStyle = FormBorderStyle.None;

                devMode.dmBitsPerPel = Depth;
                devMode.dmPelsWidth = Width;
                devMode.dmPelsHeight = Height;

                if (!MetScreen.ChangeSettings(devMode))
                    throw new Exception("Cannot change resolution");
            }
            else
            {
                Window.StartPosition = FormStartPosition.CenterScreen;
                Window.FormBorderStyle = FormBorderStyle.FixedSingle;
            }

            Window.ClientSize = new Size(Width, Height);
            Window.Controls.Add(ViewControl);

			OpenGlContext = new OpenGlContext(ViewControl, Depth, Depth, 0);

			Window.Show();
		}

        static void StartControlGl(MetOpenGlControl control)
        {
            ControlMode = true;
            ViewControl = control;
            Width = control.Width;
            Height = control.Height;
            OpenGlContext = new OpenGlContext(control, Depth, Depth, 0);
        }

		static void StopGl()
		{
			OpenGlContext.Dispose();
		}

		public static void StartWindow(MetProgram program)
		{
			Program = program;
			StartWindowGl();
			Program.Init();
			IsRunning = true;
			MainLoop();
            Program.Exit();
			Application.Exit();
		}

        public static void StartControl(MetProgram program, MetOpenGlControl control)
        {
            Program = program;
            StartControlGl(control);
            Program.Init();
            IsRunning = true;
            MainLoop();
            Program.Exit();
            Application.Exit();
        }

		public static void Stop()
		{
            if (FullScreen)
                MetScreen.ChangeSettings(_devPrev);

			IsRunning = false;
		}


	    private static void PreInputPreDraw()
        {
            if (MetInput.MouseState.LastX == MetInput.MouseState.X)
                MetInput.MouseState.DiffX = 0;

            if (MetInput.MouseState.LastY == MetInput.MouseState.Y)
                MetInput.MouseState.DiffY = 0;
        }

        static void MainLoop()
		{
            Timer = new MetSuperTimer();
            float n = Timer.GetFrequency() / MaxFps;

			if (LimitFps)
			{
                _animNext = Timer.GetCount() + (uint)n;

				while (IsRunning)
				{
                    if (Timer.GetCount() > _animNext)
					{
                        Program.Input();
                        ViewControl.ReDraw();
                        Application.DoEvents();
                        PreInputPreDraw();
                        
                        _animNext = Timer.GetCount() + (uint)n;
					}
				}
			}
			else
			{
				while (IsRunning)
				{
                    Program.Input();
				    ViewControl.ReDraw();
				    Application.DoEvents();
                    PreInputPreDraw();
				}
			}
		}
	}
}
