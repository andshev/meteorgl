﻿#region Using directives



#endregion

using System.Windows.Forms;

namespace MeteorGl.Engine
{
    /// <summary>
    /// <para>Working with keyboard and mouse</para>
    /// </summary>
    public static class MetInput
    {
        public static bool[] KeyState = new bool[256];
        public struct MouseState
        {
            public static int ScreenX;
            public static int ScreenY;
            public static int LastScreenX;
            public static int LastScreenY;
            public static int X;
            public static int Y;
            public static int LastX;
            public static int LastY;
            public static int DiffX;
            public static int DiffY;
            public static bool LeftButton;
            public static bool MiddleButton;
            public static bool RightButton;
            public static bool XButton1;
            public static bool XButton2;
        }

        public static bool KeyHold(Keys key)
        {
            if (KeyState[(int)key])
                return true;
            else
                return false;
        }

        public static bool KeyPressed(Keys key)
        {
            if (KeyState[(int)key])
            {
                KeyState[(int)key] = false;
                return true;
            }
            else
                return false;
        }
        
        public static bool MousePressedLeftButton()
        {
        	if (MouseState.LeftButton)
        	{
        		MouseState.LeftButton = false;
        		return true;
        	}
        	else
        		return false;
        }
        
        public static bool MousePressedMiddleButton()
        {
        	if (MouseState.MiddleButton)
        	{
        		MouseState.MiddleButton = false;
        		return true;
        	}
        	else
        		return false;
        }
        
        public static bool MousePressedRightButton()
        {
        	if (MouseState.RightButton)
        	{
        		MouseState.RightButton = false;
        		return true;
        	}
        	else
        		return false;
        }
        
        public static bool MousePressedXButton1()
        {
        	if (MouseState.XButton1)
        	{
        		MouseState.XButton1 = false;
        		return true;
        	}
        	else
        		return false;
        }
        
        public static bool MousePressedXButton2()
        {
        	if (MouseState.XButton2)
        	{
        		MouseState.XButton2 = false;
        		return true;
        	}
        	else
        		return false;
        }
    }
}
