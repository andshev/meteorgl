﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ManagedOpenGl;

namespace MeteorGl.Engine
{
    /// <summary>
    /// <para>User control for creating OpenGL context</para>
    /// </summary>
    public class MetOpenGlControl : Control
    {
        public MetOpenGlControl()
        {
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            if (MetEngine.Program == null)
            {
                e.Graphics.FillRectangle(Brushes.Gray, e.ClipRectangle);
                return;
            }

            if (MetEngine.OpenGlContext.DC == 0 || MetEngine.OpenGlContext.RC == 0)
                return;

            MetInput.MouseState.LastScreenX = MetInput.MouseState.X;
            MetInput.MouseState.LastScreenY = MetInput.MouseState.Y;
            MetInput.MouseState.ScreenX = Control.MousePosition.X;
            MetInput.MouseState.ScreenY = Control.MousePosition.Y;
            MetInput.MouseState.LastX = MetInput.MouseState.X;
            MetInput.MouseState.X = Control.MousePosition.X - Parent.Left - SystemInformation.BorderSize.Width - 1;
            MetInput.MouseState.LastY = MetInput.MouseState.Y;
            MetInput.MouseState.Y = Control.MousePosition.Y - Parent.Top - SystemInformation.BorderSize.Height
                - SystemInformation.CaptionHeight - 1;
            MetInput.MouseState.DiffX = MetInput.MouseState.X - MetInput.MouseState.LastX;
            MetInput.MouseState.DiffY = MetInput.MouseState.Y - MetInput.MouseState.LastY;

            MetEngine.Program.Draw();
            gl.Flush();
            ManagedOpenGl.Windows.SwapBuffers(MetEngine.OpenGlContext.DC);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Right:
                case Keys.Left:
                case Keys.Tab:
                    return true;
                default:
                    return base.IsInputKey(keyData);
            }
        }


        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                MetInput.MouseState.LeftButton = true;
            else if (e.Button == MouseButtons.Middle)
                MetInput.MouseState.MiddleButton = true;
            else if (e.Button == MouseButtons.Right)
                MetInput.MouseState.RightButton = true;
            else if (e.Button == MouseButtons.XButton1)
                MetInput.MouseState.XButton1 = true;
            else if (e.Button == MouseButtons.XButton2)
                MetInput.MouseState.XButton2 = true;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            MetInput.MouseState.DiffX = 0;
            MetInput.MouseState.DiffY = 0;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //MetInput.MouseState.LastX = MetInput.MouseState.X;
            //MetInput.MouseState.LastY = MetInput.MouseState.Y;
            //MetInput.MouseState.X = e.X;
            //MetInput.MouseState.Y = e.Y;
            //MetInput.MouseState.DiffX = MetInput.MouseState.X - MetInput.MouseState.LastX;
            //MetInput.MouseState.DiffY = MetInput.MouseState.Y - MetInput.MouseState.LastY;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                MetInput.MouseState.LeftButton = false;
            else if (e.Button == MouseButtons.Middle)
                MetInput.MouseState.MiddleButton = false;
            else if (e.Button == MouseButtons.Right)
                MetInput.MouseState.RightButton = false;
            else if (e.Button == MouseButtons.XButton1)
                MetInput.MouseState.XButton1 = false;
            else if (e.Button == MouseButtons.XButton2)
                MetInput.MouseState.XButton2 = false;
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            Size s = Size;

            if (MetEngine.ControlMode)
            {
                if (s.Width != 0 && s.Height != 0)
                {
                    MetEngine.Width = s.Width;
                    MetEngine.Height = s.Height;
                }
            }

            if (MetEngine.Program == null)
                return;

            if (s.Width != 0 && s.Height != 0)
                MetEngine.Program.Resize(s.Width, s.Height);
        }

        public void ReDraw()
        {
            OnPaint(null);
        }

        protected override void Dispose(bool disposing)
        {
            MetEngine.Stop();
            base.Dispose(disposing);
        }
    }
}
