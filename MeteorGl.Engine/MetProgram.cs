namespace MeteorGl.Engine
{
    /// <summary>
    /// <para>Core of your program. Inherit this class and overwrite Init, Draw, Input, Resize и Exit methods.</para>
    /// </summary>
    public abstract class MetProgram
    {
        public MetProgram()
        {
        }

        public virtual void Init()
        {
        }

        public virtual void Draw()
        {
        }

        public virtual void Input()
        {
        }

        public virtual void Resize(int width, int height)
        {
        }

        public virtual void Exit()
        {
        }
    }
}
