using System.Windows.Forms;

namespace MeteorGl.Engine
{
    /// <summary>
    /// <para>Окно программы</para>
    /// <para>Program window</para>
    /// </summary>
    public class MetWin : Form
    {
        public MetWin()
        {
            this.ClientSize = new System.Drawing.Size(MetEngine.Width, MetEngine.Height);
            this.Name = "Window";
            this.Text = "";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.FormClosed += new FormClosedEventHandler(MetOkno_FormClosed);
        }

        void MetOkno_FormClosed(object sender, FormClosedEventArgs e)
        {
            MetEngine.Stop();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            MetInput.KeyState[(int)e.KeyCode] = true;
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            MetInput.KeyState[(int)e.KeyCode] = false;
        }
    }
}