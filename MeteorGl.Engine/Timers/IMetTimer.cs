﻿namespace MeteorGl.Engine.Timers
{
    public interface IMetTimer<out T>
    {
        T GetCount();
        T GetFrequency();
    }
}
