﻿using System.Runtime.InteropServices;

namespace MeteorGl.Engine.Timers
{
    /// <summary>
    /// High quality timer
    /// </summary>
    public class MetSuperTimer : IMetTimer<ulong>
    {
        ulong f;
        ulong c;

        static ulong sc;
        static ulong sf;

        public MetSuperTimer()
        {
            GetFrequency();
        }

        public ulong GetFrequency()
        {
            QueryPerformanceFrequency(ref f);
            return f;
        }

        public ulong GetCount()
        {
            QueryPerformanceCounter(ref c);
            return c;
        }

        public static ulong Freq()
        {
            QueryPerformanceFrequency(ref sf);
            return sf;
        }
        
        public static ulong Count()
        {
            QueryPerformanceCounter(ref sc);
            return sc;
        }

        [DllImport("kernel32.dll")]
        private static extern bool QueryPerformanceFrequency(ref ulong frequencyCount);

        [DllImport("kernel32.dll")]
        private static extern bool QueryPerformanceCounter(ref ulong performanceCount);
    }
}
