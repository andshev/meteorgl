﻿using System;

namespace MeteorGl.Engine.Timers
{
    /// <summary>
    /// Environment.TickCount Timer
    /// </summary>
    public class MetTimer : IMetTimer<int>
    {
        public MetTimer()
        {
        }

        public int GetCount()
        {
            return Environment.TickCount;
        }

        public int GetFrequency()
        {
            return 1000;
        }

        public static int Count()
        {
            return Environment.TickCount;
        }

        public static int Freq()
        {
            return 1000;
        }
    }
}
