﻿using MeteorGl.Engine.Utils;

namespace MeteorGl.Engine.UI
{
    interface IMetUiIndicator
    {
        string[] Lines { get; }
        MetFont Font { get; set; }
    }
}
