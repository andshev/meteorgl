﻿using System.Drawing;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// Base class of Meteor user control
    /// </summary>
    public abstract class MetUiControl
    {
        public RectangleF Bounds;

        public Color ForeColor = Color.FromArgb(200, 20, 125, 20);
        public Color LightColor = Color.FromArgb(200, 20, 250, 20);
        public Color BackColor = Color.FromArgb(150, 0, 20, 0);
        public Color BorderColor = Color.FromArgb(150, 10, 70, 10);

        public bool Visible = true;

        public MetUiPanel MyPanel;

        public object Data;

        public string Text;
        public float FontSize = 10f;

        public bool MouseHover { get; private set; }

        public MetUiControl(float x, float y, float width, float height, string text)
        {
            Bounds = new RectangleF(x, y, width, height);
            Text = text;
        }

        public virtual void Draw()
        {
            MouseHover = Bounds.Contains(MetInput.MouseState.X, MetInput.MouseState.Y);
        }

        public void SetTransparentBackground()
        {
            BackColor = Color.FromArgb(0, BackColor);
        }

        public void Move(float x, float y)
        {
            Bounds.X += x;
            Bounds.Y += y;
        }
    }
}
