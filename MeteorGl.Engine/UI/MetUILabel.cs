﻿using ManagedOpenGl;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// Надпись
    /// </summary>
    public class MetUiLabel : MetUiControl
    {
        public bool DrawBorder = false;

        public MetUiLabel(float x, float y, float width, float height, string text)
            : base(x, y, width, height, text)
        {
        }

        public override void Draw()
        {
            if (MyPanel == null)
                return;

            base.Draw();

            gl.PushAttrib(gl.COLOR);

            gl.Color4ub(BackColor.R, BackColor.G, BackColor.B, BackColor.A);
            gl.Begin(gl.QUADS);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            if (DrawBorder)
            {
                gl.Color4ub(BorderColor.R, BorderColor.G, BorderColor.B, BorderColor.A);
                gl.Begin(gl.LINE_LOOP);
                gl.Vertex2f(Bounds.X, Bounds.Y);
                gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
                gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
                gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
                gl.End();
            }

            gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, ForeColor.A);
            gl.PushMatrix();
            gl.Translatef(Bounds.X + 5f, Bounds.Y + 5f, 0f);
            MyPanel.Font.Print(Text, FontSize);
            gl.PopMatrix();

            gl.PopAttrib();
        }
    }
}
