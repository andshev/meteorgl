﻿using System.Collections.Generic;
using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine.Utils;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// UI Panel
    /// </summary>
    public class MetUiPanel
    {
        public RectangleF Bounds;

        public Color ForeColor = MetUiDefaults.ForeColor;
        public Color LightColor = MetUiDefaults.LightColor;
        public Color BackColor = MetUiDefaults.BackColor;
        public Color BorderColor = MetUiDefaults.BorderColor;
        public float AlphaRegular = MetUiDefaults.AlphaRegular;
        public float AlphaParphelia = MetUiDefaults.AlphaParphelia;
        public float AlphaParpheliaStep = MetUiDefaults.AlphaParpheliaStep;
        public float AlphaCurrent = MetUiDefaults.AlphaCurrent;
        public bool DoParphelia = false;

        public MetFont Font;
        public float FontSize = 10f;
        public PointF TextOffset = new PointF(5f, 5f);

        public double NearAndFar = MetUiDefaults.NearAndFar;
        public bool CanMove = true;

        public List<MetUiControl> Controls = new List<MetUiControl>();
        public bool ControlsPositionAbsolute = false;

        public bool MouseHover { get; private set; }

        public MetUiPanel(float x, float y, float width, float height)
        {
            Bounds = new RectangleF(x, y, width, height);
            Font = new MetFont();
            Font.LoadDefault();
        }

        public MetUiPanel(float x, float y, float width, float height, MetFont texturefont)
        {
            Bounds = new RectangleF(x, y, width, height);
            Font = texturefont;
        }

        public void AddControl(MetUiControl control, bool inheritcolors)
        {
            if (inheritcolors)
            {
                control.BackColor = Color.FromArgb(0, BackColor);
                control.ForeColor = ForeColor;
                control.BorderColor = BorderColor;
                control.LightColor = LightColor;
                control.FontSize = FontSize;
            }

            control.MyPanel = this;

            if (!ControlsPositionAbsolute)
            {
                control.Bounds.Offset(this.Bounds.Location);
            }

            Controls.Add(control);
        }

        public void AddControl(MetUiControl control)
        {
            AddControl(control, true);
        }

        public void Draw()
        {
            MouseHover = Bounds.Contains(MetInput.MouseState.X, MetInput.MouseState.Y);

            if (CanMove)
            {
                if (MouseHover && MetInput.MouseState.LeftButton)
                {
                    Move(MetInput.MouseState.DiffX, MetInput.MouseState.DiffY);

                    foreach (MetUiControl ctl in Controls)
                        ctl.Move(MetInput.MouseState.DiffX, MetInput.MouseState.DiffY);
                }
            }

            if (DoParphelia)
            {
                if (AlphaCurrent >= AlphaParphelia)
                    AlphaCurrent -= AlphaParpheliaStep;
            }
            else
            {
                if (AlphaCurrent <= AlphaRegular)
                    AlphaCurrent += AlphaParpheliaStep;
            }

            gl.PushMatrix();
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            MetProjection.Ortho(MetEngine.Width, MetEngine.Height,
                0, MetEngine.Width, MetEngine.Height, 0,
                -NearAndFar, NearAndFar);
            gl.LoadIdentity();

            gl.PushAttrib(gl.COLOR);

            gl.Begin(gl.QUADS);
            gl.Color4ub(BackColor.R, BackColor.G, BackColor.B, (byte)(BackColor.A * AlphaCurrent));
            gl.Vertex2f(Bounds.Left, Bounds.Top);
            gl.Vertex2f(Bounds.Right, Bounds.Top);
            gl.Vertex2f(Bounds.Right, Bounds.Bottom);
            gl.Vertex2f(Bounds.Left, Bounds.Bottom);
            gl.End();

            gl.Begin(gl.LINE_LOOP);
            gl.Color4ub(BorderColor.R, BorderColor.G, BorderColor.B, (byte)(BorderColor.A * AlphaCurrent));
            gl.Vertex2f(Bounds.Left, Bounds.Top);
            gl.Vertex2f(Bounds.Right, Bounds.Top);
            gl.Vertex2f(Bounds.Right, Bounds.Bottom);
            gl.Vertex2f(Bounds.Left, Bounds.Bottom);
            gl.End();

            for (int i = 0; i < Controls.Count; i++)
            {
                Controls[i].Draw();
            }

            gl.PopMatrix();
        }

        public void Move(float x, float y)
        {
            Bounds.X += x;
            Bounds.Y += y;
        }
    }
}
