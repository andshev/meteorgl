﻿using System.Windows.Forms;
using ManagedOpenGl;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// Textbox
    /// </summary>
    public class MetUiTextBox : MetUiControl
    {
        public string CursorLabel = "_";
        public int CursorPosition = 0;

        public bool LockFocus = false;

        public MetUiTextBox(float x, float y, float width, float height, string text)
            : base(x, y, width, height, text)
        {
            MetEngine.ViewControl.KeyDown += new KeyEventHandler(OnKeyDown);
            MetEngine.ViewControl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(OnKeyPress);
        }

        // TODO: Remove winforms dependency
        void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (!MouseHover && !LockFocus)
                return;

            if (e.KeyCode == Keys.Back)
            {
                if (Text.Length > 0)
                {
                    Text = Text.Remove(CursorPosition - 1, 1);
                    CursorPosition--;
                }
            }

            if (e.KeyCode == Keys.Delete)
            {
                if (Text.Length > 0 && CursorPosition < Text.Length)
                {
                    Text = Text.Remove(CursorPosition, 1);
                }
            }

            if (e.KeyCode == Keys.Left)
            {
                CursorPosition--;
            }

            if (e.KeyCode == Keys.Right)
            {
                CursorPosition++;
            }

            if (e.KeyCode == Keys.Enter)
                LockFocus = false;

            if (CursorPosition < 0)
                CursorPosition = 0;

            if (CursorPosition > Text.Length)
                CursorPosition = Text.Length;
        }

        void OnKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!MouseHover && !LockFocus)
                return;

            if (char.IsLetterOrDigit(e.KeyChar))
            {
                Text = Text.Insert(CursorPosition, e.KeyChar.ToString());
                CursorPosition++;
            }

            if (char.IsPunctuation(e.KeyChar))
            {
                Text = Text.Insert(CursorPosition, e.KeyChar.ToString());
                CursorPosition++;
            }

            if (char.IsSymbol(e.KeyChar))
            {
                Text = Text.Insert(CursorPosition, e.KeyChar.ToString());
                CursorPosition++;
            }

            if (char.IsWhiteSpace(e.KeyChar))
            {
                Text = Text.Insert(CursorPosition, e.KeyChar.ToString());
                CursorPosition++;
            }
        }

        public override void Draw()
        {
            if (MyPanel == null)
                return;

            base.Draw();

            if (MouseHover && MetInput.MousePressedLeftButton())
                LockFocus = !LockFocus;

            gl.PushAttrib(gl.COLOR);

            gl.Color4ub(BackColor.R, BackColor.G, BackColor.B, BackColor.A);
            gl.Begin(gl.QUADS);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            if (MouseHover || LockFocus)
                gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, ForeColor.A);
            else
                gl.Color4ub(BorderColor.R, BorderColor.G, BorderColor.B, BorderColor.A);

            gl.Begin(gl.LINE_LOOP);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, ForeColor.A);
            gl.PushMatrix();
            gl.Translatef(Bounds.X + 5f, Bounds.Y + 5f, 0f);
            MyPanel.Font.Print(Text, FontSize);
            gl.PopMatrix();

            gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, ForeColor.A);
            gl.PushMatrix();
            gl.Translatef(Bounds.X + 5f, Bounds.Y + 5f, 0f);
            gl.Translatef(MyPanel.Font.ScaleX * CursorPosition * FontSize, 0f, 0f);
            MyPanel.Font.Print(CursorLabel, FontSize);
            gl.PopMatrix();

            gl.PopAttrib();
        }
    }
}
