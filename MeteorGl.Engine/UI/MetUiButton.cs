﻿using System;
using ManagedOpenGl;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// Button
    /// </summary>
    public class MetUiButton : MetUiControl
    {
        public event EventHandler OnClick;

        public MetUiButton(float x, float y, float width, float height, string text)
            : base(x, y, width, height, text)
        {
        }

        public override void Draw()
        {
            if (MyPanel == null)
                return;

            base.Draw();

            gl.PushAttrib(gl.COLOR);

            // Background
            gl.Color4ub(BackColor.R, BackColor.G, BackColor.B, (byte)(BackColor.A * MyPanel.AlphaCurrent));
            gl.Begin(gl.QUADS);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            // Border
            if (MouseHover)
                gl.Color4ub(LightColor.R, LightColor.G, LightColor.B, LightColor.A);
            else
                gl.Color4ub(BorderColor.R, BorderColor.G, BorderColor.B, (byte)(BorderColor.A * MyPanel.AlphaCurrent));
            gl.Begin(gl.LINE_LOOP);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            // Label
            if (MouseHover && MetInput.MouseState.LeftButton)
                gl.Color4ub(LightColor.R, LightColor.G, LightColor.B, LightColor.A);
            else
                gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, (byte)(ForeColor.A * MyPanel.AlphaCurrent));
            gl.PushMatrix();
            gl.Translatef(Bounds.X + MyPanel.TextOffset.X, Bounds.Y + MyPanel.TextOffset.Y, 0f);
            MyPanel.Font.Print(Text, FontSize);
            gl.PopMatrix();

            gl.PopAttrib();

            if (MouseHover && MetInput.MousePressedLeftButton())
            {
                OnClick?.Invoke(this, new EventArgs());
            }
        }
    }
}
