﻿using System.Drawing;

namespace MeteorGl.Engine.UI
{
    public class MetUiDefaults
    {
        public static Color ForeColor = Color.FromArgb(200, 20, 125, 20);
        public static Color LightColor = Color.FromArgb(200, 20, 250, 20);
        public static Color BackColor = Color.FromArgb(150, 0, 20, 0);
        public static Color BorderColor = Color.FromArgb(150, 10, 70, 10);
        public static float AlphaRegular = 1.0f;
        public static float AlphaParphelia = 0.5f;
        public static float AlphaParpheliaStep = 0.2f;
        public static float AlphaCurrent = 1.0f;
        public static double NearAndFar = 1.0;
    }
}
