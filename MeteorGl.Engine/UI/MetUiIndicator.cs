﻿using System.Drawing;
using ManagedOpenGl;
using MeteorGl.Engine.Utils;

namespace MeteorGl.Engine.UI
{
    /// <summary>
    /// An indicator control working without panel in any part of the screen
    /// </summary>
    public class MetUiIndicator : MetUiControl, IMetUiIndicator
    {
        public bool DrawBorder = false;
        public bool NewMatrix = true;
        public bool CanMove = true;
        //public bool AutoSize = true;

        public string[] Lines
        {
            get
            {
                string[] lines = Text.Replace("\r", "").Split('\n');

                //if (AutoSize)
                //{
                //    int maxWidth = lines.Max(l => l.Length);

                //    this.Bounds.Width = maxWidth; // * ширину буквы в плоской системе координат
                //}

                return lines;
            }
        }

        public MetFont Font { get; set; }

        public MetUiIndicator(MetFont font, float x, float y, float width, float height, string text)
            : base(x, y, width, height, text)
        {
            Font = font;
            BackColor = Color.Transparent;
        }

        public MetUiIndicator(MetFont font, float x, float y, float width, float height, string text, bool newMatrix)
            : base(x, y, width, height, text)
        {
            Font = font;
            NewMatrix = newMatrix;
            BackColor = Color.Transparent;
        }

        public override void Draw()
        {
            base.Draw();

            if (CanMove)
            {
                if (MouseHover && MetInput.MouseState.LeftButton)
                {
                    Move(MetInput.MouseState.DiffX, MetInput.MouseState.DiffY);
                }
            }

            if (NewMatrix)
            {
                gl.PushMatrix();
                MetProjection.Ortho(MetEngine.Width, MetEngine.Height,
                    0, MetEngine.Width, MetEngine.Height, 0,
                    -MetUiDefaults.NearAndFar, MetUiDefaults.NearAndFar);
                gl.LoadIdentity();
            }

            gl.PushAttrib(gl.COLOR);

            gl.Color4ub(BackColor.R, BackColor.G, BackColor.B, BackColor.A);
            gl.Begin(gl.QUADS);
            gl.Vertex2f(Bounds.X, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
            gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
            gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
            gl.End();

            if (DrawBorder)
            {
                gl.Color4ub(BorderColor.R, BorderColor.G, BorderColor.B, BorderColor.A);
                gl.Begin(gl.LINE_LOOP);
                gl.Vertex2f(Bounds.X, Bounds.Y);
                gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y);
                gl.Vertex2f(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height);
                gl.Vertex2f(Bounds.X, Bounds.Y + Bounds.Height);
                gl.End();
            }

            gl.Color4ub(ForeColor.R, ForeColor.G, ForeColor.B, ForeColor.A);
            gl.PushMatrix();
            gl.Translatef(Bounds.X + 5f, Bounds.Y + 5f, 0f);
            for (int i = 0; i < Lines.Length; i++)
            {
                gl.PushMatrix();
                gl.Translatef(0f, i * FontSize, 0f);
                Font.Print(Lines[i], FontSize);
                gl.PopMatrix();
            }
            gl.PopMatrix();

            gl.PopAttrib();

            if (NewMatrix)
                gl.PopMatrix();
        }
    }
}
