﻿using ManagedOpenGl;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Coordinate wireframe</para>
    /// </summary>
    public class MetAxisWireframe
    {
        public static void DrawCoordinateRules()
        {
            DrawCoordinateRules(10f, false);
        }

        public static void DrawCoordinateRules(float length, bool lineStipple)
        {
            gl.PushMatrix();
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            // Точка 0; 0; 0
            //gl.PointSize(3f);
            //gl.Color3ub(0, 0, 0);
            //gl.Begin(gl.POINTS);
            //gl.Vertex3f(0, 0, 0);
            //gl.End();

            if (lineStipple)
            {
                gl.Enable(gl.LINE_STIPPLE);
                //gl.LineStipple(2, 0x471c);
                gl.LineStipple(2, 0x1111);
            }

            gl.Begin(gl.LINES);

            gl.Color3ub(200, 50, 50);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(length, 0f, 0f);

            gl.Vertex3f(length - 1, 0f, -1f);
            gl.Vertex3f(length, 0f, 0f);
            gl.Vertex3f(length - 1, 0f, 1f);
            gl.Vertex3f(length, 0f, 0f);

            gl.Color3ub(80, 50, 50);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(-length, 0f, 0f);

            gl.End();

            gl.Begin(gl.LINES);

            gl.Color3ub(50, 200, 50);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, length, 0f);
            gl.Vertex3f(-1f, length - 1, 0f);
            gl.Vertex3f(0f, length, 0f);
            gl.Vertex3f(1f, length - 1, 0f);
            gl.Vertex3f(0f, length, 0f);

            gl.Color3ub(50, 80, 50);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, -length, 0f);

            gl.End();

            gl.Begin(gl.LINES);

            gl.Color3ub(50, 50, 200);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, 0f, length);

            gl.Vertex3f(-1f, 0f, length - 1f);
            gl.Vertex3f(0f, 0f, length);
            gl.Vertex3f(1f, 0f, length - 1f);
            gl.Vertex3f(0f, 0f, length);

            gl.Color3ub(50, 50, 80);

            gl.Vertex3f(0f, 0f, 0f);
            gl.Vertex3f(0f, 0f, -length);

            gl.End();

            if (lineStipple)
                gl.Disable(gl.LINE_STIPPLE);

            gl.PopAttrib();
            gl.PopMatrix();
        }
    }
}
