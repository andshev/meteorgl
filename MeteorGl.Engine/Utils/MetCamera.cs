﻿using System;
using ManagedOpenGl;
using MeteorGl.Mathematics;

namespace MeteorGl.Engine.Utils
{
    public class MetCamera
    {
        public Vector3 Eye;
        public Vector3 Target;
        public float MouseSensX = 0.005f;
        public float MouseSensY = 0.005f;

        float _rotX;
        float _rotY;

        public MetCamera(float eyex, float eyey, float eyez,
                      float targetx, float targety, float targetz)
        {
            Eye = new Vector3(eyex, eyey, eyez);
            Target = new Vector3(targetx, targety, targetz);

            _rotX = 0f;
            _rotY = 0f;
        }

        public void LookAt()
        {
            glu.LookAt(
                Eye.X, Eye.Y, Eye.Z,
                Target.X, Target.Y, Target.Z,
                0f, 1f, 0f
            );
        }

        public void RotateX(float angle)
        {
            _rotX += angle;

            if (_rotX > 314f)
                _rotX = 314f;

            if (_rotX < -314f)
                _rotX = -314f;

            float rad = _rotX * MouseSensY;

            Target.Y = (float)Math.Sin(rad);
            Target.Z = (float)Math.Cos(rad);
        }

        public void RotateY(float angle)
        {
            _rotY += angle;

            float rad = _rotY * MouseSensX;

            Target.X = (float)Math.Sin(rad);
            Target.Z = (float)Math.Cos(rad);
        }

        public void MoveForward(float speed)
        {
            Eye.X += Target.X * speed;
            Eye.Z += Target.Z * speed;
            Target.X += Eye.X;
            Target.Z += Eye.Z;
        }

        public void MoveY(float speed)
        {
            Eye.Y += speed;
            Target.Y += Eye.Y;
        }
    }
}
