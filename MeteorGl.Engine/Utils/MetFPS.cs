﻿#region Using directives

using MeteorGl.Engine.Timers;

#endregion

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Performance counter (frames per second)</para>
    /// </summary>
    public static class MetFps
    {
        /// <summary>
        /// Количество кадров в секунду
        /// </summary>
        public static short Fps;

        static short _sumFps = 0;
        static float _lastTime = 0.0f;
        static float _currentTime = 0.0f;

        private static readonly MetTimer timer = new MetTimer();

        /// <summary>
        /// Выполняйте эту функцию вначале отрисовки
        /// </summary>
        public static void StartFpsCount()
        {
            _currentTime = timer.GetCount() / timer.GetFrequency();
            _sumFps++;

            if (_currentTime - _lastTime >= 1.0f)
            {
                _lastTime = _currentTime;
                Fps = _sumFps;
                _sumFps = 0;
            }
        }
    }
}
