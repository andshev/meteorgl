﻿using System.IO;
using System.Text;
using ManagedOpenGl;
using MeteorGl.Engine.Properties;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Textured font</para>
    /// </summary>
    public class MetFont
    {
        public struct Bukva
        {
            public float x;
            public float y;
            public float w;
        };

        public Bukva[] Letters;
        public uint GlTexture;
        public float ScaleX = 1f;
        public float ScaleY = 1f;
        public float LetterWidth = 1f;

        public float Height { get; private set; }

        public MetFont()
        {
            Letters = new Bukva[256];

            for (int i = 0; i < Letters.Length; i++)
                Letters[i] = new Bukva();
        }

        public void LoadDefault()
        {
            uint defaulgGlTexFontPtr = MetTexture.MakeTexture(Resources.imageDefaultFont);
            Load(defaulgGlTexFontPtr, "");
        }

        public void Load(uint glTexturePtr, string coordsFileName)
        {
            TextReader sr;

            if (coordsFileName == "")
                sr = new StringReader(Resources.fileDefaultFontCoords);
            else
                sr = new StreamReader(coordsFileName, Encoding.GetEncoding(1251));

            string line;
            string[] elem;
            string[] subelem;
            int k;
            float x, y, w;

            while ((line = sr.ReadLine()) != null)
            {
                if (line.StartsWith("//"))
                    continue;

                try
                {
                    float.Parse("0.1");
                }
                catch
                {
                    line = line.Replace('.', ',');
                }

                elem = line.Split('=');

                if (elem[0] == "height")
                    Height = float.Parse(elem[1]);
                if (char.IsDigit(elem[0], 0))
                {
                    k = int.Parse(elem[0]);
                    subelem = elem[1].Split(';');
                    x = float.Parse(subelem[0]);
                    y = float.Parse(subelem[1]);
                    w = float.Parse(subelem[2]);
                    Letters[k].x = x;
                    Letters[k].y = y;
                    Letters[k].w = w;
                }
            }

            sr.Close();

            GlTexture = glTexturePtr;
        }

        public void Print(string text, float scale)
        {
            if (text == null)
                return;

            int kb;
            byte[] bytes = Encoding.GetEncoding(1251).GetBytes(text);

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.Enable(gl.TEXTURE_2D);
            gl.BindTexture(gl.TEXTURE_2D, GlTexture);
            gl.Enable(gl.BLEND);
            gl.BlendFunc(gl.SRC_ALPHA, gl.ONE);
            gl.DepthMask(false);

            gl.PushMatrix();
            gl.Scalef(scale, scale, 0f);

            for (int i = 0; i < text.Length; i++)
            {
                kb = bytes[i];

                gl.Begin(gl.QUADS);

                gl.TexCoord2f(Letters[kb].x, Letters[kb].y);
                gl.Vertex2f(0f, 0f);

                gl.TexCoord2f(Letters[kb].x + Letters[kb].w, Letters[kb].y);
                gl.Vertex2f(ScaleX, 0f);

                gl.TexCoord2f(Letters[kb].x + Letters[kb].w, Letters[kb].y + Height);
                gl.Vertex2f(ScaleX, ScaleY);

                gl.TexCoord2f(Letters[kb].x, Letters[kb].y + Height);
                gl.Vertex2f(0f, ScaleY);

                gl.End();

                gl.Translatef(LetterWidth, 0f, 0f);
            }

            gl.PopMatrix();

            gl.PopAttrib();
        }

        public void Test()
        {
            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.Enable(gl.TEXTURE_2D);
            gl.BindTexture(gl.TEXTURE_2D, GlTexture);
            gl.Color4f(1f, 1f, 1f, 1f);

            gl.Begin(gl.QUADS);

            gl.TexCoord2f(0.0366f,  - 0.0714f);
            gl.Vertex2f(0f, 0f);

            gl.TexCoord2f(0.0366f + 0.0884f,  - 0.0714f);
            gl.Vertex2f(1f, 0f);

            gl.TexCoord2f(0.0366f + 0.0884f,  - 0.0714f - 0.0384f);
            gl.Vertex2f(1f, 1f);

            gl.TexCoord2f(0.0366f,  - 0.0714f - 0.0384f);
            gl.Vertex2f(0f, 1f);

            //gl.TexCoord2f(0f, 0f);
            //gl.Vertex2f(0f, 0f);

            //gl.TexCoord2f(1f, 0f);
            //gl.Vertex2f(1f, 0f);

            //gl.TexCoord2f(1f, 1f);
            //gl.Vertex2f(1f, 1f);

            //gl.TexCoord2f(0f, 1f);
            //gl.Vertex2f(0f, 1f);

            gl.End();
            
            gl.PopAttrib();
        }
    }
}
