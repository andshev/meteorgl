using ManagedOpenGl;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Some simple projections</para>
    /// </summary>
    public static class MetProjection
    {
        private static double fovY = 45.0;
        private static double nearClippingPlane = 0.1f;
        private static double farClippingPlane = 100.0f;

        public static void Perspective(int width, int height)
        {
			gl.Viewport(0, 0, width, height);
			gl.MatrixMode(gl.PROJECTION);
			gl.LoadIdentity();

			glu.Perspective(fovY, (float) width / (float) height, nearClippingPlane, farClippingPlane);

			gl.MatrixMode(gl.MODELVIEW);
			gl.LoadIdentity();
		}

		public static void Perspective(int width, int height, double fovy, double near, double far)
		{
			gl.Viewport(0, 0, width, height);
			gl.MatrixMode(gl.PROJECTION);
			gl.LoadIdentity();

			glu.Perspective(fovy, (float)width / (float)height, near, far);

			gl.MatrixMode(gl.MODELVIEW);
			gl.LoadIdentity();
		}

        public static void Ortho(int width, int height, double left, double right, double bottom, double top, double near, double far)
		{
			gl.Viewport(0, 0, width, height);
			gl.MatrixMode(gl.PROJECTION);
			gl.LoadIdentity();

			gl.Ortho(left, right, bottom, top, near, far);

			gl.MatrixMode(gl.MODELVIEW);
			gl.LoadIdentity();
		}
	}
}
