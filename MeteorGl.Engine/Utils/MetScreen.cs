﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Working with screen</para>
    /// </summary>
    public static class MetScreen
    {
        public const int ENUM_CURRENT_SETTINGS = -1;
        
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct DeviceSettings
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string dmDeviceName;

            public short dmSpecVersion;
            public short dmDriverVersion;
            public short dmSize;
            public short dmDriverExtra;
            public int dmFields;
            public int dmPositionX;
            public int dmPositionY;
            public int dmDisplayOrientation;
            public int dmDisplayFixedOutput;
            public short dmColor;
            public short dmDuplex;
            public short dmYResolution;
            public short dmTTOption;
            public short dmCollate;

            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string dmFormName;

            public short dmLogPixels;
            public short dmBitsPerPel;
            public int dmPelsWidth;
            public int dmPelsHeight;
            public int dmDisplayFlags;
            public int dmDisplayFrequency;
            public int dmICMMethod;
            public int dmICMIntent;
            public int dmMediaType;
            public int dmDitherType;
            public int dmReserved1;
            public int dmReserved2;
            public int dmPanningWidth;
            public int dmPanningHeight;
        };

        static DeviceSettings CreateDevmode()
        {
            DeviceSettings dm = new DeviceSettings();
            dm.dmDeviceName = new String(new char[32]);
            dm.dmFormName = new String(new char[32]);
            dm.dmSize = (short)Marshal.SizeOf(dm);
            return dm;
        }

        public static DeviceSettings GetCurrentSettings()
        {
            DeviceSettings devMode = CreateDevmode();
            EnumDisplaySettings(null, MetScreen.ENUM_CURRENT_SETTINGS, ref devMode);
            return devMode;
        }

        public static bool ChangeSettings(DeviceSettings devmode)
        {
            if (ChangeDisplaySettings(ref devmode, 0) == 0)
                return true;
            else
                return false;
        }

        public static List<DeviceSettings> GetAllSettings()
        {
            List<DeviceSettings> modes = new List<DeviceSettings>();

            for (int i = 0; i < 1000; i++)
            {
                DeviceSettings devMode = CreateDevmode();
                if (EnumDisplaySettings(null, i, ref devMode) > 0)
                {
                    if (devMode.dmDisplayFrequency > 50)
                        modes.Add(devMode);
                }
                else
                    return modes;
            }

            return modes;
        }

        [DllImport("user32.dll", CharSet = CharSet.Ansi)]
        static extern int EnumDisplaySettings(
            string lpszDeviceName,
            int iModeNum,
            ref DeviceSettings lpDevMode);

        [DllImport("user32.dll", CharSet=CharSet.Ansi)]
        static extern int ChangeDisplaySettings(
            ref DeviceSettings lpDevMode,
            int dwFlags);
    }
}
