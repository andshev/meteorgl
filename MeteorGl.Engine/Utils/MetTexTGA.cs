using System;
using System.IO;
using System.Text;
using ManagedOpenGl;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Texture from TGA-file</para>
    /// </summary>
    public struct MetTexTga
    {
        public uint Bpp;
        public uint Width;
        public uint Height;
        public uint GlTexture;
        
        public void Load(string filenameTga)
        {
            byte[] TGAheader = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] TGAcompare = new byte[12];
            byte[] header = new byte[6];
            byte[] imageData;
            uint bytesPerPixel;
            uint imageSize;
            uint type = gl.BGRA;
            FileStream stream = null;
            ASCIIEncoding encoding = new ASCIIEncoding();
            BinaryReader reader = null;

            stream = new FileStream(filenameTga, FileMode.Open);
            reader = new BinaryReader(stream, encoding);

            for (int i = 0; i < TGAcompare.Length; i++)
                TGAcompare[i] = reader.ReadByte();

            for (int i = 0; i < TGAcompare.Length; i++)
            {
                if (TGAcompare[i] != TGAheader[i])
                    throw new Exception("Wrong header");
            }

            for (int i = 0; i < 6; i++)
                header[i] = reader.ReadByte();

            Width = (uint)header[1] * 256 + header[0];      //(highbyte * 256 + lowbyte)
            Height = (uint)header[3] * 256 + header[2];     // (highbyte * 256 + lowbyte)

            if (Width <= 0 || Height <= 0 || (header[4] != 24 && header[4] != 32))
                throw new Exception("Unsupported format is used. Only 24 and 32 bits TGA are supported.");

            Bpp = header[4];
            bytesPerPixel = Bpp / 8;
            imageSize = Width * Height * bytesPerPixel;

            imageData = new byte[imageSize];

            for (int i = 0; i < imageSize; i++)
                imageData[i] = reader.ReadByte();

            gl.GenTextures(1, ref GlTexture);
            gl.BindTexture(gl.TEXTURE_2D, GlTexture);
            gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            gl.TexParameterf(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

            if (Bpp == 24)
                type = gl.BGR;

            if (Bpp == 32)
                type = gl.BGRA;

            gl.TexImage2D(gl.TEXTURE_2D, 0, (int)gl.RGB8, (int)Width, (int)Height, 0,
                type, gl.UNSIGNED_BYTE, imageData);

            reader.Close();
            stream.Close();

            imageData = null;
        }

        public static uint LoadTga(string filenameTga)
        {
            MetTexTga tga = new MetTexTga();
            tga.Load(filenameTga);

            return tga.GlTexture;
        }
    }
}
