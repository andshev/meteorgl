﻿using System.Drawing;
using ManagedOpenGl;

namespace MeteorGl.Engine.Utils
{
    /// <summary>
    /// <para>Simple texture</para>
    /// </summary>
    public static class MetTexture
    {
    	public static uint MakeTexture(Bitmap bmp)
    	{
            uint t;

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.Enable(gl.TEXTURE_2D);

            t = gl.GenTexture();
            gl.BindTexture(gl.TEXTURE_2D, t);
            gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, (int)gl.LINEAR);
            gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, (int)gl.LINEAR);
            gl.TexImage2D(gl.TEXTURE_2D, 0, (int)gl.RGBA8, 0, bmp);

            gl.PopAttrib();

            return t;
    	}
    	
        public static uint LoadTexture(string filename)
        {
            uint t;

            gl.PushAttrib(gl.ALL_ATTRIB_BITS);

            gl.Enable(gl.TEXTURE_2D);

            t = gl.GenTexture();
            gl.BindTexture(gl.TEXTURE_2D, t);
            gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, (int)gl.LINEAR);
            gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, (int)gl.LINEAR);
            gl.TexImage2D(gl.TEXTURE_2D, 0, (int)gl.RGBA8, 0, filename);

            gl.PopAttrib();

            return t;
        }
    }
}
