﻿namespace MeteorGl.Mathematics
{
    public class MatUtils
    {
        public static float QuatTol = 0.0001f;

        public static float Rad2Deg(float rad)
        {
            return (float)(rad * 180.0f / System.Math.PI);
        }

        public static float Deg2Rad(float deg)
        {
            return (float)(deg / 180.0f * System.Math.PI);
        }
    }
}
