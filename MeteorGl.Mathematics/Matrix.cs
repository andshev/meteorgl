using System;

namespace MeteorGl.Mathematics
{
    public class Matrix
    {
        public byte Rows { get; }
        public byte Cols { get; }
        public byte Count { get; }
        public float[] Elems { get; set; }

        public float this[byte row, byte column]
        {
            get
            {
                return Elems[row * Cols + column];
            }
            set
            {
                Elems[row * Cols + column] = value;
            }
        }

        public Matrix(byte rows, byte cols)
        {
            Rows = rows;
            Cols = cols;
            Count = (byte)(Rows * Cols);
            Elems = new float[Count];
        }

        public Matrix(byte rows, byte cols, params float[] values)
        {
            Rows = rows;
            Cols = cols;
            Count = (byte)(Rows * Cols);
            Elems = new float[Count];
            values.CopyTo(Elems, 0);
        }

        public Matrix(Matrix m)
        {
            Rows = m.Rows;
            Cols = m.Cols;
            Count = (byte)(Rows * Cols);
            Elems = new float[Count];
            m.Elems.CopyTo(Elems, 0);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var mc = obj as Matrix;
            bool error = false;

            if (!DimEquals(mc))
                return false;

            for (int i = 0; i < Elems.Length; i++)
            {
                if (Elems[i] != mc.Elems[i])
                    error = true;
            }

            return !error;
        }

        public Matrix GetRow(byte row)
        {
            Matrix m = new Matrix(1, Cols);

            for (byte i = 0; i < Cols; i++)
                m[0, i] = this[0, i];

            return m;
        }

        public Matrix GetRow(byte row, byte elemCount)
        {
            Matrix m = new Matrix(1, Cols);

            for (byte i = 0; i < elemCount; i++)
                m[0, i] = this[0, i];

            return m;
        }

        public Matrix GetCol(byte col)
        {
            Matrix m = new Matrix(Rows, 1);

            for (byte i = 0; i < Rows; i++)
                m[i, 0] = this[i, 0];

            return m;
        }

        public Matrix GetCol(byte col, byte elemCount)
        {
            Matrix m = new Matrix(Rows, 1);

            for (byte i = 0; i < elemCount; i++)
                m[i, 0] = this[i, 0];

            return m;
        }

        public bool DimEquals(Matrix m)
        {
            return Rows == m.Rows && Cols == m.Cols;
        }

        public static bool operator ==(Matrix m1, Matrix m2)
        {
            return m1 != null && m1.Equals(m2);
        }

        public static bool operator !=(Matrix m1, Matrix m2)
        {
            return m1 != null && !m1.Equals(m2);
        }

        public static Matrix operator +(Matrix m1, Matrix m2)
        {
            if (!m1.DimEquals(m2))
                throw new Exception("Matrices have different dimensions");

            Matrix vcn = new Matrix(m1.Rows, m1.Cols);

            for (byte i = 0; i < vcn.Count; i++)
                vcn.Elems[i] = m1.Elems[i] + m2.Elems[i];

            return vcn;
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (!m1.DimEquals(m2))
                throw new Exception("Matrices have different dimensions");

            Matrix vcn = new Matrix(m1.Rows, m1.Cols);

            for (byte i = 0; i < vcn.Count; i++)
                vcn.Elems[i] = m1.Elems[i] - m2.Elems[i];

            return vcn;
        }

        public static Matrix operator -(Matrix m)
        {
            Matrix mn = new Matrix(m);

            for (byte i = 0; i < mn.Count; i++)
                mn.Elems[i] = -m.Elems[i];

            return mn;
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (m1.Cols != m2.Rows)
                throw new Exception("Cannot multiply matrices");

            Matrix vcn = new Matrix(m1.Rows, m2.Cols);

            for (byte r = 0; r < m1.Rows; r++)
            {
                for (byte c = 0; c < m2.Cols; c++)
                {
                    for (byte i = 0; i < m1.Cols; i++)
                    {
                        vcn[r, c] += m1[r, i] * m2[i, c];
                    }
                }
            }

            return vcn;
        }

        public static Matrix operator *(Matrix m, float f)
        {
            Matrix mn = new Matrix(m.Rows, m.Cols);

            for (byte i = 0; i < mn.Count; i++)
                mn.Elems[i] *= f;

            return mn;
        }

        public static Matrix operator /(Matrix m, float f)
        {
            Matrix mn = new Matrix(m.Rows, m.Cols);

            for (byte i = 0; i < mn.Count; i++)
                mn.Elems[i] /= f;

            return mn;
        }
    }
}
