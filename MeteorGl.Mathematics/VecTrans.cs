namespace MeteorGl.Mathematics
{
    /// <summary>
    /// DEEP DEVELOPEMENT
    /// </summary>
    public static class VecTrans
    {
        public static Vector4 Rotate4D(Vector4 v, Vector4 rotationDeg)
        {
            return Rotate4D(v, rotationDeg.X, rotationDeg.Y, rotationDeg.Z);
        }

        public static Vector4 Rotate4D(Vector4 v, float xrotdeg, float yrotdeg, float zrotdeg)
        {
            return Rotate4DRad(v, MatUtils.Deg2Rad(xrotdeg), MatUtils.Deg2Rad(yrotdeg), MatUtils.Deg2Rad(zrotdeg));
        }

        public static Vector4 Rotate4DRad(Vector4 v, float xrotrad, float yrotrad, float zrotrad)
        {
            Matrix vfinal = new Matrix(v);
            float angleSin;
            float angleCos;
            Matrix mRot;

            angleSin = (float)System.Math.Sin(xrotrad);
            angleCos = (float)System.Math.Cos(xrotrad);
            mRot = new Matrix(4, 4,
                1f, 0f,         0f, 0f,
                0f, angleCos,  -angleSin, 0f,
                0f, angleSin,  angleCos,  0f,
                0f, 0f,         0f,         1f
            );
            vfinal = mRot * vfinal;

            angleSin = (float)System.Math.Sin(yrotrad);
            angleCos = (float)System.Math.Cos(yrotrad);
            mRot = new Matrix(4, 4,
                angleCos,  0f, angleSin,  0f,
                0f,         1f, 0f,         0f,
                -angleSin, 0f, angleCos,  0f,
                0f,         0f, 0f,         1f
            );
            vfinal = mRot * vfinal;

            angleSin = (float)System.Math.Sin(zrotrad);
            angleCos = (float)System.Math.Cos(zrotrad);
            mRot = new Matrix(4, 4,
                angleCos,  -angleSin, 0f, 0f,
                angleSin,  angleCos,  0f, 0f,
                0f,         0f,         1f, 0f,
                0f,         0f,         0f, 1f
            );
            vfinal = mRot * vfinal;

            return new Vector4(vfinal);
        }

        public static Vector3 Rotate3DRad(Vector3 v, float xRotRad, float yRotRad, float zRotRad)
        {
            if (xRotRad == 0f && yRotRad == 0f && zRotRad == 0f)
                return new Vector3(v);

            Matrix vfinal = new Matrix(v);
            float angleSin;
            float angleCos;
            Matrix mRot;

            //if (xRotRad != 0f)
            //{
                angleSin = (float)System.Math.Sin(xRotRad);
                angleCos = (float)System.Math.Cos(xRotRad);
                mRot = new Matrix(3, 3,
                    1f, 0f, 0f,
                    0f, angleCos, -angleSin,
                    0f, angleSin, angleCos
                );
                vfinal = mRot * vfinal;
            //}

            //if (yRotRad != 0f)
            //{
                angleSin = (float)System.Math.Sin(yRotRad);
                angleCos = (float)System.Math.Cos(yRotRad);
                mRot = new Matrix(3, 3,
                    angleCos, 0f, angleSin,
                    0f, 1f, 0f,
                    -angleSin, 0f, angleCos
                );
                vfinal = mRot * vfinal;
            //}

            //if (zRotRad != 0f)
            //{
                angleSin = (float)System.Math.Sin(zRotRad);
                angleCos = (float)System.Math.Cos(zRotRad);
                mRot = new Matrix(3, 3,
                    angleCos, -angleSin, 0f,
                    angleSin, angleCos, 0f,
                    0f, 0f, 1f
                );
                vfinal = mRot * vfinal;
            //}

            return new Vector3(vfinal);
        }

        public static Vector3 Rotate3D(Vector3 v, float xrotdeg, float yrotdeg, float zrotdeg)
        {
            return Rotate3DRad(v, MatUtils.Deg2Rad(xrotdeg), MatUtils.Deg2Rad(yrotdeg), MatUtils.Deg2Rad(zrotdeg));
        }
    }
}
