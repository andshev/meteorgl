using System.Text;

namespace MeteorGl.Mathematics
{
    public static class VecUtils
    {
        public static float DotProduct(Vector3 v1, Vector3 v2)
        {
            float f = 0f;

            f += v1.X * v2.X;
            f += v1.Y * v2.Y;
            f += v1.Z * v2.Z;

            return f;
        }
        
        public static Vector3 CrossProduct(Vector3 v1, Vector3 v2)
        {
            return new Vector3(
                v1.Y * v2.Z - v1.Z * v2.Y,
                v1.Z * v2.X - v1.X * v2.Z,
                v1.X * v2.Y - v1.Y * v2.X);
        }

        public static string ToHtml(Matrix m)
        {
            var s = new StringBuilder();

            for (byte r = 0; r < m.Rows; r++)
            {
                for (byte c = 0; c < m.Cols; c++)
                {
                    s.Append($"{m[r, c]}; ");
                }

                s.Append("<br/>");
            }

            return s.ToString();
        }
    }
}
