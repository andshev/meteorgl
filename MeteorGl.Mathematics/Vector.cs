namespace MeteorGl.Mathematics
{
    public class Vector2 : Matrix
    {
        public float X
        {
            get
            {
                return Elems[0];
            }
            set
            {
                Elems[0] = value;
            }
        }

        public float Y
        {
            get
            {
                return Elems[1];
            }
            set
            {
                Elems[1] = value;
            }
        }

        public float Length
        {
            get
            {
                return (float)System.Math.Sqrt(X * X + Y * Y);
            }
        }
        
        public Vector2()
            : base(2, 1)
        {
        }

        public Vector2(float x, float y)
            : base(2, 1, x, y)
        {
        }

        public Vector2(Matrix m)
            : base(2, 1, m.Elems[0], m.Elems[1])
        {
        }

        public void Normalize()
        {
            float nv = Length;

            X /= nv;
            Y /= nv;
        }

        public void Reverse()
        {
            X = -X;
            Y = -Y;
        }
    }
}
