namespace MeteorGl.Mathematics
{
    public class Vector3 : Matrix
    {
        public float X
        {
            get
            {
                return Elems[0];
            }
            set
            {
                Elems[0] = value;
            }
        }

        public float Y
        {
            get
            {
                return Elems[1];
            }
            set
            {
                Elems[1] = value;
            }
        }

        public float Z
        {
            get
            {
                return Elems[2];
            }
            set
            {
                Elems[2] = value;
            }
        }

        public float Length
        {
            get
            {
                return (float)System.Math.Sqrt(X * X + Y * Y + Z * Z);
            }
        }

        public Vector3()
            : base(3, 1)
        {
        }

        public Vector3(float x, float y, float z)
            : base(3, 1, x, y, z)
        {
        }

        public Vector3(Matrix m)
            : base(3, 1, m.Elems[0], m.Elems[1], m.Elems[2])
        {
        }

        public void Clear()
        {
            X = 0f;
            Y = 0f;
            Z = 0f;
        }

        public void Normalize()
        {
            float nv = Length;

            X /= nv;
            Y /= nv;
            Z /= nv;
        }

        public void Reverse()
        {
            X = -X;
            Y = -Y;
            Z = -Z;
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        public static Vector3 operator *(Vector3 v, float scalar)
        {
            return new Vector3(v.X * scalar, v.Y * scalar, v.Z * scalar);
        }

        public static Vector3 operator /(Vector3 v, float scalar)
        {
            return new Vector3(v.X / scalar, v.Y / scalar, v.Z / scalar);
        }

        public static Vector3 operator ^(Vector3 v1, Vector3 v2)
        {
            return VecUtils.CrossProduct(v1, v2);
        }

        public static float operator *(Vector3 v1, Vector3 v2)
        {
            return VecUtils.DotProduct(v1, v2);
        }
    }
}