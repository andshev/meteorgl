namespace MeteorGl.Mathematics
{
    public class Vector4 : Matrix
    {
        public float X
        {
            get
            {
                return Elems[0];
            }
            set
            {
                Elems[0] = value;
            }
        }

        public float Y
        {
            get
            {
                return Elems[1];
            }
            set
            {
                Elems[1] = value;
            }
        }

        public float Z
        {
            get
            {
                return Elems[2];
            }
            set
            {
                Elems[2] = value;
            }
        }

        public float W
        {
            get
            {
                return Elems[3];
            }
            set
            {
                Elems[3] = value;
            }
        }

        public float Length
        {
            get
            {
                return (float)System.Math.Sqrt(X * X + Y * Y + Z * Z + W * W);
            }
        }

        public Vector4()
            : base(4, 1)
        {
        }

        public Vector4(float x, float y, float z, float w)
            : base(4, 1, x, y, z, w)
        {
        }

        public Vector4(Matrix m)
            : base(4, 1, m.Elems[0], m.Elems[1], m.Elems[2], m.Elems[3])
        {
        }

        public Vector4(Vector4 q)
            : base(4, 1, q.Elems[0], q.Elems[1], q.Elems[2], q.Elems[3])
        {
        }

        public Vector4(Vector3 v, float w)
            : base(4, 1, v.Elems[0], v.Elems[1], v.Elems[2], w)
        {
        }

        public Vector4(float xpitchdeg, float yyawdeg, float zrolldeg)
            : this(Vector4.FromEuler(xpitchdeg, yyawdeg, zrolldeg))
        {
        }

        public void Clear()
        {
            X = 0f;
            Y = 0f;
            Z = 0f;
            W = 1f;
        }

        public static Vector4 FromSpherical(float latitude, float longitude, float angle)
        {
            float sinA = (float)System.Math.Sin(angle / 2);
            float cosA = (float)System.Math.Cos(angle / 2);

            float sinLat = (float)System.Math.Sin(latitude);
            float cosLat = (float)System.Math.Cos(latitude);

            float sinLong = (float)System.Math.Sin(longitude);
            float cosLong = (float)System.Math.Cos(longitude);

            return new Vector4(
                sinA * cosLat * sinLong,
                sinA * sinLat,
                sinA * sinLat * cosLong,
                cosA);
        }

        public static Vector4 FromEuler(float xdeg, float ydeg, float zdeg)
        {
            float pitch = MatUtils.Deg2Rad(ydeg);
            float yaw = MatUtils.Deg2Rad(xdeg);
            float roll = MatUtils.Deg2Rad(zdeg);

            float   cy, cp, cr, sy, sp, sr,
                cycp, sysp, cysp, sycp;

            cy = (float)System.Math.Cos(0.5f * yaw);
            cp = (float)System.Math.Cos(0.5f * pitch);
            cr = (float)System.Math.Cos(0.5f * roll);
            sy = (float)System.Math.Sin(0.5f * yaw);
            sp = (float)System.Math.Sin(0.5f * pitch);
            sr = (float)System.Math.Sin(0.5f * roll);

            cycp = cy * cp;
            sysp = sy * sp;
            cysp = cy * sp;
            sycp = sy * cp;

            return new Vector4(
                cycp * sr - sysp * cr,
                cycp * cr + sycp * sr,
                sycp * cr - cysp * sr,
                cycp * cr + sysp * sr);
        }

        public Vector3 GetEulerAngles()
        {
            float r11, r21, r31, r32, r33, r12, r13, q00, q11, q22, q33, tmp;

            q00 = W * W;
            q11 = X * X;
            q22 = Y * Y;
            q33 = Z * Z;

            r11 = q00 + q11 - q22 - q33;
            r21 = 2 * (X * Y + W * Z);
            r31 = 2 * (X * Z + W * Y);
            r32 = 2 * (Y * Z + W * X);
            r33 = q00 - q11 - q22 + q33;

            tmp = System.Math.Abs(r31);

            if (tmp > 0.999999)
            {
                r12 = 2 * (X * Y - W * Z);
                r13 = 2 * (X * Z + W * Y);

                return new Vector3(
                    MatUtils.Rad2Deg(0f),
                    MatUtils.Rad2Deg(-((float)System.Math.PI / 2) * r31 / tmp),
                    MatUtils.Rad2Deg((float)System.Math.Atan2(-r12, -r31 * r13)));
            }

            return new Vector3(
                MatUtils.Rad2Deg((float)System.Math.Atan2(r32, r33)),
                MatUtils.Rad2Deg((float)System.Math.Asin(-r31)),
                MatUtils.Rad2Deg((float)System.Math.Atan2(r21, r11)));
        }

        public float GetScalar()
        {
            return W;
        }

        public Vector3 GetVector()
        {
            return new Vector3(X, Y, Z);
        }

        public float GetAngle()
        {
            return 2f * (float)System.Math.Acos(W);
        }

        public void Normalize()
        {
            float nv = GetVector().Length;

            X /= nv;
            Y /= nv;
            Z /= nv;
        }

        public Vector3 GetAxis()
        {
            Vector3 v = GetVector();
            float m = v.Length;

            if (m <= MatUtils.QuatTol)
                return v;
            else
                return v / m;
        }

        public void Rotate(Vector4 q)
        {
            Vector4 nv1 = q * this;
            Vector4 nv2 = ~q;
            Vector4 nv = nv1 * nv2;

            this.X = nv.X;
            this.Y = nv.Y;
            this.Z = nv.Z;
            this.W = nv.W;
        }

        //public void Rotate(Vector3 v)
        //{
        //    Vector4 q = new Vector4(v, 1f);
        //    Vector4 nv = new Vector4(q * v * (~q));

        //    this.X = nv.X;
        //    this.Y = nv.Y;
        //    this.Z = nv.Z;
        //    this.W = nv.W;
        //}

        public static Vector4 operator +(Vector4 v1, Vector4 v2)
        {
            return new Vector4(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W);
        }

        public static Vector4 operator -(Vector4 v1, Vector4 v2)
        {
            return new Vector4(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W);
        }

        public static Vector4 operator *(Vector4 v, float scalar)
        {
            return new Vector4(v.X * scalar, v.Y * scalar, v.Z * scalar, v.W * scalar);
        }

        public static Vector4 operator /(Vector4 v, float scalar)
        {
            return new Vector4(v.X / scalar, v.Y / scalar, v.Z / scalar, v.W / scalar);
        }

        public static Vector4 operator ~(Vector4 v)
        {
            return new Vector4(-v.X, -v.Y, -v.Z, v.W);
        }

        public static Vector4 operator *(Vector4 v1, Vector4 v2)
        {
            return new Vector4(
                v1.W * v2.X + v1.X * v2.W + v1.Y * v2.Z - v1.Z * v2.Y,
                v1.W * v2.Y + v1.Y * v2.W + v1.Z * v2.X - v1.X * v2.Z,
                v1.W * v2.Z + v1.Z * v2.W + v1.X * v2.Y - v1.Y * v2.X,
                v1.W * v2.W - v1.X * v2.X - v1.Y * v2.Y - v1.Z * v2.Z);
        }

        public static Vector4 operator *(Vector4 q, Vector3 v)
        {
            return new Vector4(
                q.W * v.X + q.Y * v.Z - q.Z * v.Y,
                q.W * v.X + q.Z * v.X - q.X * v.Z,
                q.W * v.X + q.X * v.Y - q.Y * v.X,
                -(q.X * v.X + q.Y * v.Y + q.Z * v.Z));
        }

        public static Vector4 operator *(Vector3 v, Vector4 q)
        {
            return new Vector4(
                q.W * v.X + q.Z * v.Y - q.Y * v.Z,
                q.W * v.Y + q.X * v.Z - q.Z * v.X,
                q.W * v.Z + q.Y * v.X - q.X * v.Y,
                -(q.X * v.X + q.Y * v.Y + q.Z * v.Z));
        }
    }
}