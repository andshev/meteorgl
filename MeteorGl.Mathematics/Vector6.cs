namespace MeteorGl.Mathematics
{
    public class Vector6 : Matrix
    {
        public float MinX
        {
            get
            {
                return Elems[0];
            }
            set
            {
                Elems[0] = value;
            }
        }

        public float MaxX
        {
            get
            {
                return Elems[1];
            }
            set
            {
                Elems[1] = value;
            }
        }

        public float MinY
        {
            get
            {
                return Elems[2];
            }
            set
            {
                Elems[2] = value;
            }
        }

        public float MaxY
        {
            get
            {
                return Elems[3];
            }
            set
            {
                Elems[3] = value;
            }
        }

        public float MinZ
        {
            get
            {
                return Elems[4];
            }
            set
            {
                Elems[4] = value;
            }
        }

        public float MaxZ
        {
            get
            {
                return Elems[5];
            }
            set
            {
                Elems[5] = value;
            }
        }

        public Vector6()
            : base(6, 1)
        {
        }

        public Vector6(float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
            : base(6, 1, minX, maxX, minY, maxY, minZ, maxZ)
        {
        }

        public Vector6(Matrix m)
            : base(6, 1, m.Elems[0], m.Elems[1], m.Elems[2], m.Elems[3], m.Elems[4], m.Elems[5])
        {
        }
    }
}