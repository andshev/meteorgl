﻿using System;
using System.Windows.Forms;
using ManagedOpenGl;
using MeteorGl.Engine;
using MeteorGl.Engine.UI;
using MeteorGl.Engine.Utils;

namespace Test
{
    public class Program : MetProgram
    {
        MetFont font;
        MetUiPanel uiPanel;
        MetUiLabel uiLabel;
        MetUiTextBox uiTextbox;
        MetUiButton uiButton;
        MetUiIndicator uiLines;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Program p = new Program();

            MetEngine.Width = 1024; // Don't use MetGlav.Window.Width and MetGlav.Window.Height!!!
            MetEngine.Height = 768;

            MetEngine.StartWindow(p); // Start in window mode.
        }

        public override void Init()
        {
            MetEngine.Window.Text = "Meteor - test program";

            gl.Enable(gl.DEPTH_TEST);
            gl.ClearColor(0f, 0f, 0f, 1f);
            gl.ClearDepth(1f);
            gl.DepthFunc(gl.LEQUAL);
            gl.ShadeModel(gl.SMOOTH);
            gl.Hint(gl.PERSPECTIVE_CORRECTION_HINT, gl.NICEST);
            gl.Enable(gl.LINE_SMOOTH);
            gl.Hint(gl.LINE_SMOOTH_HINT, gl.NICEST);
            gl.Enable(gl.BLEND);
            gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

            font = new MetFont();
            font.LoadDefault();

            // Creating control panel
            uiPanel = new MetUiPanel(20, 20, 400, 300, font);

            uiLabel = new MetUiLabel(30, 30, 100, 20, "menu");   
            uiPanel.AddControl(uiLabel);

            uiLabel = new MetUiLabel(50, 100, 50, 20, "code:");
            uiPanel.AddControl(uiLabel);

            uiTextbox = new MetUiTextBox(110, 100, 150, 20, "");
            uiPanel.AddControl(uiTextbox, false);

            uiButton = new MetUiButton(40, 270, 100, 20, "button");
            uiButton.OnClick += new EventHandler(button_OnClick);
            uiPanel.AddControl(uiButton, false);

            uiLines = new MetUiIndicator(font, 20, 400, 180, 200, "move window\nmove this text\ncool!");
        }

        void button_OnClick(object sender, EventArgs e)
        {
            uiTextbox.Text = Environment.TickCount.ToString();
        }

        public override void Draw()
        {
            gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
            MetProjection.Perspective(MetEngine.Width, MetEngine.Height, 45.0, 0.01, 5000.0);
            gl.LoadIdentity();

            // Drawing 3D scene using OpenGL

            gl.Translated(-2, -2, -20);
            MetAxisWireframe.DrawCoordinateRules();

            // Control panel
            uiPanel.Draw();
            uiLines.Draw();
        }

        public override void Input()
        {
            if (MetInput.KeyPressed(Keys.Escape))
                MetEngine.Stop();
        }
    }
}
